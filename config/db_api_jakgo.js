/**
 * Created by fzlrhmn on 11/11/16.
 */
require('dotenv').config();

module.exports = {
    client: 'mysql',
    connection: {
        host			: process.env.DB_CROP_HOST,
        user			: process.env.DB_CROP_USER,
        password		: process.env.DB_CROP_PASS,
        database		: process.env.DB_CROP_DATABASE,
        port			: process.env.DB_CROP_PORT
    },
    pool: {
        min: 2,
        max: 10
    }
};