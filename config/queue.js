/**
 * Created by fzlrhmn on 9/5/16.
 */
var async 		= require('async');
let qlueModel   = require('../models/qlue_report');
var winston		= require('../config/log');
let slack		= require('../config/slack');

var q = async.queue(function(task, callback) {

    console.log(task.url);
    qlueModel.sendStatusCallback(task.url, function(error,data) {
        let paramsError = {
            title : __filename,
            error : data,
            payload : 'test'
        };
        slack(paramsError);
        console.log(data);
    });
    callback();
},1);

module.exports = q;