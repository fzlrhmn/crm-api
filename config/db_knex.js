require('dotenv').config();

module.exports = {
    client: 'mysql',
    connection: {
        host			: process.env.DB_HOST,
        user			: process.env.DB_USER,
        password		: process.env.DB_PASS,
        database		: process.env.DB_DATABASE,
        port			: process.env.DB_PORT,
        charset         : "utf8mb4"
    },
    pool: {
        min: 2,
        max: 20
    }
};