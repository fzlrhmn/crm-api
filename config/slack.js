/**
 * Created by fzlrhmn on 8/30/16.
 */

var Slack       = require('slack-node');
let moment 		        = require('moment');
moment().format();
moment.locale('id');

module.exports = function (params) {

    let message = '';
    let error_message = ''+params.error+'';
    message += "*File* : Error in "+params.title+"\n";
    message += "*Time* : " + moment().format("Do MMMM YYYY HH:mm:ss")+'\n';
    message += "*Payload* : " + JSON.stringify(params.payload);

    var slack = new Slack();
    var webhookUri = "https://hooks.slack.com/services/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";
    slack.setWebhook(webhookUri);

    slack.webhook({
        channel: "#crmlog",
        username: "CRM log alert",
        mrkdwn : true,
        text: message,
        attachments : [
            {
                color : "danger",
                text : error_message
            }
        ]
    }, function(err, response) {
        // console.log(response);
    });
};