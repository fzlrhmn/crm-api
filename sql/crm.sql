/* SQLEditor (Postgres)*/

CREATE TABLE crm_privileges_table
(
id SERIAL NOT NULL UNIQUE,
id_user_detail INT DEFAULT NULL,
id_crm_privileges INT DEFAULT NULL,
updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
created_date TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE facebook_comment_processed
(
id SERIAL NOT NULL,
comment_id VARCHAR(255) DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE master_crm_privileges
(
id SERIAL NOT NULL UNIQUE,
privileges_name VARCHAR(255) DEFAULT NULL,
description VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE master_privileges
(
id SERIAL NOT NULL UNIQUE,
privileges_name VARCHAR(255) DEFAULT NULL,
description VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE master_report_category
(
id SERIAL NOT NULL UNIQUE,
category_name VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE master_report_source
(
id SERIAL NOT NULL UNIQUE,
source_name VARCHAR(255) DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
deleted_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE master_report_status
(
id SERIAL NOT NULL UNIQUE,
status_name VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE master_skpd
(
id SERIAL NOT NULL UNIQUE,
skpd_code VARCHAR(45) DEFAULT NULL UNIQUE,
skpd_name TEXT,
id_unit VARCHAR(45) DEFAULT NULL,
id_group INT DEFAULT NULL,
active CHAR(1) DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE master_user_group
(
id SERIAL NOT NULL UNIQUE,
group_name VARCHAR(255) DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
deleted_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE master_user_level
(
id SERIAL NOT NULL UNIQUE,
level_name VARCHAR(255) DEFAULT NULL,
description TEXT,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
deleted_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE news_twitter_processed
(
id SERIAL NOT NULL,
tweet_id VARCHAR(255) DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE report_location
(
id SERIAL NOT NULL UNIQUE,
id_report_source INT DEFAULT NULL,
id_user VARCHAR(255) DEFAULT NULL,
title VARCHAR(255) DEFAULT NULL,
content TEXT,
id_master_source INT DEFAULT NULL,
source_detail VARCHAR(255) DEFAULT NULL,
latitude FLOAT DEFAULT NULL,
longitude FLOAT DEFAULT NULL,
the_geom GEOMETRY DEFAULT NULL,
id_category INT DEFAULT NULL,
created_date DATETIME DEFAULT NULL,
created_by VARCHAR(45) DEFAULT NULL,
id_photo INT DEFAULT NULL,
inserted_date TIMESTAMP DEFAULT NULL,
id_kelurahan INT DEFAULT NULL,
completed_date TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE report_location_status_log
(
id INT NOT NULL UNIQUE,
id_report_location INT DEFAULT NULL,
timestamp DATETIME NOT NULL,
id_master_status INT DEFAULT NULL,
id_user_detail INT DEFAULT NULL,
comment INT DEFAULT NULL,
file INT DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE token_access
(
id SERIAL NOT NULL UNIQUE,
report_source VARCHAR(45) DEFAULT NULL,
type VARCHAR(255) DEFAULT NULL,
query VARCHAR(255) DEFAULT NULL,
token VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE twitter_cron_log
(
id SERIAL NOT NULL,
part VARCHAR(255) DEFAULT NULL,
count_inserted BIGINT DEFAULT NULL,
updated_at TIMESTAMP DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id)
);

CREATE TABLE twitter_keyword
(
id SERIAL NOT NULL,
keyword VARCHAR(255) DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
deleted INT DEFAULT '0',
id_user INT DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE twitter
(
id SERIAL NOT NULL,
tweet_id VARCHAR(255) DEFAULT NULL UNIQUE,
username_id VARCHAR(255) DEFAULT NULL,
screen_name VARCHAR(255) DEFAULT NULL,
fullname VARCHAR(255) DEFAULT NULL,
avatar TEXT,
tweet_created_at TIMESTAMP DEFAULT NULL,
text TEXT,
twitter_media TEXT,
id_twitter_keyword INT DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
processed INT DEFAULT '0',
PRIMARY KEY (id)
);

CREATE TABLE twitter_news_source
(
id SERIAL NOT NULL,
screen_name VARCHAR(255) DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
deleted_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE user_detail
(
id SERIAL NOT NULL UNIQUE,
full_name VARCHAR(255) DEFAULT NULL,
username VARCHAR(255) DEFAULT NULL,
password VARCHAR(255) DEFAULT NULL,
nir_nik VARCHAR(255) DEFAULT NULL,
token VARCHAR(255) DEFAULT NULL,
email VARCHAR(255) DEFAULT NULL,
phone VARCHAR(255) DEFAULT NULL,
latitude FLOAT DEFAULT NULL,
longitude FLOAT DEFAULT NULL,
key_session VARCHAR(255) DEFAULT NULL,
id_kelurahan INT DEFAULT NULL,
alamat VARCHAR(255) DEFAULT NULL,
gcm_key VARCHAR(255) DEFAULT NULL,
id_user_level INT DEFAULT '0',
id_skpd INT DEFAULT NULL,
id_group INT DEFAULT '0',
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
deleted_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE privileges_table
(
id SERIAL NOT NULL UNIQUE,
id_user_detail INT DEFAULT NULL,
id_privileges INT DEFAULT NULL,
updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
created_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE report_non_location
(
id SERIAL NOT NULL UNIQUE,
id_skpd INT DEFAULT NULL,
id_master_status INT DEFAULT NULL,
title VARCHAR(255) DEFAULT NULL,
content TEXT,
id_source INT DEFAULT NULL,
source_detail TEXT,
id_category INT DEFAULT NULL,
created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
created_by VARCHAR(255) NOT NULL DEFAULT '',
created_by_id VARCHAR(255) DEFAULT NULL,
validated_by INT NOT NULL,
sentiment VARCHAR(1) DEFAULT NULL,
url VARCHAR(255) DEFAULT NULL,
media VARCHAR(255) DEFAULT NULL,
avatar TEXT,
id_user INT DEFAULT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
deleted_at TIMESTAMP DEFAULT NULL,
completed_at TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE report_non_location_status_log
(
id SERIAL NOT NULL UNIQUE,
id_report_non_location INT DEFAULT NULL,
id_skpd INT DEFAULT NULL,
comment TEXT,
id_master_status INT DEFAULT NULL,
id_user_detail INT DEFAULT NULL,
assigner INT DEFAULT NULL,
media VARCHAR(255) DEFAULT NULL,
created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT NULL,
deleted_at TIMESTAMP DEFAULT NULL,
timestamp TIMESTAMP DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE user_login_log
(
id SERIAL NOT NULL UNIQUE,
id_user_detail INT DEFAULT NULL,
ip_address VARCHAR(255) DEFAULT NULL,
timestamp INT DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE INDEX skpdnameFT ON master_skpd (skpd_name);

CREATE INDEX id_report_location_idx ON report_location_status_log (id_report_location);

CREATE INDEX id_master_0 ON report_location_status_log (id_master_status);

CREATE INDEX id_user_0 ON report_location_status_log (id_user_detail);

CREATE INDEX TweetId_UsernameId ON twitter (tweet_id,username_id);

CREATE INDEX IdTwitterKeywordFK ON twitter (id_twitter_keyword);

ALTER TABLE twitter ADD CONSTRAINT `IdTwitterKeywordFK` FOREIGN KEY (id_twitter_keyword) REFERENCES twitter_keyword (id);

CREATE INDEX user_level_id_idxfk ON user_detail (id_user_level);

CREATE INDEX skpd_id_idxfk ON user_detail (id_skpd);

CREATE INDEX group_id_idxfk ON user_detail (id_group);

ALTER TABLE user_detail ADD CONSTRAINT `user_detail_skpd_fk` FOREIGN KEY (id_skpd) REFERENCES master_skpd (id);

CREATE INDEX user_detail_id_idxfk ON privileges_table (id_user_detail);

CREATE INDEX privileges_id_idxfk ON privileges_table (id_privileges);

ALTER TABLE privileges_table ADD CONSTRAINT `privileges_table_ibfk_1` FOREIGN KEY (id_user_detail) REFERENCES user_detail (id);

ALTER TABLE privileges_table ADD CONSTRAINT `privileges_table_ibfk_2` FOREIGN KEY (id_privileges) REFERENCES master_privileges (id);

CREATE INDEX source_id_idxfk ON report_non_location (id_source);

CREATE INDEX category_id_idxfk ON report_non_location (id_category);

CREATE INDEX report_non_location_ibfk_3 ON report_non_location (id_user);

CREATE INDEX validated_0 ON report_non_location (validated_by);

ALTER TABLE report_non_location ADD CONSTRAINT `report_non_location_ibfk_1` FOREIGN KEY (id_source) REFERENCES master_report_source (id);

ALTER TABLE report_non_location ADD CONSTRAINT `report_non_location_ibfk_2` FOREIGN KEY (id_category) REFERENCES master_report_category (id);

ALTER TABLE report_non_location ADD CONSTRAINT `report_non_location_ibfk_4` FOREIGN KEY (validated_by) REFERENCES user_detail (id);

ALTER TABLE report_non_location ADD CONSTRAINT `report_non_location_ibfk_3` FOREIGN KEY (id_user) REFERENCES user_detail (id);

CREATE INDEX report_non_location_id_idxfk ON report_non_location_status_log (id_report_non_location);

CREATE INDEX master_status_id_idxfk ON report_non_location_status_log (id_master_status);

CREATE INDEX user_detail_id_idxfk_1 ON report_non_location_status_log (id_user_detail);

CREATE INDEX report_non_location_status_log_ibfk_skpd ON report_non_location_status_log (id_skpd);

CREATE INDEX report_non_location_status_log_assigner ON report_non_location_status_log (assigner);

ALTER TABLE report_non_location_status_log ADD CONSTRAINT `report_non_location_status_log_ibfk_1` FOREIGN KEY (id_report_non_location) REFERENCES report_non_location (id);

ALTER TABLE report_non_location_status_log ADD CONSTRAINT `report_non_location_status_log_ibfk_skpd` FOREIGN KEY (id_skpd) REFERENCES master_skpd (id);

ALTER TABLE report_non_location_status_log ADD CONSTRAINT `report_non_location_status_log_ibfk_2` FOREIGN KEY (id_master_status) REFERENCES master_report_status (id);

ALTER TABLE report_non_location_status_log ADD CONSTRAINT `report_non_location_status_log_ibfk_3` FOREIGN KEY (id_user_detail) REFERENCES user_detail (id);

ALTER TABLE report_non_location_status_log ADD CONSTRAINT `report_non_location_status_log_assigner` FOREIGN KEY (assigner) REFERENCES user_detail (id);

CREATE INDEX user_detail_id_idxfk_2 ON user_login_log (id_user_detail);
