/**
 * Created by fzlrhmn on 8/25/16.
 */

var cacheManager    = require('cache-manager');
var redisStore      = require('cache-manager-redis');
var logger          = require('../config/log');

var redisCache = cacheManager.caching({
    store: redisStore,
    host: 'localhost', // default value
    port: 6379, // default value
    db: 0
});

//Option for short time
let option = {};
option.ttl = 45;

// Options for LongTimeDuration
let optionLong = {};
optionLong.ttl = 86400;

// listen for redis connection error event
redisCache.store.events.on('redisError', function(error) {
    // handle error here
    console.log(error);
    logger.log('error','error redis connection' + error);
});

let reportCache = {
    getCache : function (key) {
        return new Promise((resolve, reject) => {
            redisCache.get(key,  function(err, result) {
                if (err) {
                    logger.log('error','error redis get cache' + err);
                    reject(err);
                }else{
                    if (result !== null) {
                        resolve(result);
                    }else{
                        reject(null);
                    }
                }
            });
        });
    },
    setCache : function (key,object) {
        redisCache.set(key, object, option,function(err) {
            if (err) {
                logger.log('error','error redis set cache' + err);
            }else{
                console.log('cache set : '+ key);
            }
        })
    },
    deleteCache : function (key) {
        redisCache.del(key,function(err) {
            if (err) {
                logger.log('error','error redis delete cache' + err);
            }else{
                //console.log('cache deleted : ' + key);
            }
        });
    },
    setCacheLongDuration : function (key,object) {
        redisCache.set(key, object, optionLong,function(err) {
            if (err) {
                logger.log('error','error redis set cache' + err);
            }else{
                console.log('cache set long duration : ' + key);
            }
        })
    },
    setCacheInfiniteDuration : function (key,object) {
        redisCache.set(key, object, {ttl : 0}, function(err) {
            if (err) {
                logger.log('error','error redis set cache' + err);
            }else{
                console.log('cache set infinite duration : ' + key);
            }
        })
    }
};

module.exports = reportCache;