var moment 		            = require('moment');
var config                  = require('../config/db_knex');
var knex                    = require('knex')(config);
let move                    = require('../helpers/move');
let fs                      = require('fs');
let path                    = require('path');
var logger                  = require('../config/log');
let md5                     = require('md5');
let queuePushNotification   = require('../queue/queue-push-notification');

moment().format();
moment.locale('id');

var report = {
    /*
     * Construct parameter from req.decoded and req.params from controller
     * @param {Object} decoded
     * @param {Object} param
     * @param {string} status
     *
     * construct parameter for where condition query
     *
     */
    paramsConstructor : function(decoded, param,status) {
        let params = {};
        params.id_user      = decoded.id_user;
        params.id_skpd      = decoded.id_skpd;
        params.user_level   = decoded.id_user_level;
        params.page         = param.page;

        // filter params
        params.keyword      = param.keyword || null;
        params.skpd         = param.skpd || null;
        params.date         = param.date || null;
        params.id_report    = param.id_report || null;
        params.status       = status;

        // hash params
        params.hash         = md5(JSON.stringify(params));

        return params;
    },
    /*
     * Get Open Report From Report Non Location / ROP
     * @param {Object} params
     *
     * get open report with specified parameter in decoded token
     *
     */
    getOpenReport : function (params) {
        return new Promise((resolve, reject) => {
            var recordLimit = 10;
            if (params.page) {
                var page = parseInt(params.page) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }
            knex.select(
                'report_non_location.id',
                'report_non_location.title',
                'report_non_location.created_date',
                'report_non_location.id_master_status',
                'report_non_location.media',
                'master_report_category.category_name',
                'master_skpd.skpd_name',
                'master_report_source.source_name',
                'user_detail.full_name',
                'master_report_status.status_name')
                .from('report_non_location')
                .innerJoin('master_report_source'   , 'report_non_location.id_source'       , 'master_report_source.id')
                .innerJoin('master_report_status'   , 'report_non_location.id_master_status', 'master_report_status.id')
                .innerJoin('master_skpd'            , 'report_non_location.id_skpd'         , 'master_skpd.id')
                .innerJoin('master_report_category' , 'report_non_location.id_category'     , 'master_report_category.id')
                .innerJoin('user_detail'            , 'report_non_location.validated_by'    , 'user_detail.id')
                .whereIn( 'report_non_location.id_master_status', [2,4,5] )
                .where(function () {
                    //logic for get by dinas, kelurahan, kecamatan, kota put here
                    if (params.user_level > 3){
                        this.where('report_non_location.id_skpd', params.id_skpd)
                    }

                    if (params.date !== null) {
                        this.where(knex.raw('DATE(`report_non_location`.`created_date`) = \'' + params.date+'\''));
                    }

                    if (params.id_report !== null) {
                        this.where('report_non_location.id', params.id_report);
                    }

                    if (params.keyword !== null) {
                        this.where(function() {
                            this.where('report_non_location.title', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.content', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.source_detail', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.source_name', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.created_by', 'like','%'+params.keyword+'%')
                        });
                    }

                    if (params.skpd !== null) {
                        this.where('report_non_location.id_skpd', params.skpd);
                    }
                })
                .limit(recordLimit)
                .offset(offset)
                .orderBy('created_date', 'desc')
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.created_date)
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.count_data     = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        })
    },
    /*
     * Get Process Report From Report Non Location / ROP
     * @param {Object} params
     *
     * get process report with specified parameter in decoded token
     *
     */
    getProcessReport : function (params) {
        return new Promise((resolve, reject) => {
            var recordLimit = 10;
            if (params.page) {
                var page = parseInt(params.page) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }
            knex.select(
                'report_non_location.id',
                'report_non_location.title',
                'report_non_location.created_date',
                'report_non_location.id_master_status',
                'report_non_location.media',
                'master_report_category.category_name',
                'master_skpd.skpd_name',
                'master_report_source.source_name',
                'user_detail.full_name',
                'master_report_status.status_name')
                .from('report_non_location')
                .innerJoin('master_report_source'   , 'report_non_location.id_source'       , 'master_report_source.id')
                .innerJoin('master_report_status'   , 'report_non_location.id_master_status', 'master_report_status.id')
                .innerJoin('master_skpd'            , 'report_non_location.id_skpd'         , 'master_skpd.id')
                .innerJoin('master_report_category' , 'report_non_location.id_category'     , 'master_report_category.id')
                .innerJoin('user_detail'            , 'report_non_location.validated_by'    , 'user_detail.id')
                .whereIn( 'report_non_location.id_master_status', [3] )
                .where(function () {
                    //logic for get by dinas, kelurahan, kecamatan, kota put here
                    if (params.user_level > 3){
                        this.where('report_non_location.id_skpd', params.id_skpd)
                    }

                    if (params.date !== null) {
                        this.where(knex.raw('DATE(`report_non_location`.`created_date`) = \'' + params.date+'\''));
                    }

                    if (params.id_report !== null) {
                        this.where('report_non_location.id', params.id_report);
                    }

                    if (params.keyword !== null) {
                        this.where(function() {
                            this.where('report_non_location.title', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.content', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.source_detail', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.source_name', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.created_by', 'like','%'+params.keyword+'%')
                        });
                    }

                    if (params.skpd !== null) {
                        this.where('report_non_location.id_skpd', params.skpd);
                    }
                })
                .limit(recordLimit)
                .offset(offset)
                .orderBy('created_date', 'desc')
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.created_date)
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.count_data     = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        })
    },
    /*
     * Get Complete Report From Report Non Location / ROP
     * @param {Object} params
     *
     * get complete report with specified parameter in decoded token
     *
     */
    getCompleteReport : function (params) {
        return new Promise((resolve, reject) => {
            var recordLimit = 10;
            if (params.page) {
                var page = parseInt(params.page) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }
            knex.select(
                'report_non_location.id',
                'report_non_location.title',
                'report_non_location.created_date',
                'report_non_location.id_master_status',
                'report_non_location.media',
                'master_report_category.category_name',
                'master_skpd.skpd_name',
                'master_report_source.source_name',
                'user_detail.full_name',
                'master_report_status.status_name')
                .from('report_non_location')
                .innerJoin('master_report_source'   , 'report_non_location.id_source'       , 'master_report_source.id')
                .innerJoin('master_report_status'   , 'report_non_location.id_master_status', 'master_report_status.id')
                .innerJoin('master_skpd'            , 'report_non_location.id_skpd'         , 'master_skpd.id')
                .innerJoin('master_report_category' , 'report_non_location.id_category'     , 'master_report_category.id')
                .innerJoin('user_detail'            , 'report_non_location.validated_by'    , 'user_detail.id')
                .whereIn( 'report_non_location.id_master_status', [6] )
                .where(function () {
                    //logic for get by dinas, kelurahan, kecamatan, kota put here
                    if (params.user_level > 3){
                        this.where('report_non_location.id_skpd', params.id_skpd)
                    }

                    if (params.date !== null) {
                        this.where(knex.raw('DATE(`report_non_location`.`created_date`) = \'' + params.date+'\''));
                    }

                    if (params.id_report !== null) {
                        this.where('report_non_location.id', params.id_report);
                    }

                    if (params.keyword !== null) {
                        this.where(function() {
                            this.where('report_non_location.title', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.content', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.source_detail', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.source_name', 'like','%'+params.keyword+'%')
                                .orWhere('report_non_location.created_by', 'like','%'+params.keyword+'%')
                        });
                    }

                    if (params.skpd !== null) {
                        this.where('report_non_location.id_skpd', params.skpd);
                    }
                })
                .limit(recordLimit)
                .offset(offset)
                .orderBy('created_date', 'desc')
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.created_date)
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.count_data     = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        })
    },
    /*
     * Get Detail Report From Report Non Location / ROP
     * @param {Object} params
     *
     * get detail report with specified parameter in decoded token
     *
     */
    getDetailReport: function(params) {
        return new Promise((resolve, reject) => {
            knex.select(
                'report_non_location.*',
                'master_report_source.source_name',
                'master_report_status.status_name',
                'user_detail.username',
                'master_report_category.category_name')
                .from('report_non_location')
                .innerJoin('master_report_source',  'report_non_location.id_source'         , 'master_report_source.id')
                .innerJoin('master_report_status',  'report_non_location.id_master_status'  , 'master_report_status.id')
                .innerJoin('user_detail',  'report_non_location.validated_by'  , 'user_detail.id')
                .innerJoin('master_report_category',  'report_non_location.id_category'  , 'master_report_category.id')
                .where('report_non_location.id', params.id_report)
                .where(function () {
                    if (params.user_level > 3){
                        //logic for get by dinas, kelurahan, kecamatan, kota put here
                    }
                })
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.created_date)
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.data         = results[0];

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    /*
     * Get Detail Log Report From Report Non Location / ROP
     * @param {Integer} id_report
     *
     * get detail log report with specified parameter in decoded token
     *
     */
    getDetailLogReport: function(id_report) {
        return new Promise((resolve, reject) => {
            knex.select(
                'report_non_location_status_log.id',
                'report_non_location_status_log.created_at',
                'report_non_location_status_log.comment',
                'report_non_location_status_log.id_skpd',
                'report_non_location_status_log.media as file',
                ' master_report_status.status_name',
                ' master_skpd.skpd_name',
                ' assigners.username as assigner',
                ' workers.username as worker')
                .from('report_non_location_status_log')
                .innerJoin('master_report_status'       , 'report_non_location_status_log.id_master_status' , 'master_report_status.id')
                .innerJoin('master_skpd'            , 'report_non_location_status_log.id_skpd'          , 'master_skpd.id')
                .leftJoin('user_detail as assigners'    , 'report_non_location_status_log.assigner'         , 'assigners.id')
                .leftJoin('user_detail as workers'      , 'report_non_location_status_log.id_user_detail'   , 'workers.id')
                .where('report_non_location_status_log.id_report_non_location', id_report)
                .orderBy('id', 'asc')
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.timestamp);
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.data         = results;
                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        })
    },
    /*
     * Processing Report e.g process, dispatch, coordination
     * @param {Object} params
     *
     * Set Report Progress like process, dispatch, and coordination
     *
     */
    setProcessReport : function (params, status) {

        return new Promise((resolve, reject) => {
            knex.insert(params)
                .into('report_non_location_status_log')
                .then(function (results) {
                    let payload = {};
                    payload.id_report   = params.id_report_non_location;
                    payload.id_skpd     = params.id_skpd;
                    payload.channel     = 'rop';
                    payload.heading     = 'Notifikasi Laporan ROP';
                    payload.status      = 'coordination';

                    if (status === 'dispatch') {
                        payload.message = "Anda Menerima Disposisi Laporan ROP #" + params.id_report_non_location;
                    }
                    if (status === 'coordination') {
                        payload.message = "Anda Menerima Koordinasi Laporan ROP #" + params.id_report_non_location;
                    }
                    if (status === 'process') {
                        payload.message = "Laporan ROP #" + params.id_report_non_location+ " Sedang Dikerjakan";
                    }

                    queuePushNotification(payload);
                    resolve(results);
                })
                .catch(function(error) {
                    reject(error);
                });
        })
    },
    /*
     * Set Complete Report
     * @param {Object} params
     *
     * Set Report Complete with an image
     *
     */
    setComplete : function (params) {
        return new Promise((resolve, reject) => {
            let new_filename = (Math.random().toString(36)+'00000000000000000').slice(2, 10) + Date.now() + path.extname(params.file_name);
            let old_path = params.file_path;
            let new_path = '../cdn/images/crm/' + new_filename;
            move(old_path, new_path, function(err) {
                if (err) {
                    logger.log('error', err);
                    reject(err);
                }else{
                    let payload = {
                        id_report_non_location  : params.id_report_non_location,
                        id_user_detail          : params.id_user_detail,
                        id_skpd                 : params.id_skpd,
                        comment                 : params.comment,
                        media                   : new_filename,
                        id_master_status        : 6,
                        completed_at            : moment().format("YYYY-MM-DD HH:mm:ss")
                    };
                    knex.insert(payload)
                        .into('report_non_location_status_log')
                        .then(function (results) {
                            queuePushNotification({
                                id_report   : params.id_report_non_location,
                                id_skpd     : params.id_skpd,
                                channel     : 'rop',
                                heading     : 'Notifikasi Laporan ROP',
                                message     : "Laporan #" + params.id_report_non_location+ " Telah Selesai Diproses",
                                status      : 'coordination'
                            });
                            resolve(results);
                        })
                        .catch(function(error) {
                            reject(error);
                        });
                }
            });
        });
    }
};

module.exports = report;