/**
 * Created by fzlrhmn on 8/9/16.
 */
var moment 		= require('moment');
var config      = require('../config/db_knex');
var knex        = require('knex')(config);
var logger      = require('../config/log');
var push        = require('../helpers/push');

moment().format();
moment.locale('id');

var user = {
    /**
     * Get list of user
     *
     */
    getUser : function() {
        return new Promise((resolve, reject) => {
                knex.select('id','username')
                    .from('user_detail')
                    .then(function (results) {
                        var arrayId = [];
                        results.forEach(function(item){
                            arrayId.push(item.id);
                        });
                        resolve(arrayId);
                    })
                    .catch(function(error) {
                        logger.log('error', 'error get user notification : ' + error);
                        reject(error);
                    })
            });
    },
    /**
     * Get user app_id for notification
     *
     * get list of user that have app_id for notification based on role
     *
     * @param param
     * @param status
     * @param callback
     */
    getNotificationUser: function(param, status, callback) {
        knex.select('user_notification.app_id','user_notification.user_id')
            .rightJoin('user_notification', 'user_notification.user_id', 'user_detail.id')
            .leftJoin('master_skpd','master_skpd.id','user_detail.id_skpd')
            .where(function() {
                if (status === 'wait') {
                    this.where('master_skpd.id_wilayah',param);
                    this.where('user_detail.id_user_level','8');
                }
                if (status === 'dispatch') {
                    this.where('user_notification.user_id',param);
                }
                if (status === 'coordination') {
                    this.where('user_detail.id_user_level','in',[4,6,8]);
                    this.where('user_detail.id_skpd',param);
                }
                if (status === 'process') {
                    this.where('user_detail.id_user_level','in',[4,8]);
                    this.where('user_detail.id_skpd',param);
                }
                if (status === 'complete') {
                    this.where('user_detail.id_user_level','in',[4,8]);
                    this.where('user_detail.id_skpd',param);
                }
                if (status === 'all_call_center') {
                    this.where('user_detail.id_user_level','in',[4,6,9]);
                    this.where('user_detail.id_skpd',param);
                }
            })
            .from('user_detail')
            .then(function(results) {
                let object = {};
                object.appIdArray = [];
                object.userId = [];

                results.forEach(function(item) {
                    object.appIdArray.push(item.app_id);
                    object.userId.push(item.user_id);
                });
                callback(null,object);
            })
            .catch(function(error) {
                callback(error,null);
            });
    },
    /**
     * Set notification
     * @param {Object} data
     *
     * set notification for user
     *
     */
    setNotification : function (data) {
        return new Promise((resolve, reject) => {
            knex
                .insert(data)
                .into('notification')
                .then(res => {
                    resolve(res);
                })
                .catch(error => {
                    reject(error);
                })
        });
    },
    /**
     * Get notification for specified user
     *
     * get notification for specified user
     *
     * @param params
     */
    getNotification : function (params) {
        return new Promise((resolve, reject) => {
            let recordLimit = 10;
            if (params.page) {
                var page = parseInt(params.page) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }

            knex
                .select('notification.id', 'notification.id_report', 'notification.user_id', 'notification.title as notification_title', 'notification.message as notification_message','notification.created_at','notification.read_at','report_location.media_format','report_location.media','master_report_status.status_name')
                .from('notification')
                .leftJoin('report_location','notification.id_report','report_location.id_report_source')
                .leftJoin('master_report_status','report_location.id_master_status','master_report_status.id')
                .where('user_id', params.user_id)
                .limit(recordLimit)
                .offset(offset)
                .orderBy('created_at', 'desc')
                .then(results => {
                    let response = {};
                    results.forEach(function(item) {
                        let created_timestamp = moment(item.created_at);
                        item.created_timestamp = created_timestamp.format("Do MMMM YYYY HH:mm:ss");

                        if (item.read_at !== null) {
                            let read_timestamp = moment();
                            item.read_timestamp = read_timestamp.format("Do MMMM YYYY HH:mm:ss");
                        }else{
                            item.read_timestamp = null;
                        }
                    });
                    response.count_data     = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                })
        });
    },
    /**
     * Set notification has been read by report_id and user_id
     * @param {Object} data
     *
     * set notification that already read by set read_at by current timestamp
     *
     */
    readNotification : function (data) {
        return new Promise((resolve, reject) => {
            let updateData = {
                read_at : moment().format("YYYY-MM-DD HH:mm:ss")
            };
            knex('notification')
                .update(updateData)
                .where('id', data.id)
                .then(res => {
                    resolve(res);
                })
                .catch(error => {
                    reject(error);
                })
        });
    }
};

module.exports = user;