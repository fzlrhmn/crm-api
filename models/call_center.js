/**
 * Created by fzlrhmn on 9/29/16.
 */

let moment 		            = require('moment');
let config                  = require('../config/db_knex');
let knex                    = require('knex')(config);
let fs                      = require('fs');
let async                   = require('async');

require('dotenv').config();
moment().format();
moment.locale('id');

let callCenterData = {
    validateDataReport : function(params) {
        return new Promise((resolve, reject) => {
            async.parallel([
                /**
                 * Validate ticket number
                 **/
                function(callback) {
                    if (params.ticket === undefined || params.ticket === null || params.ticket === '') {
                        callback(null, 'Ticket is required');
                    }else{
                        callback(null, null);
                    }
                },
                /**
                 * Validate latitude
                 **/
                function(callback) {
                    if (typeof params.latitude === 'number') {
                        if ( Number.isInteger(params.latitude) ) {
                            callback(null, 'Latitude is not valid');
                        }else{
                            callback(null,null);
                        }
                    }else{
                        callback(null, 'Latitude is not valid');
                    }
                },
                /**
                 * Validate longitude
                 **/
                function(callback) {
                    if (typeof params.longitude === 'number') {
                        if ( Number.isInteger(params.longitude) ) {
                            callback(null, 'Longitude is not valid');
                        }else{
                            callback(null,null);
                        }
                    }else{
                        callback(null, 'Longitude is not valid');
                    }
                },
                /**
                 * Validate status
                 **/
                function(callback) {
                    if (params.status === undefined || params.status === null || params.status === '') {
                        callback(null, 'Status is required');
                    }else{
                        callback(null, null);
                    }
                },
                /**
                 * Validate created date timestamp
                 **/
                function(callback) {
                    if ( moment(params.created_date, 'YYYY-MM-DD HH:mm:ss',true).isValid() !== true) {
                        callback(null, 'Not a valid date');
                    }else{
                        callback(null, null);
                    }
                },

                /**
                 * Validate SKPD
                 **/
                function(callback) {
                    if( params.skpd === undefined || params.skpd.length < 1 || params.skpd.length === null || params.skpd.length === '' ) {
                        callback(null, 'SKPD is required');
                    }else{
                        callback(null, null);
                    }
                },
                /**
                 * Validate Kota
                 **/
                function(callback) {
                    if( typeof params.city === 'number') {
                        knex('master_kota')
                            .count('kode_kota as kode_kota')
                            .where('kode_kota', params.city)
                            .then(result => {
                                if (result[0].kode_kota === 0 ) {
                                    callback(null, 'City is required or wrong city code');
                                }else{
                                    callback(null, null);
                                }
                            })
                            .catch(error => {
                                callback(null, error);
                            });
                    }else{
                        callback(null , 'City must be in number');
                    }
                },
                /**
                 * Validate kecamatan
                 **/
                function(callback) {
                    if( typeof params.kecamatan === 'number') {
                        knex('master_kecamatan')
                            .count('kode_kecamatan as kode_kecamatan')
                            .where('kode_kecamatan', params.kecamatan)
                            .then(result => {
                                if (result[0].kode_kecamatan === 0 ) {
                                    callback(null, 'Kecamatan is required or wrong kecamatan code');
                                }else{
                                    callback(null, null);
                                }
                            })
                            .catch(error => {
                                callback(null, error);
                            });
                    }else{
                        callback(null , 'Kecamatan must be in number');
                    }
                }
            ],
                /**
                 * Final function
                 **/
                function(error, result) {

                let errorArray = result.filter((e) => e !== null);

                if (errorArray.length > 0) {
                    reject(errorArray);
                }else{
                    knex
                        .select('id_child')
                        .from('master_skpd_hierarchy')
                        .whereIn('id_parent', params.skpd)
                        .then(result => {
                            let skpdData = result.map(e => {
                                return e.id_child;
                            });

                            let allSkpdData = params.skpd.concat(skpdData);
                            resolve(allSkpdData);
                        })
                        .catch(error => {
                            reject(error);
                        });
                }
            })
        });
    },
    validateDataReportLog : function(params) {
        return new Promise((resolve, reject) => {
            async.parallel([
                /**
                 * Validate ticket
                 *
                 * @param callback
                 */
                function(callback) {
                    if (params.ticket === undefined || params.ticket === null || params.ticket === '') {
                        callback(null, 'Ticket is required');
                    }else{
                        knex('report_call_center')
                            .count('ticket as ticket')
                            .where('ticket', params.ticket)
                            .then(result => {
                                if (result[0].ticket === 0 ) {
                                    callback(null, 'Ticket not found');
                                }else{
                                    callback(null, null);
                                }
                            })
                            .catch(error => {
                                callback(null, error);
                            });
                    }
                },
                /**
                 * Validate status
                 *
                 * @param callback
                 */
                    function(callback) {
                        if (params.status === undefined || params.status === null || params.status === '') {
                            callback(null, 'Status is required');
                        }else{
                            if ( ['OPEN', 'ON PROGRESS', 'CLOSED'].indexOf( params.status.toUpperCase() ) === -1 ) {
                                callback(null, 'Status not found');
                            } else {
                                callback(null, null);
                            }
                        }
                    },
                /**
                 * Validate sub status
                 *
                 * @param callback
                 */
                    function(callback) {
                        if (params.substatus === undefined || params.substatus === null || params.substatus === '') {
                            callback(null, 'Sub Status is required');
                        }else{
                            if ( ['PREPARATION', 'ON GOING', 'ON PROCESS', 'RESOLVED', 'CLOSED', 'CANCEL'].indexOf( params.substatus.toUpperCase() ) === -1 ) {
                                callback(null, 'Sub Status not found');
                            } else {
                                callback(null, null);
                            }
                        }
                    },
                /**
                 * Validate status and sub status relation
                 *
                 * @param callback
                 */
                    function(callback) {
                        if (params.substatus === undefined || params.substatus === null || params.substatus === '' || params.status === undefined || params.status === null || params.status === '') {
                            callback(null, 'Status or Sub Status is required');
                        } else {
                            if (
                                params.substatus.toUpperCase() === 'PREPARATION' && params.status.toUpperCase() !== 'ON PROGRESS' ||
                                params.substatus.toUpperCase() === 'ON GOING' && params.status.toUpperCase() !== 'ON PROGRESS' ||
                                params.substatus.toUpperCase() === 'ON PROCESS' && params.status.toUpperCase() !== 'ON PROGRESS' ||
                                params.substatus.toUpperCase() === 'RESOLVED' && params.status.toUpperCase() !== 'ON PROGRESS' ||
                                params.substatus.toUpperCase() === 'CANCEL' && params.status.toUpperCase() !== 'CLOSED' ||
                                params.substatus.toUpperCase() === 'CLOSED' && params.status.toUpperCase() !== 'CLOSED'
                            ) {
                                callback(null, 'Sub Status and Status not match');
                            }else{
                                callback(null, null);
                            }
                        }
                    },
                /**
                 * Validate status
                 *
                 * @param callback
                 */
                    function(callback) {
                        if(params.id_skpd === undefined || params.id_skpd === null || typeof params.id_skpd !== 'number') {
                            callback(null, 'SKPD is required or wrong skpd identifier');
                        }
                        else{
                            callback(null, null);
                        }
                    }
            ], function(error, result) {

                let errorArray = result.filter(function(e) {
                    return e !== null;
                });

                if (errorArray.length > 0) {
                    reject(errorArray);
                }else{
                    knex
                        .select('id_child')
                        .from('master_skpd_hierarchy')
                        .where('id_parent', params.id_skpd)
                        .then(result => {
                            let allSkpdData = result.map(e => {
                                return e.id_child;
                            });

                            allSkpdData.push(params.id_skpd);
                            resolve(allSkpdData);
                        })
                        .catch(error => {
                            reject(error);
                        });
                }
            })
        });
    },
    setCallCenter : function (params, skpd) {
        return new Promise ((resolve, reject) => {
            knex.insert(params)
                .into('report_call_center')
                .then(() => {

                    let data = [];
                    skpd.forEach(item => {
                        let ticket_skpd = {
                            ticket : params.ticket,
                            id_skpd : item
                        };

                        data.push(ticket_skpd);
                    });

                    return knex
                            .insert(data)
                            .into('report_call_center_skpd')
                })
                .then(() => {
                    return knex.insert({
                        ticket : params.ticket,
                        status : params.status,
                        latitude : params.latitude,
                        longitude : params.longitude,
                        created_at : params.created_date,
                        the_geom : knex.raw('ST_geomfromtext("POINT(' + params.longitude + ' ' + params.latitude + ')")'),
                        message : null
                    })
                    .into('report_call_center_status_log')
                })
                .then((result) => {
                    resolve(result);
                })
                .catch(error => {
                    let errorText = JSON.stringify(error);
                    reject(errorText);
                })
        });
    },
    setCallCenterLog : function(params) {
        return new Promise((resolve, reject) => {
            knex.insert(params)
                .into('report_call_center_status_log')
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    let errorText = JSON.stringify(error);
                    reject(errorText);
                })
        })
    },
    getCallCenterReport : function(skpd, count) {
        return new Promise ((resolve, reject) => {
            let recordLimit = 10;
            if (count) {
                var page = parseInt(count) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }
            knex.select(
                'report_call_center.ticket',
                'report_call_center.latitude',
                'report_call_center.longitude',
                'report_call_center.status',
                'report_call_center.substatus',
                'report_call_center.category',
                'report_call_center.subcategory',
                'report_call_center.location',
                'report_call_center.location_typo',
                'report_call_center.created_date',
                'master_kota.nama_kota',
                'master_kecamatan.nama_kecamatan',
                'master_skpd.skpd_name as nama_skpd'
                )
                .from('report_call_center')
                .innerJoin('master_kota','report_call_center.city','master_kota.kode_kota')
                .innerJoin('master_kecamatan','report_call_center.kecamatan','master_kecamatan.kode_kecamatan')
                .rightJoin('report_call_center_skpd','report_call_center.ticket','report_call_center_skpd.ticket')
                .leftJoin('master_skpd','report_call_center_skpd.id_skpd','master_skpd.id')
                .where('report_call_center_skpd.id_skpd',skpd)
                .limit(recordLimit)
                .offset(offset)
                .orderBy('report_call_center.updated_at','desc')
                .then((results) => {

                    results.forEach(function(item) {
                        let date = moment(item.created_date);
                        item.date = date.format("dddd Do MMMM YYYY HH:mm:ss");

                        delete item.created_date;
                    });

                    results.filter(function(e) {
                        return results.indexOf(parseInt(e.ticket)) == -1;
                    });

                    let data = {
                        count : results.length,
                        data : results
                    };

                    resolve(data);
                })
                .catch(err => {
                    let errorMessage = JSON.stringify(err);
                    reject(errorMessage);
                })
        })
    },
    getDetailCallCenterReport : function(ticket) {
        return new Promise((resolve, reject) => {
            async.waterfall([
                (callback) => {
                    knex.select(
                        'report_call_center.ticket',
                        'report_call_center.latitude',
                        'report_call_center.longitude',
                        'report_call_center.status',
                        'report_call_center.substatus',
                        'report_call_center.category',
                        'report_call_center.subcategory',
                        'report_call_center.location',
                        'report_call_center.location_typo',
                        'report_call_center.created_date',
                        'master_kota.nama_kota',
                        'master_kecamatan.nama_kecamatan'
                        )
                        .from('report_call_center')
                        .innerJoin('master_kota','report_call_center.city','master_kota.kode_kota')
                        .innerJoin('master_kecamatan','report_call_center.kecamatan','master_kecamatan.kode_kecamatan')
                        .where('report_call_center.ticket',ticket)
                        .limit(1)
                        .then((results) => {
                            let response = results[0];
                            results.forEach(function(item) {
                                let date = moment(item.created_date);
                                response.date = date.format("dddd Do MMMM YYYY HH:mm:ss");
                            });
                            callback(null, results[0]);
                        })
                        .catch(err => {
                            callback(err, null);
                        })
                },
                (data,callback) => {
                    return knex
                        .select(
                            'report_call_center_skpd.id_skpd',
                            'master_skpd.skpd_name as nama_skpd'
                        )
                        .from('report_call_center_skpd')
                        .leftJoin('master_skpd','report_call_center_skpd.id_skpd','master_skpd.id')
                        .where('report_call_center_skpd.ticket',ticket)
                        .then(results => {
                            let skpd = [];
                            results.forEach(item => {
                                skpd.push(item.nama_skpd);
                            });
                            data.skpd = skpd;
                            callback(null, data);
                        })
                        .catch(error => {
                            callback(error, null);
                        })
                }
            ], (error, results) => {
                if (error) {
                    reject(error);
                }else{
                    resolve(results);
                }
            })
        });
    },
    getCallCenterReportLog : function(ticket) {
        return new Promise ((resolve, reject) => {
            knex.select(
                'report_call_center_status_log.ticket',
                'report_call_center_status_log.latitude',
                'report_call_center_status_log.longitude',
                'report_call_center_status_log.status',
                'report_call_center_status_log.substatus',
                'report_call_center_status_log.message',
                'report_call_center_status_log.update_source',
                'report_call_center_status_log.created_at',
                'master_skpd.skpd_name as nama_skpd'
                )
                .from('report_call_center_status_log')
                .leftJoin('master_skpd','report_call_center_status_log.id_skpd','master_skpd.id')
                .where('report_call_center_status_log.ticket',ticket)
                .orderBy('report_call_center_status_log.created_at','asc')
                .then((results) => {

                    results.forEach(function(item) {
                        let date = moment(item.created_at);
                        item.date = date.format("dddd Do MMMM YYYY HH:mm:ss");

                        delete item.created_at;
                    });

                    let data = {
                        count : results.length,
                        data : results
                    };

                    resolve(data);
                })
                .catch(err => {
                    let errorMessage = JSON.stringify(err);
                    reject(errorMessage);
                })
        })
    },
    getCallCenterSkpd : () => {
        return new Promise((resolve, reject) => {
            knex.select(
                'master_skpd.id',
                'master_skpd.skpd_name',
                'master_skpd.active'
            )
                .from('master_skpd')
                .where('master_skpd.active', 0)
                .whereNot('master_skpd.active', null)
                .then(result => {
                    let data = result.map(e => {
                        return {
                            id : e.id,
                            nama_skpd : e.skpd_name,
                            is_active : true
                        }
                    });
                    resolve(data);
                })
                .catch(error => {
                    reject(error);
                })
        });
    }
};

module.exports = callCenterData;