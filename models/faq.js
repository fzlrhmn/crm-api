/**
 * Created by fzlrhmn on 10/10/16.
 */

let moment 		            = require('moment');
let config                  = require('../config/db_knex');
let knex                    = require('knex')(config);
let fs                      = require('fs');

require('dotenv').config();
moment().format();
moment.locale('id');

let faqData = {
    getFaq : () => {
        return new Promise((resolve, reject) => {
            knex.select(
                'id',
                'question',
                'answer'
                )
                .from('faq')
                .then(res => {
                    let response = res.map(e => {
                        return {
                            id : e.id,
                            question : e.question,
                            answer : e.answer
                        }
                    });
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                })
        })
    }
};

module.exports = faqData;