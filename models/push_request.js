/**
 * Created by fzlrhmn on 9/7/16.
 */
var async 		= require('async');

var sendNotification = function(data) {
    var message = {
        app_id: "863335bd-3f12-4af7-8cb6-7b1273224008",
        contents: {"en": data.message},
        headings:{"en" : "Notifikasi Laporan"},
        include_player_ids: ['c14ae24a-45b9-4233-bc6b-176ed25c1491'],
        data:{"id_report" : data.id_report,channel : "qlue"}
    };

    var headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic OGUwNmM5YTQtZTBkMC00NjlmLTg0ZTEtNzJkYmM4OTU3ZDNh"
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };

    var https = require('https');
    var req = https.request(options, function(res) {
        res.on('data', function(data) {
            console.log("Response:");
            console.log(JSON.parse(data));
        });
    });

    req.on('error', function(e) {
        console.log("ERROR:");
        console.log(e);
    });

    req.write(JSON.stringify(message));
    req.end();
};

let q = async.queue(function(task, callback) {

    sendNotification({
        message : 'ini pesan',
        id_report : 123
    });

    callback();
});

module.exports = q;