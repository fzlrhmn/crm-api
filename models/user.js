/**
 * Created by fzlrhmn on 8/23/16.
 */

let moment 		= require('moment');
let config      = require('../config/db_knex');
let knex        = require('knex')(config);
let logger      = require('../config/log');

require('dotenv').config();
moment().format();
moment.locale('id');

let user = {
    getUser : function(id_user) {
        return new Promise((resolve, reject) => {
            knex('user_detail')
                .select('id','full_name','phone','email','alamat','nir_nik')
                .where('id', id_user)
                .then((result) => {
                    resolve(result);
                })
                .catch((error) => {
                    logger.log('error', 'Error on ' + __filename + ' on getUser function : ' + error);
                    reject(error);
                })
        })
    },
    getUserDetail : function(id_user) {
        return new Promise((resolve, reject) => {
            knex('user_detail')
                .select(
                    'user_detail.id',
                    'user_detail.full_name',
                    'user_detail.username',
                    'user_detail.nir_nik',
                    'user_detail.id_user_level',
                    'user_detail.id_skpd',
                    'user_detail.id_group',
                    'user_detail.phone',
                    'user_detail.email',
                    'user_detail.alamat',
                    'master_skpd.skpd_name',
                    'master_skpd.id_wilayah',
                    knex.raw('master_user_level.description as level_name'),
                    knex.raw('master_user_group.group_name as group_name'))
                .join('master_user_level', 'user_detail.id_user_level', 'master_user_level.id')
                .join('master_user_group', 'user_detail.id_group', 'master_user_group.id')
                .join('master_skpd', 'user_detail.id_skpd', 'master_skpd.id')
                .where('user_detail.id', id_user)
                .then((result) => {
                    resolve(result);
                })
                .catch((error) => {
                    logger.log('error', 'Error on ' + __filename + ' on getUserDetail function : ' + error);
                    reject(error);
                })
        })
    },
    updateUser : function(payload) {
        return new Promise((resolve, reject) => {
            knex('user_detail')
                .where('id', payload.id)
                .update(payload)
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    logger.log('error', 'Error on ' + __filename + ' on updateUser function : ' + error);
                    reject(err);
                })
        })
    },
    getPassword : function(id_user) {
        return new Promise((resolve, reject) => {
            knex('user_detail')
                .select('id','password')
                .where('id', id_user)
                .then((result) => {
                    let data = {
                        id_user : result[0].id,
                        password : result[0].password
                    };

                    resolve(data);
                })
                .catch((error) => {
                    logger.log('error', 'Error on ' + __filename + ' on getPassword function : ' + error);
                    reject(error);
                })
        })
    },
    updatePassword : function(payload) {
        return new Promise((resolve, reject) => {
            knex('user_detail')
                .where('id', payload.id)
                .update(payload)
                .then((result) => {
                    resolve(result);
                })
                .catch((error) => {
                    logger.log('error', 'Error on ' + __filename + ' on updatePassword function : ' + error);
                    reject(err);
                })
        })
    },
    checkDeletedUser : function(id_user) {
        return new Promise((resolve, reject) => {
            knex('user_detail')
                .select('id','full_name','phone','email','alamat','nir_nik')
                .where('id', id_user)
                .where('deleted_at', null)
                .then((result) => {
                    resolve(result.length);
                })
                .catch((error) => {
                    logger.log('error', 'Error on ' + __filename + ' on getUser function : ' + error);
                    reject(error);
                })
        })
    },
};

module.exports = user;