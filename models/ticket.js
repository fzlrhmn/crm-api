/**
 * Created by fzlrhmn on 12/27/16.
 */

let async                   = require('async');
let moment 		            = require('moment');
let config                  = require('../config/db_knex');
let knex                    = require('knex')(config);

require('dotenv').config();
moment().format();
moment.locale('id');

let ticket = {
    /**
     *
     * @returns {Promise}
     */
    getKelurahan : () => {
        return new Promise((resolve, reject) => {
            knex
                .select(
                    'id_kelurahan',
                    'kode_kota',
                    'kode_kecamatan',
                    'kode_kelurahan',
                    'nama_kota',
                    'nama_kecamatan',
                    'nama_kelurahan',
                    knex.raw('ST_asgeojson(the_geom) as geojson')
                )
                .from('master_kelurahan')
                .then(result => {
                    resolve(result.map(item => {
                        return {
                            kode_kota : parseInt(item.kode_kota),
                            kode_kecamatan : parseInt(item.kode_kecamatan),
                            kode_kelurahan : parseInt(item.kode_kelurahan),
                            nama_kota : item.nama_kota,
                            nama_kecamatan : item.nama_kecamatan,
                            nama_kelurahan : item.nama_kelurahan,
                            geojson : JSON.parse(item.geojson)
                        }
                    }))
                })
                .catch(error => {
                    reject(error)
                })
        });
    },
    /**
     *
     * @returns {Promise}
     */
    getIssue : () => {
        return new Promise((resolve, reject) => {
            knex
                .select(
                    'id',
                    'issue_name',
                    'issue_code'
                )
                .from('master_report_issue')
                .then(result => {
                    resolve(result.map(item => {
                        return {
                            id : item.id,
                            issue : item.issue_name,
                            code : item.issue_code
                        }
                    }))
                })
                .catch(error => {
                    reject(error)
                })
        });
    },
    setIssue : (data, decoded) => {
        return new Promise((resolve, reject) => {
            switch (data.id_issue) {
                case 1 :
                    resolve(1);
                case 2 :
                    let issueData = {
                        ticket : data.issue_code + data.id_report,
                        id_report_location : data.id_report,
                        id_issue : data.id_issue,
                        id_user : decoded.id_user,
                        content : data.content,
                        latitude_proposal : data.latitude_proposal,
                        longitude_proposal : data.longitude_proposal,
                        address : data.address,
                        id_kota : data.kode_kota,
                        id_kecamatan : data.kode_kecamatan,
                        id_kelurahan : data.kode_kelurahan,
                        belongs_to_kecamatan : data.is_same_kecamatan
                    };

                    knex
                        .insert(issueData)
                        .into('report_location_ticket_issue')
                        .then(result => {
                            if (data.is_same_kecamatan === true ) {
                                // TODO find and send notification to kecamatan
                                resolve(result);
                            }
                        })
                        .catch(error => {
                            reject(error);
                        });
                case 3 :
                    resolve(3);
                case 4 :
                    resolve(4);
                case 5 :
                    resolve(5);
                default :
                    reject(new Error('Issue Not Found'));
            }
        });
    }
};

module.exports = ticket;