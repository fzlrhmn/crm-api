/**
 * Created by fzlrhmn on 1/3/17.
 */
let glob    = require('glob');
let R       = require('ramda');
let async   = require('async');
let fs      = require('fs');

require('dotenv').config();

let fileData = {
    getData : (date) => {
        return new Promise((resolve, reject) => {
            glob(process.env.LOG_FOLDER + 'qlue_' + date + '*.json', {}, (error, files) => {
                if (error) reject(error)
                resolve(files);
            })
        })
    },
    readAsync : (fileDir, callback) => {
        fs.readFile(fileDir, 'utf8', callback);
    }
};

module.exports = fileData;