/**
 * Created by fzlrhmn on 7/14/16.
 */
var moment 		= require('moment');
var config      = require('../config/db_knex');
var knex        = require('knex')(config);

moment().format();
moment.locale('id');

var skpd = {
    /**
     * Get List of SKPD from master_skpd table
     * @param {String} id_user_level
     *
     * get SKPD data from master_skpd table
     *
     */
    getSkpd : function (id_user_level) {
        return new Promise((resolve,reject) => {
            knex.select('id', 'skpd_code', 'skpd_name', 'id_unit', 'id_group', 'id_wilayah')
                .from('master_skpd')
                .where('active', 0)
                .where(function() {
                    if ( id_user_level === 8 || id_user_level === 5 ) {
                        this.where('master_skpd.id_group', '<>' , 4)
                    }
                })
                .orderBy('skpd_name', 'asc')
                .then(function (results) {
                    var response = {};
                    response.total = results.length;
                    response.data = results;
                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    /**
     * Get SKPD name from master_skpd table
     * @param {Object} params
     *
     * get SKPD data from master_skpd table
     *
     */
    getSkpdById : function (idSkpd) {
        return new Promise((resolve,reject) => {
            knex.select('id', 'skpd_code', 'skpd_name', 'id_unit', 'id_group', 'id_wilayah')
                .from('master_skpd')
                .where('active', 0)
                .where('id',idSkpd)
                .orderBy('skpd_name', 'asc')
                .then(function (results) {
                    var response = {};
                    response.total = results.length;
                    response.data = results;
                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    /**
     * Get List of kelurahan from master_kelurahan table
     * @param {Object} params
     *
     * get kelurahan data from table
     *
     */
    getKelurahan : function (params) {
        return new Promise((resolve,reject) => {
            knex.select('kode_kelurahan', knex.raw('UCASE(`nama_kelurahan`) as nama_kelurahan'))
                .from('master_kelurahan')
                .where('deleted', 0)
                .whereNot('nama_kota','KEPULAUAN SERIBU')
                .where(function () {
                    switch (params.user_level) {
                        case 5 :
                            this.where('kode_kota', params.id_wilayah);
                            break;
                        case 7 :
                            this.where('kode_kecamatan', params.id_wilayah);
                            break;
                    }
                })
                .orderBy('nama_kelurahan', 'asc')
                .then(function (results) {
                    var response = {};
                    response.total = results.length;
                    response.data = results;
                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    /**
     * Get List of SKPD workers from user_detail table with left join with report_location
     * @param {Object} params
     *
     * take workers data from database with total completion report today and on total on progress report
     *
     */
    getSkpdWorker : function (params) {
        return new Promise((resolve, reject) => {
            knex.select(
                'user_detail.id',
                'user_detail.username',
                'user_detail.full_name',
                'tmp.total_in_progress',
                'tmp_progress_today.total_complete_today',
                'user_detail.last_tracked_time',
                'user_detail.id_skpd',
                'master_skpd.skpd_name',
                knex.raw('SQRT( POW( 69.1 * ( ST_Y(last_location) - '+ params.latitude +') , 2 ) + POW( 69.1 * ( '+ params.longitude +' - ST_X(last_location) ) * COS( ST_Y(last_location) / 57.3 ) , 2 ) ) AS distance_in_km'))
                .from('user_detail')
                .innerJoin('master_skpd', 'user_detail.id_skpd', 'master_skpd.id')
                .leftJoin(knex.raw('(select count(0) as total_in_progress, id_user from report_location where id_master_status = 3 and id_user is not null group by id_user) as tmp'),'user_detail.id','tmp.tmp.id_user')
                .leftJoin(knex.raw('(select count(0) as total_complete_today, id_user from report_location where id_master_status = 6 and id_user is not null and DATE(completed_at) = CURDATE() group by id_user) as tmp_progress_today'),'user_detail.id','tmp_progress_today.tmp_progress_today.id_user')
                .where(function() {
                    this.where('user_detail.id_user_level', 9);
                    if ( params.id_user_level === 4 || params.id_user_level === 6 || params.id_user_level === 8) {
                        this.where('user_detail.id_skpd', params.id_skpd);
                    } else if (params.id_user_level === 1 || params.id_user_level === 2 || params.id_user_level === 3) {
                        this.where('user_detail.id_skpd', params.id_skpd);
                    }
                })
                .orderBy('distance_in_km', 'desc')
                .orderBy('user_detail.last_tracked_time', 'desc')
                .then(function (results) {
                    var response = {};

                    results.forEach(function(item) {
                        if (item.last_tracked_time) {
                            var date = moment(item.last_tracked_time);
                            item.last_seen = date.format("Do MMMM YYYY HH:mm:ss");
                        }else{
                            item.last_seen = 'Tanggal Tidak Tersedia';
                        }

                        if (item.distance_in_km === null) {
                            item.distance_in_km = 'Jarak Tidak Tersedia';
                        }else{
                            item.distance_in_km = Number((item.distance_in_km).toFixed(2)) + ' KM dari laporan';
                        }

                        if (item.total_in_progress === null) {
                            item.total_in_progress = 0;
                        }

                        if (item.total_complete_today === null) {
                            item.total_complete_today = 0;
                        }

                        delete item.last_tracked_time;
                    });

                    response.total = results.length;
                    response.data = results;
                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        })
    },
    /**
     * Get List of Coordination SKPD from master_skpd table
     * @param {Object} params
     *
     * get SKPD data from master_skpd table
     *
     */
    getSkpdKoordinasi : function (id_group) {
        return new Promise((resolve,reject) => {
            knex.select('id', 'skpd_code', 'skpd_name', 'id_unit', 'id_group', 'id_wilayah')
                .from('master_skpd')
                .where('active', 0)
                .where('id_group', '<=', id_group)
                .orderBy('skpd_name', 'asc')
                .then(function (results) {
                    var response = {};
                    response.total = results.length;
                    response.data = results;
                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    /**
     * Get List of Coordination SKPD from master_skpd table
     * @param {Object} params
     *
     * get SKPD data from master_skpd table
     *
     */
    getSkpdDisposisi : function (id_group) {
        return new Promise((resolve,reject) => {
            knex.select('id', 'skpd_code', 'skpd_name', 'id_unit', 'id_group', 'id_wilayah')
                .from('master_skpd')
                .where('active', 0)
                .where('id_group', '>', id_group)
                .orderBy('skpd_name', 'asc')
                .then(function (results) {
                    var response = {};
                    response.total = results.length;
                    response.data = results;
                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
};

module.exports = skpd;