/**
 * Created by fzlrhmn on 7/25/16.
 */
var moment 		= require('moment');
var config      = require('../config/db_knex');
var knex        = require('knex')(config);
var fs          = require('fs');
var path        = require('path');
var move        = require('../helpers/move');
var logger      = require('../config/log');
var slack       = require('../helpers/slack');
require('dotenv').config();
moment().format();
moment.locale('id');

var report = {
    getOpenReport : function (req, res) {
        var id_skpd     = req.decoded.id_skpd;
        var id_wilayah  = req.decoded.id_wilayah;
        var id_user     = req.decoded.id_user;
        var user_level  = req.decoded.id_user_level;

        var recordLimit = 10;
        knex('report_location')
            .whereIn( 'id_master_status', [2,4,5] )
            .count('id as count')
            .then(function(results){
                var total = results[0].count;
                if (req.params.page) {
                    var page = parseInt(req.params.page) - 1;
                    var offset = recordLimit * page;
                }else{
                    var page = 0;
                    var offset = 0;
                }
                var test = knex.select(
                    'report_location.id_report_source',
                    'report_location.title',
                    'report_location.created_date',
                    'report_location.media_format',
                    'report_location.media',
                    'report_location.category',
                    'report_location.id_master_status',
                    'master_report_source.source_name',
                    'master_report_status.status_name')
                    .from('report_location')
                    .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                    .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                    .whereIn( 'report_location.id_master_status', [2,4,5] )
                    .where(function () {
                        /*
                         * logic for get by dinas, kelurahan, kecamatan, kota put here
                         *
                         * If user level = 9, then choose from id_user column
                         * If user level = 8, then choose from id_kelurahan column
                         * If user level = 7, then choose from id_kecamatan column
                         * If user level = 6 OR 5, then choose from id_kota column
                         * If user level = 4, then choose from id_skpd column
                         * If user level = 8, then choose all
                         *
                         */
                        if (user_level === 9){
                            this.where('report_location.id_user', id_user);
                            //this.where('report_location.id_master_status', null);
                        }
                        if (user_level === 8){
                            this.where('report_location.id_kelurahan', id_wilayah);
                            this.where('report_location.id_skpd', null);
                        }
                        if (user_level === 7){
                            this.where('report_location.id_kecamatan', id_wilayah);
                        }
                        if (user_level === 6 || user_level === 5){
                            this.where('report_location.id_kecamatan', id_wilayah);
                        }
                        if (user_level === 4){
                            this.where('report_location.id_skpd', id_skpd);
                        }
                    })
                    .limit(recordLimit)
                    .offset(offset)
                    //    .toString();
                    //console.log(test);
                    .orderBy('updated_at', 'desc')
                    .then(function (results) {
                        var response = {};
                        results.forEach(function(item) {
                            var date = moment(item.created_date)
                            item.date = date.format("Do MMMM YYYY HH:mm:ss");
                        });
                        response.total          = total;
                        response.count_data     = results.length;
                        response.data           = results;

                        res.json(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
            })
    },
    getProcessReport : function (req, res) {
        var id_skpd     = req.decoded.id_skpd;
        var id_wilayah  = req.decoded.id_wilayah;
        var id_user     = req.decoded.id_user;
        var user_level  = req.decoded.id_user_level;

        var recordLimit = 10;
        knex('report_location')
            .whereIn( 'id_master_status', [2,4,5] )
            .count('id as count')
            .then(function(results){
                var total = results[0].count;
                if (req.params.page) {
                    var page = parseInt(req.params.page) - 1;
                    var offset = recordLimit * page;
                }else{
                    var page = 0;
                    var offset = 0;
                }
                var test = knex.select(
                    'report_location.id_report_source',
                    'report_location.title',
                    'report_location.created_date',
                    'report_location.media_format',
                    'report_location.media',
                    'report_location.category',
                    'report_location.id_master_status',
                    'master_report_source.source_name',
                    'master_report_status.status_name')
                    .from('report_location')
                    .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                    .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                    .whereIn( 'report_location.id_master_status', [3] )
                    .where(function () {
                        /*
                         * logic for get by dinas, kelurahan, kecamatan, kota put here
                         *
                         * If user level = 9, then choose from id_user column
                         * If user level = 8, then choose from id_kelurahan column
                         * If user level = 7, then choose from id_kecamatan column
                         * If user level = 6 OR 5, then choose from id_kota column
                         * If user level = 4, then choose from id_skpd column
                         * If user level = 8, then choose all
                         *
                         */
                        if (user_level === 9){
                            this.where('report_location.id_user', id_user);
                        }
                        if (user_level === 8){
                            this.where('report_location.id_kelurahan', id_wilayah);
                        }
                        if (user_level === 7){
                            this.where('report_location.id_kecamatan', id_wilayah);
                        }
                        if (user_level === 6 || user_level === 5){
                            this.where('report_location.id_kecamatan', id_wilayah);
                        }
                        if (user_level === 4){
                            this.where('report_location.id_skpd', id_skpd);
                        }
                    })
                    .limit(recordLimit)
                    .offset(offset)
                    .orderBy('updated_at', 'desc')
                    .then(function (results) {
                        var response = {};
                        results.forEach(function(item) {
                            var date = moment(item.created_date)
                            item.date = date.format("Do MMMM YYYY HH:mm:ss");
                        });
                        response.total          = total;
                        response.count_data     = results.length;
                        response.data           = results;

                        res.json(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
            })
    },
    getCompleteReport : function (req, res) {
        var id_skpd     = req.decoded.id_skpd;
        var id_wilayah  = req.decoded.id_wilayah;
        var id_user     = req.decoded.id_user;
        var user_level  = req.decoded.id_user_level;

        var recordLimit = 10;
        knex('report_location')
            .whereIn( 'id_master_status', [2,4,5] )
            .count('id as count')
            .then(function(results){
                var total = results[0].count;
                if (req.params.page) {
                    var page = parseInt(req.params.page) - 1;
                    var offset = recordLimit * page;
                }else{
                    var page = 0;
                    var offset = 0;
                }
                var test = knex.select(
                    'report_location.id_report_source',
                    'report_location.title',
                    'report_location.created_date',
                    'report_location.media_format',
                    'report_location.media',
                    'report_location.category',
                    'report_location.id_master_status',
                    'master_report_source.source_name',
                    'master_report_status.status_name')
                    .from('report_location')
                    .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                    .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                    .whereIn( 'report_location.id_master_status', [6] )
                    .where(function () {
                        /*
                         * logic for get by dinas, kelurahan, kecamatan, kota put here
                         *
                         * If user level = 9, then choose from id_user column
                         * If user level = 8, then choose from id_kelurahan column
                         * If user level = 7, then choose from id_kecamatan column
                         * If user level = 6 OR 5, then choose from id_kota column
                         * If user level = 4, then choose from id_skpd column
                         * If user level = 8, then choose all
                         *
                         */
                        if (user_level === 9){
                            this.where('report_location.id_user', id_user);
                        }
                        if (user_level === 8){
                            this.where('report_location.id_kelurahan', id_wilayah);
                        }
                        if (user_level === 7){
                            this.where('report_location.id_kecamatan', id_wilayah);
                        }
                        if (user_level === 6 || user_level === 5){
                            this.where('report_location.id_kecamatan', id_wilayah);
                        }
                        if (user_level === 4){
                            this.where('report_location.id_skpd', id_skpd);
                        }
                    })
                    .limit(recordLimit)
                    .offset(offset)
                    .orderBy('updated_at', 'desc')
                    .then(function (results) {
                        var response = {};
                        results.forEach(function(item) {
                            var date = moment(item.created_date)
                            item.date = date.format("Do MMMM YYYY HH:mm:ss");
                        });
                        response.total          = total;
                        response.count_data     = results.length;
                        response.data           = results;

                        res.json(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
            })
    },
    getDetailReport: function(req,res) {
        var id_report = req.params.id_report;
        var user_level  = req.decoded.id_user_level;
        var sql = knex.select(
            'report_location.*',
            'master_report_source.source_name',
            'master_report_status.status_name',
            'master_kelurahan.nama_kelurahan')
            .from('report_location')
            .innerJoin('master_kelurahan', 'report_location.id_kelurahan', 'master_kelurahan.kode_kelurahan')
            .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
            .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
            .where('id_report_source', id_report)
            .where(function () {
                if (user_level > 3){
                    //logic for get by dinas, kelurahan, kecamatan, kota put here
                }
            })
            .then(function (results) {
                var response = {};
                results.forEach(function(item) {
                    var date = moment(item.created_date)
                    item.date = date.format("Do MMMM YYYY HH:mm:ss");
                });
                response.data         = results[0];

                res.json(response);
            })
            .catch(function(error) {
                console.log(error);
            })
    },
    getDetailLogReport: function(req,res) {
        var id_report = req.params.id_report;
        var user_level  = req.decoded.id_user_level;
        var sql = knex.select('report_location_status_log.timestamp','report_location_status_log.comment','report_location_status_log.id_skpd','report_location_status_log.file','report_location_status_log.id_user_detail',' master_report_status.status_name')
            .from('report_location_status_log')
            .innerJoin('master_report_status', 'report_location_status_log.id_master_status', 'master_report_status.id')
            .where('id_report_location', id_report)
            .where(function () {
                if (user_level > 3){
                    //logic for get by dinas, kelurahan, kecamatan, kota put here
                }
            })
            .then(function (results) {
                var response = {};
                results.forEach(function(item) {
                    var date = moment(item.timestamp);
                    item.date = date.format("Do MMMM YYYY HH:mm:ss");
                });
                response.data         = results;
                res.json(response);
            })
            .catch(function(error) {
                console.log(error);
            })
    },
    getLocationLogReport: function(req,res) {
        var id_report = req.params.id_report;
        var user_level  = req.decoded.id_user_level;
        var sql = knex.select(knex.raw('id_report_source, ST_asgeojson(the_geom) as the_geom'))
            .from('report_location')
            .where('id_report_source', id_report)
            .where(function () {
                if (user_level > 3){
                    //logic for get by dinas, kelurahan, kecamatan, kota put here
                }
            })
            .then(function (results) {

                var geojson = {
                    "type" : "FeatureCollection",
                    "features" : []
                };

                var geom = results[0].the_geom;

                delete results[0].the_geom;

                var geometry_object = {
                    "type" : "Feature",
                    "properties" : results[0],
                    "geometry" : JSON.parse(geom)
                };

                geojson.features.push(geometry_object);

                res.json(geojson);
            })
            .catch(function(error) {
                console.log(error);
            })
    },
    setDisposisi: function(req,res) {
        var sql = knex.insert(
            {
                id_report_location  : req.body.id_report,
                assigner            : req.decoded.id_user,
                id_user_detail      : req.body.id_user,
                id_skpd             : req.decoded.id_skpd,
                comment             : req.body.comment,
                the_geom            : knex.raw('ST_geomfromtext("POINT(' + req.body.longitude + ' ' + req.body.latitude + ')")'),
                id_master_status    : 4
            }
            )
            .into('report_location_status_log')
            .then(function (results) {
                res.json(200, {'message' : 'Disposisi Laporan Berhasil dimasukkan'});
            })
            .catch(function(error) {
                var data = '';
                data += "*Message* : Disposisi laporan gagal dimasukkan database\n";
                data += "*ID Laporan* : " + req.body.id_report;
                data += '\n';
                data += "*ID User Assigner* : " + req.decoded.id_user;
                data += '\n';
                data += "*ID User Worker* : " + req.body.id_user;
                data += '\n';
                data += "*Comment* : " + req.body.comment;
                data += '\n';
                data += "*Latitude* : " + req.body.latitude;
                data += '\n';
                data += "*Longitude* : " + req.body.longitude;
                data += '\n';
                data += "*Time* : " + moment().format("Do MMMM YYYY HH:mm:ss");
                data += '\n\n';
                var error_msg = '`'+error + '`';
                slack.slackReport(data,error_msg);
                res.json(400, {'message' : 'Disposisi Laporan Gagal dimasukkan', 'error' : error});
            });
    },
    setKoordinasi: function(req,res) {
        var sql = knex.insert(
            {
                id_report_location  : req.body.id_report,
                assigner            : req.decoded.id_user,
                id_skpd             : req.body.id_skpd,
                comment             : req.body.comment,
                the_geom            : knex.raw('ST_geomfromtext("POINT(' + req.body.longitude + ' ' + req.body.latitude + ')")'),
                id_master_status    : 5
            }
            )
            .into('report_location_status_log')
            .then(function (results) {
                res.json(200, {'message' : 'Koordinasi Laporan Berhasil dimasukkan'});
            })
            .catch(function(error) {
                var data = '';
                data += "*Message* : Disposisi laporan gagal dimasukkan database\n";
                data += "*ID Laporan* : " + req.body.id_report;
                data += '\n';
                data += "*ID User Assigner* : " + req.decoded.id_user;
                data += '\n';
                data += "*ID SKPD* : " + req.body.id_skpd;
                data += '\n';
                data += "*Comment* : " + req.body.comment;
                data += '\n';
                data += "*Latitude* : " + req.body.latitude;
                data += '\n';
                data += "*Longitude* : " + req.body.longitude;
                data += '\n';
                data += "*Time* : " + moment().format("Do MMMM YYYY HH:mm:ss");
                data += '\n\n';
                var error_msg = '`'+error + '`';
                slack.slackReport(data,error_msg);
                res.json(400, {'message' : 'Koordinasi Laporan Gagal dimasukkan', 'error' : error});
            });
    },
    setProgress: function(req,res) {
        var sql = knex.insert(
            {
                id_report_location  : req.body.id_report,
                assigner            : req.decoded.id_user,
                id_skpd             : req.decoded.id_skpd,
                comment             : req.body.comment,
                the_geom            : knex.raw('ST_geomfromtext("POINT(' + req.body.longitude + ' ' + req.body.latitude + ')")'),
                id_master_status    : 3
            }
            )
            .into('report_location_status_log')
            .then(function (results) {
                res.json(200, {'message' : 'Silahkan Kerjakan Laporan Anda'});
            })
            .catch(function(error) {
                res.json(400, {'message' : 'Terjadi Kesalahan', 'error' : error});
            });
    },
    setFinish: function(req,res) {
        var new_filename = (Math.random().toString(36)+'00000000000000000').slice(2, 10) + Date.now() + path.extname(req.files.file.name);
        var old_path = req.files.file.path;
        var new_path = '../cdn/images/crm/' + new_filename;
        move(old_path, new_path, function(err) {
            if (err) {
                logger.log('error', err);
                //error send to slack
                res.json(400, {status : 'Error', message : err});
            }else{
                var data = {
                    id_report_location  : req.body.id_report,
                    id_user_detail      : req.decoded.id_user,
                    id_skpd             : req.decoded.id_skpd,
                    comment             : req.body.comment,
                    file                : new_filename,
                    the_geom            : knex.raw('ST_geomfromtext("POINT(' + req.body.longitude + ' ' + req.body.latitude + ')")'),
                    id_master_status    : 6
                };
                var sql = knex.insert(data)
                    .into('report_location_status_log')
                    .then(function (results) {
                        res.json(200, {status : 'Success', message : 'Laporan Telah Selesai Dikerjakan', filename: new_filename});
                    })
                    .catch(function(error) {
                        logger.log('error', err);
                        res.json(400, {status : 'Error', message : 'Terjadi kesalahan. Silahkan ulangi lagi', error : error});
                    });
            }
        });
    }
}

module.exports = report;