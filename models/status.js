/**
 * Created by fzlrhmn on 7/14/16.
 */
var moment 		= require('moment');
var config      = require('../config/db_knex');
var knex        = require('knex')(config);

moment().format();
moment.locale('id');

var status = {
    getQlueCompletionStatus : function (params) {
        return new Promise((resolve, reject) => {
            knex.select()
                .from('master_report_status')
                .whereNot('id', 1)
                .then(function (results) {
                    var status = results;
                    var idArray = [];

                    results.forEach(function(item) {
                        idArray.push(item.id);
                    });

                    knex.select('id_master_status','id_skpd','id_report_source')
                        .from('report_location')
                        .whereIn('id_master_status', idArray)
                        .where(function () {
                            /*
                             * logic for get by dinas, kelurahan, kecamatan, kota put here
                             *
                             * If user level = 9, then choose from id_user column
                             * If user level = 8, then choose from id_kelurahan column
                             * If user level = 7, then choose from id_kecamatan column
                             * If user level = 6, then choose from id_skpd column
                             * If user level = 5, then choose from id_kota column
                             * If user level = 4, then choose id_skpd column (need to be verified)
                             * If user level = 8, then choose all
                             *
                             */
                            if (params.id_user_level === 9){
                                this.where('report_location.id_user', params.id_user);
                            }
                            if (params.id_user_level === 8){
                                this.where('report_location.id_kelurahan', params.id_wilayah);
                                this.where(function () {
                                    this.where('report_location.id_skpd', params.id_skpd)
                                        .orWhere('report_location.id_skpd', null)
                                });
                            }
                            if (params.id_user_level === 7){
                                this.where('report_location.id_kecamatan', params.id_wilayah);
                            }
                            if (params.id_user_level === 6){
                                this.where('report_location.id_skpd', params.id_skpd);
                            }
                            if (params.id_user_level === 5){
                                this.where('report_location.id_kota', params.id_wilayah);
                            }
                            if (params.id_user_level === 4){
                                this.where('report_location.id_skpd', params.id_skpd);
                            }
                        })
                        .whereIn( 'report_location.tag1', ['Keluhan Masyarakat','Surveyor Transjakarta'] )
                        .then(function(results) {
                            status.forEach(function(itemStatus) {
                                itemStatus.total = 0;
                                results.forEach(function(item) {
                                    if ( item.id_master_status == itemStatus.id) {
                                        itemStatus.total += 1;
                                    }
                                })
                            });

                            resolve(status);
                        })
                        .catch(error => {
                            reject(error)
                        })

                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    getRopCompletionStatus : function (params) {
        return new Promise((resolve, reject) => {
            knex.select()
                .from('master_report_status')
                .whereNot('id', 1)
                .then(function (results) {
                    var status = results;
                    var idArray = [];

                    results.forEach(function(item) {
                        idArray.push(item.id);
                    });

                    knex.select('id_master_status')
                        .from('report_non_location')
                        .whereIn('id_master_status', idArray)
                        .where(function () {
                            if (params.id_user_level > 3){
                                this.where('report_non_location.id_skpd', params.id_skpd);
                            }
                        })
                        .then(function(results) {
                            var response = {};

                            status.forEach(function(itemStatus) {
                                itemStatus.total = 0;
                                results.forEach(function(item) {
                                    if ( item.id_master_status == itemStatus.id) {
                                        itemStatus.total += 1;
                                    }
                                })
                            });

                            response.data = status;

                            resolve(status);
                        });

                })
                .catch(function(error) {
                    reject(error);
                })
        });

    }
};

module.exports = status;