/**
 * Created by fzlrhmn on 8/27/16.
 */

let config      = require('../config/db_knex');
let knex        = require('knex')(config);
let winston		= require('../config/log');
let bcrypt 		= require('bcrypt-nodejs');
let jwt			= require('jsonwebtoken');
let jwt_config	= require('../config/jwt');
let moment      = require('moment');


let login = {
    checkLogin : function(params) {
        return new Promise((resolve, reject) => {
            knex('user_detail')
                .select(
                    'user_detail.id',
                    'user_detail.username',
                    knex.raw('user_detail.full_name as fullname'),
                    'user_detail.password',
                    'user_detail.id_user_level',
                    'user_detail.id_skpd',
                    'user_detail.id_group',
                    knex.raw('master_skpd.skpd_name as nama_skpd'),
                    knex.raw('master_skpd.id_wilayah as id_wilayah')
                )
                .innerJoin('master_skpd','user_detail.id_skpd','master_skpd.id')
                .rightJoin('privileges_table', 'user_detail.id','privileges_table.id_user_detail')
                .where('user_detail.username', params.username)
                .where('privileges_table.id_privileges', 1)
                .where('user_detail.deleted_at', null)
                .limit(1)
                .then(result => {
                    if (result.length > 0) {
                        params.id_user = result[0].id;
                        this
                            .checkHash(result,params)
                            .then(results => {
                                resolve(results);
                            })
                            .catch(error => {
                                reject(error);
                            })
                    }
                    else{
                        reject({ status_code : 403, code : 'Forbidden', message : 'Username Tidak Ditemukan', data : params.username });
                    }
                })
                .catch(error => {
                    reject(error);
                })
        });
    },
    checkHash : function(payload,params) {
        return new Promise((resolve, reject) => {
            let payloadToken = {};
            payloadToken.code            = 'Success';
            payloadToken.id_user         = payload[0].id;
            payloadToken.username        = payload[0].username;
            payloadToken.fullname        = payload[0].fullname;
            payloadToken.id_user_level   = payload[0].id_user_level;
            payloadToken.id_group        = payload[0].id_group;
            payloadToken.id_skpd         = payload[0].id_skpd;
            payloadToken.nama_skpd       = payload[0].nama_skpd;
            payloadToken.id_wilayah      = payload[0].id_wilayah;

            // since password stored in bcrypt hashes from PHP, we need to replace first 4 character with nodejs generated string hashes
            var PhpGeneratedHash = payload[0].password;
            var NodeGeneratedHash = PhpGeneratedHash.replace('$2y$', '$2a$');

            if (bcrypt.compareSync(params.password, NodeGeneratedHash)){
                this.logLogin(params);
                let token = jwt.sign(payloadToken, jwt_config.secret,{
                    expiresIn : "14d"
                });
                resolve(this.createObject(payloadToken,token));
            }else{
                reject({ status_code : 401, code : 'Unauthorized', message : 'Username dan Password Tidak Cocok' });
            }
        })
    },
    createObject : function (payload, token) {
        let res = {
            code : 'Success',
            message : 'Success login',
            token : token,
            data : payload
        };
        return res;
    },
    logLogin : function (params) {
        knex.insert({
                geom            : knex.raw('ST_geomfromtext("POINT(' + params.longitude + ' ' + params.latitude + ')")'),
                id_user_detail  : params.id_user,
                device_model    : params.device_model,
                device_uuid     : params.device_uuid,
                device_platform : params.device_platform,
                os_version      : params.os_version,
                timestamp       : moment().format("YYYY-MM-DD HH:mm:ss")

            })
            .into('user_login_log')
            .then(function (results) {
                winston.log('info', 'User logged')
            })
            .catch(function(error) {
                winston.log('error', 'error insert user log : ' + error );
            });
    },
    createHash : function(payload) {
        return new Promise((resolve, reject) => {
            let payloadToken = {};
            payloadToken.code            = 'Success';
            payloadToken.id_user         = payload[0].id;
            payloadToken.username        = payload[0].username;
            payloadToken.fullname        = payload[0].fullname;
            payloadToken.id_user_level   = payload[0].id_user_level;
            payloadToken.id_group        = payload[0].id_group;
            payloadToken.id_skpd         = payload[0].id_skpd;
            payloadToken.nama_skpd       = payload[0].nama_skpd;
            payloadToken.id_wilayah      = payload[0].id_wilayah;

            let token = jwt.sign(payloadToken, jwt_config.secret,{
                expiresIn : "14d"
            });
            resolve(this.createObject(payloadToken,token));
        })
    }
};

module.exports = login;