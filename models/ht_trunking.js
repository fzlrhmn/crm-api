/**
 * Created by fzlrhmn on 11/11/16.
 */

let moment 		            = require('moment');
let config                  = require('../config/db_api_jakgo');
let knex                    = require('knex')(config);
let fs                      = require('fs');

require('dotenv').config();
moment().format();
moment.locale('id');

let htTrunkingData = {
    getHtTrunking : () => {
        return new Promise((resolve, reject) => {
            knex.select(
                'id',
                'device_ssi',
                'device_alias',
                'search_string',
                'lat',
                'lng',
                'rn',
                'received_datetime')
                .from('app_kominfo_ht_trunking_updated')
                .then(res => {
                    resolve(res);
                })
                .catch(error => {
                    reject(error);
                })
        })
    }
};

module.exports = htTrunkingData;