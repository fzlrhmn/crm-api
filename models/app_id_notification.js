/**
 * Created by fzlrhmn on 8/9/16.
 */
var moment 		= require('moment');
var config      = require('../config/db_knex');
var knex        = require('knex')(config);
let logger      = require('../config/log');

moment().format();
moment.locale('id');

var push_service = {
    setAppId : function(payload) {
        return new Promise((resolve, reject) => {
            knex.insert(payload)
                .into('user_notification')
                .then(function (results) {
                    resolve(results);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on setAppID function : ' + error);
                    reject(error);
                })
        });
    },
    removeAppId : function(params) {
        return new Promise((resolve, reject) => {
            knex('user_notification')
                .where('app_id', params.key)
                .del()
                .then(function (results) {
                    resolve(results);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on removeAppId function : ' + error);
                    reject(error);
                })
        })
    }
};

module.exports = push_service;