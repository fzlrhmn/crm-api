/**
 * Created by fzlrhmn on 7/29/16.
 */
var moment 		= require('moment');
var config      = require('../config/db_knex');
var knex        = require('knex')(config);

moment().format();
moment.locale('id');

var skpd = {
    setLocation : function (params) {

        return new Promise((resolve, reject) => {
            knex.insert(
                {
                    the_geom            : knex.raw('ST_geomfromtext("POINT(' + params.longitude + ' ' + params.latitude + ')")'),
                    id_user             : params.id_user
                }
                )
                .into('user_tracking')
                .then(function (results) {
                    resolve(results);
                })
                .catch(function(error) {
                    reject(error);
                });
        });
    }
};

module.exports = skpd;