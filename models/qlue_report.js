/***
 * Created by fzlrhmn on 8/24/16.
 */

let moment 		            = require('moment');
let config                  = require('../config/db_knex');
let knex                    = require('knex')(config);
let fs                      = require('fs');
let path                    = require('path');
let move                    = require('../helpers/move');
let push                    = require('../helpers/push');
let logger                  = require('../config/log');
let md5                     = require('md5');
let http                    = require('http');
let sendData                = require('../queue/queue-post-qlue');
let queuePushNotification   = require('../queue/queue-push-notification');
let async                   = require('async');

require('dotenv').config();
moment().format();
moment.locale('id');

let qlue_report = {
    /**
     * Construct parameter from req.decoded and req.params from controller
     * @param {Object} decoded
     * @param {Object} param
     * @param {string} status
     *
     * construct parameter for where condition query
     *
     */
    paramsConstructor : function(decoded, param, status) {
        let params      = {};
        params.id_skpd     = decoded.id_skpd;
        params.id_wilayah  = decoded.id_wilayah;
        params.id_user     = decoded.id_user;
        params.user_level  = decoded.id_user_level;
        params.page        = param.page;
        params.sort_by     = param.sort_by;

        // filter params
        params.id_kelurahan = param.id_kelurahan || null;
        params.date         = param.date || null;
        params.id_report    = param.id_report || null;
        params.keyword      = param.keyword || null;
        params.skpd         = param.skpd || null;
        params.status       = status;

        // create hash for set cache key
        let hash            = md5(JSON.stringify(params));

        params.hash         = hash;

        return params;
    },
    /**
     * Get Coordinate with given id_report
     * @param {string} id_report
     *
     * get coordinate
     *
     */
    getCoordinate : function(id_report) {
        return new Promise((resolve, reject) => {
            knex('report_location')
                .select(knex.raw('ST_X(the_geom) as longitude'),knex.raw('ST_Y(the_geom) as latitude'),'address')
                .where('id_report_source', id_report)
                .limit(1)
                .then((result) => {
                    let response = {};
                    response.latitude   = result[0].latitude;
                    response.longitude  = result[0].longitude;
                    if (result[0].address !== null) {
                        response.address    = result[0].address;
                    }
                    resolve(response);
                })
                .catch((error) => {
                    logger.log('error', 'Error on ' + __filename + ' on getCoordinate function : ' + error);
                    reject(error);
                })
        });
    },
    /**
     * Get Open Report with given params
     * @param {Object} params
     *
     * get open report given coordinate with specified parameter
     *
     */
    getOpenReport : function (params) {
        return new Promise ((resolve, reject) => {
            let recordLimit = 10;
            if (params.page) {
                var page = parseInt(params.page) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }

            //sort by created or updated
            if (params.sort_by === "created") {
                var sortBy = "report_location.created_date DESC";
            }
            else if (params.sort_by === "updated") {
                var sortBy = "report_location.updated_at DESC";
            } else {
                var sortBy = "report_location.updated_at DESC";
            }

            knex.select(
                'report_location.id_report_source',
                'report_location.title',
                'report_location.content',
                'report_location.username',
                'report_location.created_date',
                'report_location.updated_at',
                'report_location.media_format',
                'report_location.media',
                'report_location.category',
                'report_location.id_user',
                'report_location.id_master_status',
                'master_report_source.source_name',
                'master_report_status.status_name'
                )
                .from('report_location')
                .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .whereIn( 'report_location.id_master_status', [2,4,5] )
                .whereIn( 'report_location.tag1', ['Keluhan Masyarakat','Surveyor Transjakarta'] )
                .where(function () {
                    /**
                     * logic for get by dinas, kelurahan, kecamatan, kota put here
                     *
                     * If user level = 9, then choose from id_user column
                     * If user level = 8, then choose from id_kelurahan column
                     * If user level = 7, then choose from id_kecamatan column
                     * If user level = 6, then choose from id_skpd column
                     * If user level = 5, then choose from id_kota column
                     * If user level = 4, then choose from id_skpd column (need to be verified)
                     * If user level = 8, then choose all
                     *
                     */
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd)
                                .orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }

                    // Filtering
                    if (params.id_kelurahan !== null) {
                        this.where('report_location.id_kelurahan', params.id_kelurahan);
                    }
                    if (params.date !== null) {
                        this.where(knex.raw('DATE(`report_location`.`created_date`) = \'' + params.date+'\''));
                    }
                    if (params.id_report !== null) {
                        this.where('report_location.id_report_source', params.id_report);
                    }
                    if (params.keyword !== null) {
                        this.where(function() {
                            this.where('report_location.title', 'like','%'+params.keyword+'%')
                                .orWhere('report_location.content', 'like','%'+params.keyword+'%')
                                .orWhere('report_location.username', 'like','%'+params.keyword+'%')
                        });
                    }
                    if (params.skpd !== null) {
                        this.where('report_location.id_skpd', params.skpd);
                    }
                })
                .limit(recordLimit)
                .offset(offset)
                .orderByRaw(sortBy)
                .then(function (results) {
                    let response = {};
                    results.forEach(function(item) {
                        let created_date = moment(item.created_date);
                        item.day = created_date.format("dddd");
                        item.date = created_date.format("Do MMMM YYYY HH:mm:ss");
                        item.date_web = created_date.format("Do MMMM YYYY | HH:mm:ss");

                        let updated_date = moment(item.updated_at);
                        item.updated_date = updated_date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.count_data     = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on getOpenReport function : ' + error);
                    reject(error);
                })
        })
    },
    /**
     * Get Process Report with given params
     * @param {Object} params
     *
     * get process report given coordinate with specified parameter
     *
     */
    getProcessReport : function (params) {
        return new Promise((resolve, reject) => {
            let recordLimit = 10;
            if (params.page) {
                var page = parseInt(params.page) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }

            //sort by created or updated
            if (params.sort_by === "created") {
                var sortBy = "report_location.created_date DESC";
            }
            else if (params.sort_by === "updated") {
                var sortBy = "report_location.updated_at DESC";
            } else {
                var sortBy = "report_location.updated_at DESC";
            }

            knex.select(
                'report_location.id_report_source',
                'report_location.title',
                'report_location.content',
                'report_location.username',
                'report_location.created_date',
                'report_location.updated_at',
                'report_location.media_format',
                'report_location.media',
                'report_location.category',
                'report_location.id_user',
                'report_location.id_master_status',
                'master_report_source.source_name',
                'master_report_status.status_name'
                )
                .from('report_location')
                .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .whereIn( 'report_location.id_master_status', [3] )
                .whereIn( 'report_location.tag1', ['Keluhan Masyarakat','Surveyor Transjakarta'] )
                .where(function () {
                    /**
                     * logic for get by dinas, kelurahan, kecamatan, kota put here
                     *
                     * If user level = 9, then choose from id_user column
                     * If user level = 8, then choose from id_kelurahan column
                     * If user level = 7, then choose from id_kecamatan column
                     * If user level = 6, then choose from id_skpd column
                     * If user level = 5, then choose from id_kota column
                     * If user level = 4, then choose from id_skpd column (need to be verified)
                     * If user level = 8, then choose all
                     *
                     */
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd).orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }

                    // Filtering
                    if (params.id_kelurahan !== null) {
                        this.where('report_location.id_kelurahan', params.id_kelurahan);
                    }
                    if (params.date !== null) {
                        this.where(knex.raw('DATE(`report_location`.`created_date`) = \'' + params.date+'\''));
                    }
                    if (params.id_report !== null) {
                        this.where('report_location.id_report_source', params.id_report);
                    }
                    if (params.keyword !== null) {
                        this.where(function() {
                            this.where('report_location.title', 'like','%'+params.keyword+'%')
                                .orWhere('report_location.content', 'like','%'+params.keyword+'%')
                                .orWhere('report_location.username', 'like','%'+params.keyword+'%')
                        });
                    }
                    if (params.skpd !== null) {
                        this.where('report_location.id_skpd', params.skpd);
                    }
                })
                .limit(recordLimit)
                .offset(offset)
                .orderByRaw(sortBy)
                .then(function (results) {
                    let response = {};
                    results.forEach(function(item) {
                        let created_date = moment(item.created_date);
                        item.day = created_date.format("dddd");
                        item.date = created_date.format("Do MMMM YYYY HH:mm:ss");
                        item.date_web = created_date.format("Do MMMM YYYY | HH:mm:ss");

                        let updated_date = moment(item.updated_at);
                        item.updated_date = updated_date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.count_data     = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on getProcessReport function : ' + error);
                    reject(error);
                })
        })
    },
    /**
     * Get Complete Report with given params
     * @param {Object} params
     *
     * get complete report given coordinate with specified parameter
     *
     */
    getCompleteReport : function (params) {
        return new Promise((resolve, reject) => {
            let recordLimit = 10;
            if (params.page) {
                var page = parseInt(params.page) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }

            //sort by created or updated
            if (params.sort_by === "created") {
                var sortBy = "report_location.created_date DESC";
            }
            else if (params.sort_by === "updated") {
                var sortBy = "report_location.updated_at DESC";
            } else {
                var sortBy = "report_location.updated_at DESC";
            }

            knex.select(

                'report_location.id_report_source',
                'report_location.title',
                'report_location.content',
                'report_location.username',
                'report_location.created_date',
                'report_location.updated_at',
                'report_location.media_format',
                'report_location.media',
                'report_location.category',
                'report_location.id_user',
                'report_location.id_master_status',
                'master_report_source.source_name',
                'master_report_status.status_name')
                .from('report_location')
                .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .whereIn( 'report_location.id_master_status', [6] )
                .whereIn( 'report_location.tag1', ['Keluhan Masyarakat','Surveyor Transjakarta'] )
                .where(function () {
                    /**
                     * logic for get by dinas, kelurahan, kecamatan, kota put here
                     *
                     * If user level = 9, then choose from id_user column
                     * If user level = 8, then choose from id_kelurahan column
                     * If user level = 7, then choose from id_kecamatan column
                     * If user level = 6, then choose from id_skpd column
                     * If user level = 5, then choose from id_kota column
                     * If user level = 4, then choose from id_skpd column (need to be verified)
                     * If user level = 8, then choose all
                     *
                     */
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd).orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }

                    // Filtering
                    if (params.id_kelurahan !== null) {
                        this.where('report_location.id_kelurahan', params.id_kelurahan);
                    }
                    if (params.date !== null) {
                        this.where(knex.raw('DATE(`report_location`.`created_date`) = \'' + params.date+'\''));
                    }
                    if (params.id_report !== null) {
                        this.where('report_location.id_report_source', params.id_report);
                    }
                    if (params.keyword !== null) {
                        this.where(function() {
                            this.where('report_location.title', 'like','%'+params.keyword+'%')
                                .orWhere('report_location.content', 'like','%'+params.keyword+'%')
                                .orWhere('report_location.username', 'like','%'+params.keyword+'%')
                        });
                    }
                    if (params.skpd !== null) {
                        this.where('report_location.id_skpd', params.skpd);
                    }
                })
                .limit(recordLimit)
                .offset(offset)
                .orderByRaw(sortBy)
                .then(function (results) {
                    let response = {};
                    results.forEach(function(item) {
                        let created_date = moment(item.created_date);
                        item.day = created_date.format("dddd");
                        item.date = created_date.format("Do MMMM YYYY HH:mm:ss");
                        item.date_web = created_date.format("Do MMMM YYYY | HH:mm:ss");

                        let updated_date = moment(item.updated_at);
                        item.updated_date = updated_date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.count_data     = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on getCompleteReport function : ' + error);
                    reject(error);
                })
        })
    },
    /**
     * Get Detail Report with given params
     * @param {Object} params
     *
     * get Detail report given coordinate with specified parameter
     *
     */
    getDetailReport: function(params) {
        return new Promise((resolve, reject) => {
            knex.select(
                'report_location.*',
                'master_report_source.source_name',
                'master_report_status.status_name',
                'master_kelurahan.nama_kelurahan')
                .from('report_location')
                .innerJoin('master_kelurahan', 'report_location.id_kelurahan', 'master_kelurahan.kode_kelurahan')
                .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .where('id_report_source', params.id_report)
                .where(function () {
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd)
                                .orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                })
                .then(function (results) {
                    let response = {};
                    if (results.length > 0 ) {
                        results.forEach(function(item) {
                            let date = moment(item.created_date);
                            item.day = date.format("dddd");
                            item.date = date.format("Do MMMM YYYY HH:mm:ss");
                            item.date_web = date.format("Do MMMM YYYY | HH:mm:ss");
                        });
                        response.data         = results[0];
                    } else {
                        response.data         = null;
                    }

                    resolve(response);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on getDetailReport function : ' + error);
                    reject(error);
                })
        });
    },
    /**
     * Get Detail Log Report with given params
     * @param {Object} params
     *
     * get detail log report given coordinate with specified parameter
     *
     * MUST CHECK TIMESTAMP FROM QLUE, ORDER ONLY USE TIMESTAMP
     *
     */
    getDetailLogReport: function(params) {
        return new Promise((resolve,reject) => {
            knex.select('report_location_status_log.timestamp',
                'report_location_status_log.comment',
                'report_location_status_log.id_skpd',
                'master_skpd.skpd_name as nama_skpd',
                'report_location_status_log.file',
                'report_location_status_log.id_user_detail',
                'report_location_status_log.assigner',
                ' master_report_status.status_name',
                'user_detail.username as worker_username',
                'username_assigner.username as assigner_username')
                .from('report_location_status_log')
                .innerJoin('master_report_status', 'report_location_status_log.id_master_status', 'master_report_status.id')
                .leftJoin('user_detail', 'report_location_status_log.id_user_detail', 'user_detail.id')
                .leftJoin('user_detail as username_assigner', 'report_location_status_log.assigner', 'username_assigner.id')
                .leftJoin('master_skpd', 'report_location_status_log.id_skpd', 'master_skpd.id')
                .where('id_report_location', params.id_report)
                .where(function () {
                    if (params.user_level > 3){
                        //logic for get by dinas, kelurahan, kecamatan, kota put here
                    }
                })
                .orderBy('report_location_status_log.timestamp', 'asc')
                .then(function (results) {
                    let response = {};
                    results.forEach(function(item) {
                        let date = moment(item.timestamp);
                        item.day = date.format("dddd");
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                        item.date_web = date.format("Do MMMM YYYY | HH:mm:ss");
                    });
                    response.data         = results;
                    resolve(response);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on getDetailLogReport function : ' + error);
                    reject(error);
                })
        });
    },
    /**
     * Get Coordination History Log Report
     * @param {Object} params
     *
     */
    getCoordinationHistoryReport: function(params) {
        return new Promise((resolve,reject) => {
            let recordLimit = 10;
            if (params.page) {
                var page = parseInt(params.page) - 1;
                var offset = recordLimit * page;
            }else{
                var page = 0;
                var offset = 0;
            }
            knex.select('report_location_coordination_history.created_time',
                'report_location_coordination_history.id_report_source',
                'report_location_coordination_history.id_skpd',
                'report_location_coordination_history.assigner',
                'report_location.title',
                'master_report_status.status_name',
                'master_skpd.skpd_name')
                .from('report_location_coordination_history')
                .innerJoin('report_location', 'report_location_coordination_history.id_report_source', 'report_location.id_report_source')
                .innerJoin('master_skpd', 'report_location_coordination_history.id_skpd', 'master_skpd.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .where('report_location_coordination_history.assigner', params.id_user)
                .limit(recordLimit)
                .offset(offset)
                .orderBy('report_location_coordination_history.created_time', 'desc')
                .then(function (results) {
                    let response = {};
                    results.forEach(function(item) {
                        let date = moment(item.created_time);
                        item.day = date.format("dddd");
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                        item.date_web = date.format("Do MMMM YYYY | HH:mm:ss");
                    });
                    response.data         = results;
                    resolve(response);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on getCoordinationHistoryReport function : ' + error);
                    reject(error);
                })
        });
    },
    /**
     * Get Location Log Report with given params
     * @param {Object} params
     *
     * get location log report given coordinate with specified parameter
     *
     */
    getLocationLogReport: function(params) {
        return new Promise((resolve, reject) => {
            knex.select(knex.raw('id_report_source, ST_asgeojson(the_geom) as the_geom'))
                .from('report_location')
                .where('id_report_source', params.id_report)
                .where(function () {
                    if (params.user_level > 3){
                        //logic for get by dinas, kelurahan, kecamatan, kota put here
                    }
                })
                .then(function (results) {

                    let geojson = {
                        "type" : "FeatureCollection",
                        "features" : []
                    };

                    let geom = results[0].the_geom;

                    delete results[0].the_geom;

                    let geometry_object = {
                        "type" : "Feature",
                        "properties" : results[0],
                        "geometry" : JSON.parse(geom)
                    };

                    geojson.features.push(geometry_object);

                    resolve(geojson);
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on getLocationLogReport function : ' + error);
                    reject(error);
                })
        });
    },
    /**
     * Set Dispatch with given body params
     * @param {Object} params
     *
     * set dispatch with specified body post parameter
     *
     */
    setDispatch: function(params) {
        return new Promise((resolve, reject) => {
            let payload = {
                id_report_location  : params.id_report_location,
                assigner            : params.assigner,
                id_user_detail      : params.id_user_detail,
                id_skpd             : params.id_skpd,
                comment             : params.comment,
                the_geom            : knex.raw('ST_geomfromtext("POINT(' + params.longitude + ' ' + params.latitude + ')")'),
                id_master_status    : 4,
                timestamp           : moment().format("YYYY-MM-DD HH:mm:ss")
            };
            knex.insert(payload)
                .into('report_location_status_log')
                .then(function (results) {
                    async
                        .parallel([
                            (callback) => {
                                knex('report_location')
                                    .where('id_report_source', params.id_report_location)
                                    .update({
                                        id_master_status    : 4,
                                        id_skpd             : params.id_skpd,
                                        id_user             : params.id_user_detail,
                                        completed_at        : null
                                    })
                                    .then(result => {
                                        callback(null, result);
                                    })
                                    .catch(error => {
                                        callback(error, null);
                                    })
                            },
                            (callback) => {
                                queuePushNotification({
                                    id_report   : params.id_report_location,
                                    id_skpd     : params.id_user_detail,
                                    channel     : 'qlue',
                                    heading     : 'Notifikasi Laporan CRM',
                                    message     : "Anda Menerima Disposisi Laporan #" + params.id_report_location,
                                    status      : 'dispatch'
                                }, (error, payload) => {
                                    if (error) callback(error, null);
                                    else callback(null, payload);
                                });
                            }
                        ], (error, result) => {
                            if (error) reject(error);
                            else resolve(result);
                        });
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on setDispatch function : ' + error);
                    reject(error);
                });
        });
    },
    /**
     * Set Coordination with given body params
     * @param {Object} params
     *
     * set coordination with specified body post parameter
     *
     */
    setCoordination: function(params) {
        return new Promise ((resolve, reject) => {
            let payload = {
                id_report_location  : params.id_report_location,
                assigner            : params.assigner,
                id_skpd             : params.id_skpd,
                comment             : params.comment,
                the_geom            : knex.raw('ST_geomfromtext("POINT(' + params.longitude + ' ' + params.latitude + ')")'),
                id_master_status    : 5,
                timestamp           : moment().format("YYYY-MM-DD HH:mm:ss")
            };
            knex.insert(payload)
                .into('report_location_status_log')
                .then(results => {
                    async
                        .parallel([
                            (callback) => {
                                knex('report_location')
                                    .where('id_report_source', params.id_report_location)
                                    .update({
                                        id_master_status    : 5,
                                        id_skpd             : params.id_skpd,
                                        completed_at        : null,
                                        coordination_count  : knex.raw('coordination_count + 1')
                                    })
                                    .then(result => {
                                        callback(null, result);
                                    })
                                    .catch(error => {
                                        callback(error, null);
                                    })
                            },
                            (callback) => {
                                queuePushNotification({
                                    id_report   : params.id_report_location,
                                    id_skpd     : params.id_skpd,
                                    channel     : 'qlue',
                                    heading     : 'Notifikasi Laporan CRM',
                                    message     : "Anda Menerima Koordinasi Laporan #" + params.id_report_location,
                                    status      : 'coordination'
                                }, (error, payload) => {
                                    if (error) callback(error, null);
                                    else callback(null, payload);
                                });
                            }
                        ], (error, result) => {
                            if (error) reject(error);
                            else resolve(result);
                        });
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on setCoordination function : ' + error);
                    reject(error);
                });
        });
    },
    /**
     * Set Progress with given body params
     * @param {Object} params
     *
     * set progress with specified body post parameter
     *
     */
    setProgress: function(params) {
        return new Promise((resolve, reject) => {
            let payload = {
                id_report_location  : params.id_report_location,
                id_user_detail      : params.id_user_detail,
                id_skpd             : params.id_skpd,
                comment             : params.comment,
                the_geom            : knex.raw('ST_geomfromtext("POINT(' + params.longitude + ' ' + params.latitude + ')")'),
                id_master_status    : 3,
                timestamp           : moment().format("YYYY-MM-DD HH:mm:ss")
            };
            knex.insert(payload)
                .into('report_location_status_log')
                .then(function (results) {
                    async
                        .parallel([
                            (callback) => {
                                knex('report_location')
                                    .where('id_report_source', params.id_report_location)
                                    .update({
                                        id_master_status    : 3,
                                        id_user             : params.id_user_detail,
                                        id_skpd             : params.id_skpd,
                                        completed_at        : null
                                    })
                                    .then(result => {
                                        callback(null, result);
                                    })
                                    .catch(error => {
                                        callback(error, null);
                                    })
                            },
                            (callback) => {
                                sendData({
                                    post_id     : params.id_report_location,
                                    status      : "process",
                                    skpd        : params.skpd_name,
                                    username    : params.username,
                                    comment     : params.comment,
                                    file        : null,
                                    timestamp   : payload.timestamp
                                }, (error, payload) => {
                                    if (error) callback(error, null);
                                    else callback(null, payload);
                                });
                            },
                            (callback) => {
                                queuePushNotification({
                                    id_report   : params.id_report_location,
                                    id_skpd     : params.id_skpd,
                                    channel     : 'qlue',
                                    heading     : 'Notifikasi Laporan CRM',
                                    message     : "Laporan #" + params.id_report_location+ " Sedang Dikerjakan",
                                    status      : 'process'
                                }, (error, payload) => {
                                    if (error) callback(error, null);
                                    else callback(null, payload);
                                });
                            }
                        ], (error, result) => {
                            if (error) reject(error);
                            else resolve(result);
                        });
                })
                .catch(function(error) {
                    logger.log('error', 'Error on ' + __filename + ' on setProgress function : ' + error);
                    reject(error);
                });
        });
    },
    /**
     * Set Complete with given body params
     * @param {Object} params
     *
     * set complete with specified body post parameter
     *
     */
    setComplete: function(params) {
        return new Promise((resolve, reject) => {
            let new_filename = (Math.random().toString(36)+'00000000000000000').slice(2, 10) + Date.now() + path.extname(params.file_name);
            let old_path = params.file_path;
            let new_path = '../cdn/images/crm/' + new_filename;
            move(old_path, new_path, function(err) {
                if (err) {
                    logger.log('error', err);
                    reject(err);
                }else{
                    let data = {
                        id_report_location  : params.id_report_location,
                        id_user_detail      : params.id_user_detail,
                        id_skpd             : params.id_skpd,
                        comment             : params.comment,
                        file                : new_filename,
                        the_geom            : knex.raw('ST_geomfromtext("POINT(' + params.longitude + ' ' + params.latitude + ')")'),
                        id_master_status    : 6,
                        timestamp           : moment().format("YYYY-MM-DD HH:mm:ss"),
                        completed_at        : moment().format("YYYY-MM-DD HH:mm:ss")
                    };
                    knex.insert(data)
                        .into('report_location_status_log')
                        .then(function (results) {
                            async
                                .parallel([
                                    (callback) => {
                                        knex('report_location')
                                            .where('id_report_source', params.id_report_location)
                                            .update({
                                                completed_at        : moment().format("YYYY-MM-DD HH:mm:ss"),
                                                id_master_status    : 6,
                                                id_user             : params.id_user_detail,
                                                id_skpd             : params.id_skpd
                                            })
                                            .then(result => {
                                                callback(null, result);
                                            })
                                            .catch(error => {
                                                callback(error, null);
                                            })
                                    },
                                    (callback) => {
                                        sendData({
                                            post_id   : params.id_report_location,
                                            status    : "complete",
                                            skpd      : params.skpd_name,
                                            username  : params.username,
                                            comment   : params.comment,
                                            file      : 'http://cdn.smartcity.jakarta.go.id/images/crm/'+new_filename,
                                            timestamp : data.timestamp
                                        }, (error, payload) => {
                                            if (error) callback(error, null);
                                            else callback(null, payload);
                                        });
                                    },
                                    (callback) => {
                                        queuePushNotification({
                                            id_report   : params.id_report_location,
                                            id_skpd     : params.id_skpd,
                                            channel     : 'qlue',
                                            heading     : 'Notifikasi Laporan CRM',
                                            message     : "Laporan #" + params.id_report_location+ " Telah Selesai Diproses",
                                            status      : 'complete'
                                        }, (error, payload) => {
                                            if (error) callback(error, null);
                                            else callback(null, payload);
                                        });
                                    }
                            ], (error, result) => {
                                if (error) reject(error);
                                else resolve(result);
                            });
                        })
                        .catch(function(error) {
                            logger.log('error', 'Error on ' + __filename + ' on setComplete function : ' + error);
                            reject(error);
                        });
                }
            });
        });
    },
    /**
     * Set Address with given id_report
     * @param {object} address
     * @param {string} id_report
     *
     * Set address
     *
     */
    setAddress : function(address,id_report) {
        knex.select('updated_at')
            .from('report_location')
            .where('id_report_source', id_report)
            .then(function (results) {
                address.updated_at = results[0].updated_at;
                return knex('report_location')
                    .where('id_report_source', id_report)
                    .update(address)
            })
            .catch(function(error) {
                logger.log('error', 'Error on ' + __filename + ' on getLocationLogReport function : ' + error);
            })

    },
    /**
     * Set Address with given id_report
     * @param {object} payload
     * @param {string} id_report
     *
     * Set Process Qlue equal true if queue has sent to http://sync.qlue.id
     *
     */
    setProcessQlue : function(payload,id_report) {
        knex('report_location')
            .where('id_report_source', id_report)
            .update(payload)
            .then((result) => {
            })
            .catch((error ) => {
                logger.log('error', 'Error on ' + __filename + ' on setAddress function : ' + error);
            })
    }
};

module.exports = qlue_report;