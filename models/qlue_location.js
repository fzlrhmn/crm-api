/**
 * Created by fzlrhmn on 8/16/16.
 */

var config      = require('../config/db_knex');
var knex        = require('knex')(config);
var moment 		= require('moment');

var data = {
    getAllReportLocation : function(params) {
        return new Promise((resolve, reject) => {
            knex.select(
                'report_location.id_report_source',
                'report_location.title',
                'report_location.created_date',
                'master_report_status.status_name',
                'report_location.media_format',
                'report_location.media',
                knex.raw('ST_X(the_geom) as latitude'),
                knex.raw('ST_Y(the_geom) as longitude')
                )
                .from('report_location')
                .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .whereIn( 'report_location.id_master_status', [2,3,4,5,6] )
                .whereRaw('`report_location`.`updated_at` >= ( CURDATE() - INTERVAL 3 DAY )')
                .where(function () {
                    /*
                     * logic for get by dinas, kelurahan, kecamatan, kota put here
                     *
                     * If user level = 9, then choose from id_user column
                     * If user level = 8, then choose from id_kelurahan column
                     * If user level = 7, then choose from id_kecamatan column
                     * If user level = 6, then choose from id_skpd column
                     * If user level = 5, then choose from id_kota column
                     * If user level = 4, then choose from id_skpd column (need to be verified)
                     * If user level = 8, then choose all
                     *
                     */
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd).orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                })
                .orderBy('report_location.updated_at', 'desc')
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.created_date);
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.total          = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    getOpenReportLocation : function(params) {
        return new Promise((resolve, reject) => {
            knex.select(
                'report_location.id_report_source',
                'report_location.title',
                'report_location.created_date',
                'master_report_status.status_name',
                'report_location.media_format',
                'report_location.media',
                knex.raw('ST_X(the_geom) as latitude'),
                knex.raw('ST_Y(the_geom) as longitude')
                )
                .from('report_location')
                .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .whereIn( 'report_location.id_master_status', [2,4,5] )
                .where(function () {
                    /*
                     * logic for get by dinas, kelurahan, kecamatan, kota put here
                     *
                     * If user level = 9, then choose from id_user column
                     * If user level = 8, then choose from id_kelurahan column
                     * If user level = 7, then choose from id_kecamatan column
                     * If user level = 6, then choose from id_skpd column
                     * If user level = 5, then choose from id_kota column
                     * If user level = 4, then choose from id_skpd column (need to be verified)
                     * If user level = 8, then choose all
                     *
                     */
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd).orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                })
                .orderBy('report_location.updated_at', 'desc')
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.created_date);
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.total          = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    getProcessReportLocation : function(params) {
        return new Promise((resolve, reject) => {
            knex.select(
                'report_location.id_report_source',
                'report_location.title',
                'report_location.created_date',
                'master_report_status.status_name',
                knex.raw('ST_X(the_geom) as latitude'),
                knex.raw('ST_Y(the_geom) as longitude')
                )
                .from('report_location')
                .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .whereIn( 'report_location.id_master_status', [3] )
                .where(function () {
                    /*
                     * logic for get by dinas, kelurahan, kecamatan, kota put here
                     *
                     * If user level = 9, then choose from id_user column
                     * If user level = 8, then choose from id_kelurahan column
                     * If user level = 7, then choose from id_kecamatan column
                     * If user level = 6, then choose from id_skpd column
                     * If user level = 5, then choose from id_kota column
                     * If user level = 4, then choose from id_skpd column (need to be verified)
                     * If user level = 8, then choose all
                     *
                     */
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd).orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                })
                .orderBy('report_location.updated_at', 'desc')
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.created_date);
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.total          = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        })
    },
    getCompleteReportLocation : function(params) {
        return new Promise((resolve, reject) => {
            knex.select(
                'report_location.id_report_source',
                'report_location.title',
                'report_location.created_date',
                'master_report_status.status_name',
                'report_location.media_format',
                'report_location.media',
                knex.raw('ST_X(the_geom) as latitude'),
                knex.raw('ST_Y(the_geom) as longitude')
                )
                .from('report_location')
                .innerJoin('master_report_source', 'report_location.id_master_source', 'master_report_source.id')
                .innerJoin('master_report_status', 'report_location.id_master_status', 'master_report_status.id')
                .whereIn( 'report_location.id_master_status', [6] )
                .where(function () {
                    /*
                     * logic for get by dinas, kelurahan, kecamatan, kota put here
                     *
                     * If user level = 9, then choose from id_user column
                     * If user level = 8, then choose from id_kelurahan column
                     * If user level = 7, then choose from id_kecamatan column
                     * If user level = 6, then choose from id_skpd column
                     * If user level = 5, then choose from id_kota column
                     * If user level = 4, then choose from id_skpd column (need to be verified)
                     * If user level = 8, then choose all
                     *
                     */
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd).orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                })
                .orderBy('report_location.updated_at', 'desc')
                .then(function (results) {
                    var response = {};
                    results.forEach(function(item) {
                        var date = moment(item.created_date);
                        item.date = date.format("Do MMMM YYYY HH:mm:ss");
                    });
                    response.total          = results.length;
                    response.data           = results;

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    },
    searchReportMaps : function(params) {
        return new Promise((resolve, reject) => {
            let sql = knex.select(
                'report_location.username',
                'report_location.id_report_source',
                'report_location.title',
                'report_location.content',
                'report_location.created_date',
                'report_location.media_format',
                'report_location.media',
                'report_location.category',
                'report_location.id_master_status',
                'master_report_status.status_name',
                'master_kelurahan.nama_kota',
                'master_kelurahan.nama_kecamatan',
                'master_kelurahan.nama_kelurahan',
                knex.raw('ST_asgeojson(report_location.the_geom) as geojson')
                )
                .from('report_location')
                .innerJoin('master_report_status', 'report_location.id_master_status', '=', 'master_report_status.id')
                .leftJoin('master_kelurahan', 'report_location.id_kelurahan', '=', 'master_kelurahan.kode_kelurahan')
                .whereIn( 'report_location.tag1', ['Keluhan Masyarakat','Surveyor Transjakarta'] )
                .where(function () {
                    /**
                     * logic for get by dinas, kelurahan, kecamatan, kota put here
                     *
                     * If user level = 9, then choose from id_user column
                     * If user level = 8, then choose from id_kelurahan column
                     * If user level = 7, then choose from id_kecamatan column
                     * If user level = 6, then choose from id_skpd column
                     * If user level = 5, then choose from id_kota column
                     * If user level = 4, then choose from id_skpd column (need to be verified)
                     * If user level = 8, then choose all
                     *
                     */
                    if (params.user_level === 9){
                        this.where('report_location.id_user', params.id_user);
                    }
                    if (params.user_level === 8){
                        this.where('report_location.id_kelurahan', params.id_wilayah);
                        this.where(function () {
                            this.where('report_location.id_skpd', params.id_skpd)
                                .orWhere('report_location.id_skpd', null)
                        });
                    }
                    if (params.user_level === 7){
                        this.where('report_location.id_kecamatan', params.id_wilayah);
                    }
                    if (params.user_level === 6){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }
                    if (params.user_level === 5){
                        this.where('report_location.id_kota', params.id_wilayah);
                    }
                    if (params.user_level === 4){
                        this.where('report_location.id_skpd', params.id_skpd);
                    }

                    // search params
                    if (params.keyword !== null) {
                        this.where(function() {
                            this.where('report_location.title', 'like','%'+params.keyword+'%')
                                .orWhere('report_location.content', 'like','%'+params.keyword+'%')
                                .orWhere('report_location.username', 'like','%'+params.keyword+'%')
                        });
                    }

                    if (params.id_laporan !== null) {
                        this.where('report_location.id_report_source', params.id_laporan);
                    }

                    if (params.date !== null) {
                        this.where(knex.raw('DATE(`report_location`.`created_date`) = \'' + params.date+'\''));
                    }

                    if (params.category !== null) {
                        this.where('report_location.id_category', params.category);
                    }

                    // TODO need to fixed for wait status because there isn't id_skpd in it
                    if (params.dinas !== null) {
                        this.where('report_location.id_skpd', params.dinas);
                    }

                    if (params.kota !== null) {
                        this.where('report_location.id_kota', params.kota);
                    }

                    if (params.kecamatan !== null) {
                        this.where('report_location.id_kecamatan', params.kecamatan);
                    }

                    if (params.kelurahan !== null) {
                        this.where('report_location.id_kelurahan', params.kelurahan);
                    }

                    if (params.status !== null) {
                        if (params.status !== '0') {
                            this.whereIn('report_location.id_master_status', params.status);
                        } else {
                            this.whereIn('report_location.id_master_status', [2,3,4,5,6]);
                        }
                    }
                })
                .orderBy('report_location.updated_at', 'desc')
                .then(function (results) {
                    var response = {};

                    let data = results.map(item => {
                        var date = moment(item.created_date);
                        var newDate = date.format("Do MMMM YYYY HH:mm:ss");

                        return {
                            category : item.category,
                            content : item.content,
                            created_date : newDate,
                            id_master_status : item.id_master_status,
                            id_report_source : item.id_report_source,
                            media : item.media,
                            media_format : item.media_format,
                            nama_kota : item.nama_kota,
                            nama_kecamatan : item.nama_kecamatan,
                            nama_kelurahan : item.nama_kelurahan,
                            title : item.title,
                            status_name : item.status_name,
                            username : item.username,
                            geojson : JSON.parse(item.geojson)
                        }
                    });

                    response.total          = data.length;
                    response.data           = data;
                    response.params         = params;

                    resolve(response);
                })
                .catch(function(error) {
                    reject(error);
                })
        });
    }
};

module.exports = data;