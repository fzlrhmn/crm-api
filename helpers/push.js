/**
 * Created by fzlrhmn on 8/9/16.
 */
var sendNotification = function(data) {
    var message = {
        app_id: "863335bd-3f12-4af7-8cb6-7b1273224008",
        contents: {"en": data.message},
        headings:{"en" : "Notifikasi Laporan"},
        include_player_ids: data.app_id_array,
        data:{"id_report" : data.id_report,channel : data.channel}
    };

    var headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic OGUwNmM5YTQtZTBkMC00NjlmLTg0ZTEtNzJkYmM4OTU3ZDNh"
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };

    var https = require('https');
    var req = https.request(options, function(res) {
        res.on('data', function(data) {
            console.log();
            console.log("Response: "+ JSON.parse(data));
        });
    });

    req.on('error', function(e) {
        console.log("ERROR:");
        console.log(e);
    });

    req.write(JSON.stringify(message));
    req.end();
};

module.exports = sendNotification;