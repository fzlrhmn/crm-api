/**
 * Created by fzlrhmn on 7/22/16.
 */
var Slack = require('slack-node');

var log = {
    slackReport : function (msg,error) {
        var slack = new Slack();
        var webhookUri = "https://hooks.slack.com/services/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";
        slack.setWebhook(webhookUri);

        slack.webhook({
            channel: "#crmlog",
            username: "CRM API Log",
            mrkdwn : true,
            text: msg,
            attachments : [
                {
                    color : "danger",
                    text : error
                }
            ]
        }, function(err, response) {
            console.log(response);
        });
    }
}

module.exports = log;