var jwt             = require('jsonwebtoken');
var jwt_config      = require('../config/jwt');
let userModel       = require('../models/user');

module.exports = function (req, res, next) {
    var token = req.header('x-access-token');
    if (token) {
      jwt.verify(token, jwt_config.secret, function(err, decoded) {      
            if (err) {
                return res.send(401, { success: false, message: 'Failed to authenticate token' });
            } else {
                // if everything is good, save to request for use in other routes
                userModel
                    .checkDeletedUser(decoded.id_user)
                    .then(result => {
                        if ( result === 0 ) {
                            return res.send(401, { success: false, message: 'User not found' });
                        } else{
                            req.decoded = decoded;
                            next();
                        }
                    })
                    .catch(error => {
                        return res.send(401, { success: false, message: 'User not found' });
                    })
            }
          });
    }else{
      res.send(403, {
         message : "No Token Provided"
      });
    }
};