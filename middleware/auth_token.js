/**
 * Created by fzlrhmn on 8/16/16.
 */
var jwt     = require('jsonwebtoken');
var jwt_config  = require('../config/jwt');

module.exports = function (req, res, next) {
    var token = req.params.token;
    if (token) {
        jwt.verify(token, jwt_config.secret, function(err, decoded) {
            if (err) {
                return res.send(401, { success: false, message: 'Failed to authenticate token' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    }else{
        res.send(403, {
            message : "No Token Provided"
        });
    }
};