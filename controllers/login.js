/**
 * Created by fzlrhmn on 8/27/16.
 */

let loginModel  = require('../models/login');
let slack       = require('../config/slack');
let userModel   = require('../models/user');

let login = {
    /*
     * Get login credential
     * @param {Object} req
     * @param {Object} res
     *
     * Get login credential and send response as JSON. Contains JWT token that contain user data.
     *
     */
    checkLogin : function (req,res) {
        let params = {};
        params.username         = req.body.username;
        params.password         = req.body.password;
        params.device_model     = req.body.device_model;
        params.device_uuid      = req.body.device_uuid;
        params.device_platform  = req.body.device_platform;
        params.os_version       = req.body.os_version;
        params.longitude        = req.body.longitude;
        params.latitude         = req.body.latitude;

        loginModel
            .checkLogin(params)
            .then(result => {
                res.json(200, result);
            })
            .catch(error => {
                res.json(error.status_code, error);
            })
    },
    /*
     * Get login credential
     * @param {Object} req
     * @param {Object} res
     *
     * Get login credential and send response as JSON. Contains JWT token that contain user data.
     *
     */
    generateToken : function (req,res) {

        userModel
            .getUserDetail(req.params.id_user)
            .then(result => {
                loginModel.createHash(result)
                    .then(result => {
                        res.json(200, result);
                    })
            })
            .catch(error => {

                res.json(error.status_code, error);
            })
    },
    /*
     * Get login credential
     * @param {Object} req
     * @param {Object} res
     *
     * Get login credential and send response as JSON. Contains JWT token that contain user data.
     *
     */
    checkIp : function (req,res) {
        res.json(200,{ip : req.headers});
    }
};

module.exports = login;