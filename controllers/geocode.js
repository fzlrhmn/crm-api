/**
 * Created by fzlrhmn on 8/24/16.
 */

let qlueReport      = require('../models/qlue_report');
let geocodeService  = require('../helpers/geocode');
let cache           = require('../cache/cache');
let logger          = require('../config/log');
let slack			= require('../config/slack');
let async           = require('async');

let geocode = {
    /**
     * Get geocode in physical address from google maps geocode services
     * @param {Object} req
     * @param {Object} res
     *
     * Get physical address from google maps service or table. Check cache first and response in JSON, if there isn't any result, get from google maps service
     * and then set it to cache and table. Send the response.
     *
     */
    getCoordinate : function(req,res) {
        cache
            .getCache('geocode_location_'+req.params.id_report)
            .then(result => {
                res.json(200, result);
            })
            .catch(error => {
                qlueReport.getCoordinate(req.params.id_report)
                    .then((result) => {
                        if (result.address) {
                            cache.setCacheInfiniteDuration('geocode_location_'+req.params.id_report, result);
                            res.json(200, result);
                        }else{
                            geocodeService(result.latitude, result.longitude)
                                .then(result => {
                                    let response = {
                                        latitude : result.latitude,
                                        longitude : result.longitude,
                                        address : result[0].formatted_address
                                    };
                                    qlueReport.setAddress(response,req.params.id_report);
                                    cache.setCacheInfiniteDuration('geocode_location_'+req.params.id_report, response);
                                    res.json(200, response);
                                })
                                .catch(error => {
                                    let paramsError = {
                                        title : __filename,
                                        error : error,
                                        payload : {decoded : req.decoded, params : req.params}
                                    };
                                    slack(paramsError);

                                    logger.log('error', 'error get geocode service : ' + error);
                                    res.json(400, error);
                                })
                        }
                    })
                    .catch((err) => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : {decoded : req.decoded, params : req.params}
                        };
                        slack(paramsError);

                        logger.log('error', 'error get coordinate : ' + error);
                        res.json(400, err);
                    })
            });

    }
};

module.exports = geocode;