/**
 * Created by fzlrhmn on 9/29/16.
 */

let callCenter              = require('../models/call_center');
let fs                      = require('fs');
let queuePushNotification   = require('../queue/queue-push-notification');
let async                   = require('async');
let moment 		            = require('moment');

moment().format();
moment.locale('id');

let data = {
    setCallCenter : (req,res) => {
        let params = {};

        params.ticket           = req.body.ticket;
        params.latitude         = req.body.latitude;
        params.longitude        = req.body.longitude;
        params.status           = req.body.status;
        params.category         = req.body.category;
        params.subcategory      = req.body.subcategory;
        params.location         = req.body.location;
        params.location_typo    = req.body.location_typo;
        params.city             = req.body.city;
        params.kecamatan        = req.body.kecamatan;
        params.created_date     = req.body.created_date;
        params.skpd             = req.body.skpd;

        fs.writeFileSync('./log/call_center/call_center_' + params.ticket + '.json', JSON.stringify(params),'utf8');

        callCenter
            .validateDataReport(params)
            .then(result => {
                let payload = params;
                delete payload.skpd;

                let skpd                = result.filter((value,index,array) => {
                    return array.indexOf(value) == index;
                });

                callCenter
                    .setCallCenter(payload, skpd)
                    .then(result => {
                        if ( typeof result[0] === 'number' ) {
                            async.waterfall([
                                function(callback) {
                                    let notificationSkpd = [];
                                    skpd.forEach(item => {
                                        let notificationPayload = {
                                            id_report : params.ticket,
                                            status : 'all_call_center',
                                            channel : 'call_center_112',
                                            heading : 'Call Center 112 ' + params.category,
                                            message : 'Laporan ' + params.subcategory + ' di ' + params.location
                                        };

                                        notificationPayload.id_skpd = item;

                                        notificationSkpd.push(notificationPayload);
                                    });

                                    callback(null, notificationSkpd);
                                },
                                function(payload, callback) {
                                    payload.forEach(item => {
                                        queuePushNotification(item);
                                    });

                                    callback(null, true)
                                }],
                                function(error, result) {
                                    res.json(200, {
                                        status : 'success',
                                        message : 'Success input data'
                                    });
                                });
                        }else{

                            fs.writeFile('./log/error_insert_call_center_data_result_' + params.ticket + '.json', JSON.stringify(result),'utf8', (err) => {
                                res.json(400, {
                                    status : 'error',
                                    payload : result
                                });
                            });
                        }
                    })
                    .catch(error => {
                        let errorMessage = JSON.parse(error);

                        fs.writeFile('./log/error_insert_call_center_data_' + params.ticket + '.json', JSON.stringify(errorMessage),'utf8', (err) => {
                            if (errorMessage.errno === 1062) {
                                res.json(400, {
                                    status : 'error',
                                    message : 'Duplicate Entry'
                                });
                            }else{
                                res.json(400, {
                                    status : 'error on post data'
                                });
                            }
                        });
                    })
            })
            .catch(error => {
                let errorMessage = {
                    status : 'error',
                    message : error
                };
                res.json(400,errorMessage);
            });
    },
    setCallCenterLog : (req,res) => {
        let params = {};

        params.ticket           = req.body.ticket;
        params.status           = req.body.status;
        params.substatus        = req.body.substatus;
        params.id_skpd          = req.body.skpd;
        params.id_user          = req.body.user;
        params.latitude         = req.body.latitude;
        params.longitude        = req.body.longitude;
        params.message          = req.body.message;
        params.update_source    = req.body.update_source;

        fs.writeFileSync('./log/call_center/call_center_update_' + params.ticket + '.json', JSON.stringify(params),'utf8');

        callCenter
            .validateDataReportLog(params)
            .then(result => {
                let skpd = result;
                callCenter
                    .setCallCenterLog(params)
                    .then(result => {
                        if ( typeof result[0] === 'number' ) {

                            async.waterfall([
                                function(callback) {
                                    let notificationSkpd = [];
                                    skpd.forEach(item => {
                                        let notificationPayload = {
                                            id_report : params.ticket,
                                            status : 'all_call_center',
                                            channel : 'call_center_112',
                                            id_skpd : item,
                                            heading : 'Update Status #' + params.ticket,
                                            message : params.status
                                        };
                                        notificationSkpd.push(notificationPayload);
                                    });
                                    callback(null, notificationSkpd);
                                },
                                function(payload, callback) {
                                    payload.forEach(item => {
                                        queuePushNotification(item);
                                    });
                                    callback(null, true)
                                }
                            ],
                            function(error, result) {
                                res.json(200, {
                                    status : 'success',
                                    message : 'Success input data'
                                });
                            });
                        }else{
                            fs.writeFile('./log/error_insert_call_center_log_'+params.ticket+' '+moment().format("YYYY-MM-DD HH:mm:ss")+'.json',JSON.stringify(params),'utf8', (err) => {
                                res.json(400, {
                                    status : 'error on post data'
                                });
                            });
                        }
                    })
                    .catch(error => {
                        fs.writeFile('./log/error_insert_call_center_log_'+params.ticket+' '+moment().format("YYYY-MM-DD HH:mm:ss")+'.json',error,'utf8', (err) => {
                            res.json(400, {
                                status : 'error on post data'
                            });
                        });
                    })
            })
            .catch(error => {
                let errorMessage = {
                    status : 'error',
                    message : error
                };
                res.json(400,errorMessage);
            });
    },
    getCallCenterReport : (req, res) => {
        callCenter
            .getCallCenterReport(req.decoded.id_skpd, req.params.page)
            .then(result => {
                res.json(200, result);
            })
            .catch(error => {
                res.json(400, {
                    error : error,
                    status : 'error on get data'
                });
            })

    },
    getDetailCallCenterReport : (req, res) => {
        callCenter
            .getDetailCallCenterReport(req.params.ticket)
            .then(result => {
                res.json(200, result);
            })
            .catch(error => {
                res.json(400, {
                    status : 'error on get data'
                });
            })

    },
    getCallCenterReportLog : (req, res) => {
        callCenter
            .getCallCenterReportLog(req.params.ticket)
            .then(result => {
                res.json(200, result);
            })
            .catch(error => {
                res.json(400, {
                    error : error,
                    status : 'error on get data'
                });
            })

    },
    getCallCenterSkpd : (req, res) => {
        callCenter
            .getCallCenterSkpd()
            .then(result => {
                res.json(200, result);
            })
            .catch(error => {
                res.json(400, {
                    error : error,
                    status : 'Error on get SKPD data'
                })
            })
    }
};

module.exports = data;