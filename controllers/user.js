/**
 * Created by fzlrhmn on 8/23/16.
 */

let userModel   = require('../models/user');
let bcrypt 		= require('bcrypt-nodejs');
let cache       = require('../cache/cache');
let logger      = require('../config/log');
var slack 		= require('../config/slack');

let user = {
    /*
     * Get user data from user models
     * @param {Object} req
     * @param {Object} res
     *
     * get list of user data by id user from user model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache.
     *
     */
    getUser : function(req,res,next) {
        let id_user = req.decoded.id_user;

        cache
            .getCache('user_edit_' + id_user)
            .then(result => {
                res.json(200, result);
            })
            .catch(err => {
                userModel.getUser(id_user)
                    .then((result) => {
                        let response = result[0] || {};
                        cache.setCache('user_edit_' + id_user, response);
                        res.json(200, response);
                    })
                    .catch((error) => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get user : ' + error);
                        res.json(400, error);
                    });
            });
    },
    /*
     * Get user detail data from user models
     * @param {Object} req
     * @param {Object} res
     *
     * get list of user detail data by id user from user model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache.
     *
     */
    getUserDetail : function(req,res) {
        let id_user = req.decoded.id_user;

        cache
            .getCache('user_detail_' + id_user)
            .then(result => {
                res.json(200, result);
            })
            .catch(err => {
                userModel.getUserDetail(id_user)
                    .then((result) => {
                        let response = result[0] || {};
                        cache.setCache('user_detail_' + id_user, response);
                        res.json(200, response);
                    })
                    .catch((error) => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get user detail : ' + error);
                        res.json(400, error);
                    });
            })
    },
    /*
     * Update user data
     * @param {Object} req
     * @param {Object} res
     *
     * Update user data
     */
    updateUser : function(req,res) {
        let userPayload = {
            id : req.decoded.id_user,
            full_name : req.body.full_name,
            phone : req.body.phone,
            email : req.body.email,
            alamat : req.body.alamat,
            nir_nik : req.body.nir_nik
        };

        userModel.updateUser(userPayload)
            .then((result) => {
                let response = {
                    message : "Data berhasil dirubah",
                    payload : userPayload
                };

                cache.deleteCache('user_detail_' + req.decoded.id_user);

                res.json(200, response);
            })
            .catch((err) => {
                let response = {
                    message : "Data gagal dirubah",
                    error : err
                };
                res.json(400, response);
            });
    },
    /*
     * Update user password
     * @param {Object} req
     * @param {Object} res
     *
     * Update user password. Use bcrypt as encryption.
     */
    updatePassword :function(req,res) {
        userModel
            .getPassword(req.decoded.id_user)
            .then((result) => {
                // since password stored in bcrypt hashes from PHP, we need to replace first 4 character with nodejs generated string hashes
                let PhpGeneratedHash = result.password;
                let NodeGeneratedHash = PhpGeneratedHash.replace('$2y$', '$2a$');
                let NewPasswordHash = bcrypt.hashSync(req.body.new_password);

                if (bcrypt.compareSync(req.body.old_password, NodeGeneratedHash)) {
                    let userPayload = {
                        id : req.decoded.id_user,
                        password : NewPasswordHash
                    };
                    userModel
                        .updatePassword(userPayload)
                        .then(result => {
                            let response = {
                                message : "Data berhasil dirubah"
                            };
                            res.json(200, response);
                        })
                        .catch((err) => {
                            let response = {
                                message : "Data gagal dirubah",
                                error : err
                            };
                            res.json(400, response);
                        });
                }else{
                    res.json(400, {message: "Password lama tidak cocok"});
                }
            })
    }
};

module.exports = user;