/**
 * Created by fzlrhmn on 10/10/16.
 */

let faqModel = require('../models/faq');

let faqData = {
   getFaqData : (req,res) => {
       faqModel.getFaq()
           .then(result => {
               res.json(200, result);
           })
           .catch(error => {
               res.json(400, {
                   status : 'error',
                   message : 'error on get faq data'
               })
           })
   }
};

module.exports = faqData;