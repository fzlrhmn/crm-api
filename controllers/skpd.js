/**
 * Created by fzlrhmn on 8/26/16.
 */

let skpdModel   = require('../models/skpd');
let cache       = require('../cache/cache');
let logger      = require('../config/log');
let slack       = require('../config/slack');
let md5         = require('md5');


let skpd = {
    /**
     * Get List of SKPD from skpd models
     * @param {Object} req
     * @param {Object} res
     *
     * get list of SKPD from skpd model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    skpdList : function (req,res) {
        cache
            .getCache('skpd')
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                skpdModel
                    .getSkpd(req.decoded.id_user_level)
                    .then(result => {
                        cache.setCacheLongDuration('skpd', result);
                        res.json(200, result);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger('error', 'error get SKPD : ' + error);
                        res.json(400, error)
                    })
            })
    },
    /**
     * Get List of Kelurahan from skpd models
     * @param {Object} req
     * @param {Object} res
     *
     * get list of Kelurahan from skpd model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    kelurahanList : function (req,res) {
        let params = {
            user_level : req.decoded.id_user_level,
            id_wilayah : parseInt(req.decoded.id_wilayah)
        };

        let kelurahanParamsHash = md5(JSON.stringify(params));

        cache
            .getCache('kelurahan_' + kelurahanParamsHash)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                skpdModel
                    .getKelurahan(params)
                    .then(result => {
                        cache.setCacheLongDuration('kelurahan_' + kelurahanParamsHash, result);
                        res.json(200, result);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger('error', 'error get Kelurahan : ' + error);
                        res.json(400, error)
                    })
            })
    },
    /**
     * Get List of SKPD workers from skpd models
     * @param {Object} req
     * @param {Object} res
     *
     * get list of SKPD workers from skpd model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    skpdWorkers : function (req,res) {
        let params = {};
        params.longitude        = req.params.longitude;
        params.latitude         = req.params.latitude;
        params.id_skpd          = req.decoded.id_skpd;
        params.id_user_level    = req.decoded.id_user_level;
        params.id_user          = req.decoded.id_user;

        cache
            .getCache('skpd_workers_'+params.id_user)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                skpdModel
                    .getSkpdWorker(params)
                    .then(result => {
                        cache.setCache('skpd_workers_'+params.id_user, result);
                        res.json(200, result);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger('error', 'error get SKPD workers : ' + error);
                        res.json(400, error)
                    })
            })
    }
};

module.exports = skpd;