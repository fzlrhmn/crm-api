/**
 * Created by fzlrhmn on 8/26/16.
 */

let locationModel   = require('../models/location');
let logger          = require('../config/log');
let cache           = require('../cache/cache');
let slack			= require('../config/slack');

let location = {
    /*
     * Set location of user
     * @param {Object} req
     * @param {Object} res
     *
     * Set location of user periodically
     *
     */
    setLocation : function (req,res) {
        let params = {};
        params.id_user      = req.decoded.id_user;
        params.latitude     = req.body.latitude;
        params.longitude    = req.body.longitude;

        locationModel
            .setLocation(params)
            .then(result => {
                cache.deleteCache('skpd_workers_'+req.decoded.id_skpd);
                res.json(200, {'message' : 'Lokasi Telah Diterima', result : result});
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded,
                    params : params
                };
                slack(paramsError);

                logger('error', 'error when set user location : ' + error);
                res.json(400, {'message' : 'Terjadi Kesalahan', 'error' : error});
            })
    }
};

module.exports = location;