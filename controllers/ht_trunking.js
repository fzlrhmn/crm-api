/**
 * Created by fzlrhmn on 11/11/16.
 */

let htTrunking = require('../models/ht_trunking');

let htTrunkingData = {
    getHtTrunkingData : (req,res) => {
        htTrunking.getHtTrunking()
            .then(result => {
                let response = {};
                response.status = 'Success';
                response.count = result.length;
                response.data = result;

                res.json(200, response);
            })
            .catch(error => {
                res.json(400, {
                    status : 'error',
                    message : 'error on get faq data'
                })
            })
    }
};

module.exports = htTrunkingData;