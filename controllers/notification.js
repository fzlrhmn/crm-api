/**
 * Created by fzlrhmn on 9/23/16.
 */

let notificationUser    = require('../models/user_notification');
let logger              = require('../config/log');

let notification = {
    /*
     * Get notification for specified user
     * @param {Object} id_user
     *
     * get notification for specified user by id user
     *
     */
    getNotification : function(req,res) {
        let data = {
            user_id : req.decoded.id_user,
            page : req.params.page || 1
        };

        notificationUser
            .getNotification(data)
            .then(result => {
                res.json(200, result);
            })
            .catch(error => {
                logger.log('error', 'error get notification list : ' + error);
                let errorMessage = {
                    status : 'Error',
                    message : 'Cannot get notification list'
                };
                res.json(400, errorMessage);
            })
    },
    /*
     * Set notification has been read by report_id and user_id
     * @param {Object} id_user
     *
     * set notification that already read by set read_at
     * by current timestamp by update it by user_notification model
     *
     * if result equals 1, send success message
     * if result equals 0, send error message
     *
     */
    readNotification : function(req,res) {
        let data = {
            id : req.body.id
        };

        notificationUser
            .readNotification(data)
            .then(result => {
                if (result == 1 ) {
                    let data = {
                        status : 'Success',
                        message : 'Notification has been read'
                    };

                    res.json(200, data);
                }else{
                    let errorMessage = {
                        status : 'Error',
                        message : 'Cannot set notification read'
                    };

                    res.json(400, errorMessage);
                }
            })
            .catch(error => {
                logger.log('error', 'error set notification read : ' + error);

                let errorMessage = {
                    status : 'Error',
                    message : 'Cannot set notification read'
                };

                res.json(400, errorMessage);
            })
    }
};

module.exports = notification;