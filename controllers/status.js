/**
 * Created by fzlrhmn on 8/26/16.
 */

let statusModel     = require('../models/status');
let statusCache     = require('../cache/cache');
let logger          = require('../config/log');
var slack 		    = require('../config/slack');

let status = {
    /*
     * Get qlue status resume from status models
     * @param {Object} req
     * @param {Object} res
     *
     * get list of qlue status from status model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache.
     *
     * It is contain count of report group by status id
     *
     */
    getQlueStatus : function (req,res) {
        let params = {};
        params.id_skpd      = req.decoded.id_skpd;
        params.id_wilayah   = req.decoded.id_wilayah;
        params.id_user      = req.decoded.id_user;
        params.id_user_level= req.decoded.id_user_level;

        statusCache
            .getCache('status_qlue_report_' + params.id_user)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                statusModel
                    .getQlueCompletionStatus(params)
                    .then(results => {
                        statusCache.setCache('status_qlue_report_' + params.id_user, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get resume status qlue : ' + error);
                        res.json(400, error);
                    });
            });

    },
    /*
     * Get status ROP resume from status models
     * @param {Object} req
     * @param {Object} res
     *
     * get list of ROP status from status model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache.
     *
     * It is contain count of report group by status id
     *
     */
    getRopStatus : function (req,res) {
        let params = {};
        params.id_skpd      = req.decoded.id_skpd;
        params.id_wilayah   = req.decoded.id_wilayah;
        params.id_user      = req.decoded.id_user;
        params.id_user_level= req.decoded.id_user_level;

        statusCache
            .getCache('status_rop_report_' + params.id_user)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                statusModel
                    .getRopCompletionStatus(params)
                    .then(results => {
                        statusCache.setCache('status_rop_report_' + params.id_user, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get resume status rop : ' + error);
                        res.json(400, error);
                    });
            });
    }
};

module.exports = status;