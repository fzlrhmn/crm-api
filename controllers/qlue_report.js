/**
 * Created by fzlrhmn on 8/25/16.
 */

let qlueModel           = require('../models/qlue_report');
let qlueCache           = require('../cache/cache');
let push                = require('../helpers/push');
let logger              = require('../config/log');
let slack			    = require('../config/slack');

let qlue = {
    /**
     * Get Open Report From Report Location
     * @param {Object} req
     * @param {Object} res
     *
     * get open report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    openReport : function(req,res) {
        const STATUS = 'open';
        let params = qlueModel.paramsConstructor(req.decoded,req.params,STATUS);

        qlueModel
            .getOpenReport(params)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);

                logger.log('error', 'error get open report : ' + error);
                res.json(400, error);
            })
    },
    /**
     * Get Process Report From Report Location
     * @param {Object} req
     * @param {Object} res
     *
     * get on progress report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    processReport : function(req,res) {
        const STATUS = 'process';
        let params = qlueModel.paramsConstructor(req.decoded,req.params,STATUS);

        qlueModel
            .getProcessReport(params)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);
                logger.log('error', 'error get process report : ' + error);
                res.json(400, error);
            })
    },
    /**
     * Get Complete Report From Report Location
     * @param {Object} req
     * @param {Object} res
     *
     * get list of complete report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    completeReport : function(req,res) {
        const STATUS = 'complete';
        let params = qlueModel.paramsConstructor(req.decoded,req.params,STATUS);

        qlueModel
            .getCompleteReport(params)
            .then(results => {
                qlueCache.setCache(params.hash, results);
                res.json(200, results);
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);
                logger.log('error', 'error get complete report : ' + error);
                res.json(400, error);
            })
    },
    /**
     * Get Complete Report From Report Location
     * @param {Object} req
     * @param {Object} res
     *
     * get list of complete report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    coordinationHistoryReport : function(req,res) {
        let params = {};
        params.id_user = req.decoded.id_user;
        params.page = req.params.page;

        qlueModel
            .getCoordinationHistoryReport(params)
            .then(results => {
                qlueCache.setCache('coordination history ' + params.id_user + '_' + params.page, results);
                res.json(200, results);
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);
                logger.log('error', 'error get coordination history report : ' + error);
                res.json(400, error);
            })
    },
    /**
     * Get Detail Report From Report Location
     * @param {Object} req
     * @param {Object} res
     *
     * get detail report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     */
    detailReport : function(req,res) {
        let params          = {};
        params.id_report    = req.params.id_report;
        params.user_level   = req.decoded.id_user_level;
        params.id_skpd      = req.decoded.id_skpd;
        params.id_wilayah   = req.decoded.id_wilayah;
        params.id_user      = req.decoded.id_user;

        qlueCache
            .getCache('report_'+params.id_report)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                qlueModel
                    .getDetailReport(params)
                    .then(result => {
                        qlueCache.setCache('report_'+params.id_report, result);
                        res.json(200, result);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);
                        logger.log('error', 'error get detail report : ' + error);
                        res.json(400, error);
                    })
            });

    },
    /**
     * Get Detail Log Report From Report Location
     * @param {Object} req
     * @param {Object} res
     *
     * Get detail log report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     */
    detailLogReport : function(req,res) {
        let params          = {};
        params.id_report    = req.params.id_report;
        params.user_level   = req.decoded.id_user_level;

        qlueCache
            .getCache('log_report_'+params.id_report)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                qlueModel
                    .getDetailLogReport(params)
                    .then(result => {
                        qlueCache.setCache('log_report_'+params.id_report, result);
                        res.json(200, result);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);
                        logger.log('error', 'error get detail log report : ' + error);
                        res.json(400, error);
                    })
            });

    },
    /**
     * Get Detail Location Report From Report Location
     * @param {Object} req
     * @param {Object} res
     *
     * Get report location in geojson form from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     */
    locationReport : function(req,res) {
        let params          = {};
        params.id_report    = req.params.id_report;
        params.user_level   = req.decoded.id_user_level;

        qlueCache
            .getCache('location_report_'+params.id_report)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                qlueModel
                    .getLocationLogReport(params)
                    .then(result => {
                        qlueCache.setCache('location_report_'+params.id_report, result);
                        res.json(200, result);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);
                        logger.log('error', 'error get location of report : ' + error);
                        res.json(400, error);
                    })
            });

    },
    /**
     * Set Report Dispatch
     * @param {Object} req
     * @param {Object} res
     *
     * Set dispatch report record and send push notification to specified users based on req.body.id_user
     *      - when done, delete cache based on process key and send push notification
     *
     */
    dispatchReport : function(req,res) {
        let params          = {};
        params.id_report_location  = req.body.id_report;
        params.assigner            = req.decoded.id_user;
        params.id_user_detail      = req.body.id_user;
        params.id_skpd             = req.decoded.id_skpd;
        params.comment             = req.body.comment;
        params.latitude            = req.body.latitude;
        params.longitude           = req.body.longitude;

        qlueModel
            .setDispatch(params)
            .then(results => {
                qlueCache.deleteCache('report_' + params.id_report_location);
                qlueCache.deleteCache('log_report_' + params.id_report_location);
                qlueCache.deleteCache('status_qlue_report_' + req.decoded.id_user);

                res.json(200, {message : 'Disposisi Laporan Berhasil dimasukkan'});
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);
                logger.log('error', 'error set dispatch report : ' + error);
                res.json(400, error);
            });

    },
    /**
     * Set Report Dispatch
     * @param {Object} req
     * @param {Object} res
     *
     * Set coordination report record and send push notification to specified users based on req.body.id_skpd
     *      - when done, delete cache based on process key and send push notification
     *
     */
    coordinationReport : function(req,res) {
        let params          = {};
        params.id_report_location  = req.body.id_report;
        params.assigner            = req.decoded.id_user;
        params.id_skpd             = req.body.id_skpd;
        params.comment             = req.body.comment;
        params.latitude            = req.body.latitude;
        params.longitude           = req.body.longitude;

        qlueModel
            .setCoordination(params)
            .then(results => {
                qlueCache.deleteCache('report_'+params.id_report_location);
                qlueCache.deleteCache('log_report_'+params.id_report_location);
                qlueCache.deleteCache('status_qlue_report_' + req.decoded.id_user);

                res.json(200, {message : 'Koordinasi Laporan Berhasil dimasukkan'});
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);
                logger.log('error', 'error set coordination report : ' + error);
                res.json(400, error);
            });

    },
    /**
     * Set Processing Report
     * @param {Object} req
     * @param {Object} res
     *
     * Set processing report record and send push notification to specified users based on req.decoded.id_skpd
     *      - when done, delete cache based on process key and send push notification
     *
     */
    processingReport : function(req,res) {
        let params          = {};
        params.id_report_location  = req.body.id_report;
        params.id_user_detail      = req.decoded.id_user;
        params.username            = req.decoded.username;
        params.id_skpd             = req.decoded.id_skpd;
        params.skpd_name           = req.decoded.nama_skpd;
        params.comment             = req.body.comment;
        params.latitude            = req.body.latitude;
        params.longitude           = req.body.longitude;

        qlueModel
            .setProgress(params)
            .then(results => {
                qlueCache.deleteCache('report_'+params.id_report_location);
                qlueCache.deleteCache('log_report_'+params.id_report_location);
                qlueCache.deleteCache('status_qlue_report_' + req.decoded.id_user);

                res.json(200, {message : 'Silahkan Kerjakan Laporan Anda'});
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);
                logger.log('error', 'error set processing report : ' + error);
                res.json(400, error);
            });
    },
    /**
     * Set Complete Report
     * @param {Object} req
     * @param {Object} res
     *
     * Set complete report record, upload image and send push notification to specified users based on req.decoded.id_skpd
     *      - when done, delete cache based on process key and send push notification
     *
     */
    completingReport : function(req,res) {
        let params          = {};
        params.id_report_location   = parseInt(req.body.id_report);
        params.id_user_detail       = req.decoded.id_user;
        params.username             = req.decoded.username;
        params.id_skpd              = req.decoded.id_skpd;
        params.skpd_name            = req.decoded.nama_skpd;
        params.comment              = req.body.comment;
        params.latitude             = req.body.latitude;
        params.longitude            = req.body.longitude;
        params.file_name            = req.files.file.name;
        params.file_path            = req.files.file.path;

        qlueModel
            .setComplete(params)
            .then(results => {
                qlueCache.deleteCache('report_'+params.id_report_location);
                qlueCache.deleteCache('log_report_'+params.id_report_location);
                qlueCache.deleteCache('status_qlue_report_' + req.decoded.id_user);

                res.json(200, {message : 'Laporan Telah Selesai Dikerjakan'});
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);
                logger.log('error', 'error set complete report : ' + error);
                res.json(400, {status : 'Error', message : 'Terjadi kesalahan. Silahkan ulangi lagi', error : error});
            });
    }
};

module.exports = qlue;