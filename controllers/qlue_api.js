/**
 * Created by fzlrhmn on 10/17/16.
 */

let fetch       = require('node-fetch');
let fs          = require('fs');
let moment 		= require('moment');

moment().format();
moment.locale('id');

let qlueApi = {
    grabQlueReport : (req,res) => {
        fetch('http://sync.qlue.id/crop_generate_data.php')
            .then(res => {
                // if res.status equal 200 and then return it as json, else write the error into file
                if (res.status === 200) {
                    return res.json();
                }else {
                    res.json(400, {status : "error", message : res.json()});
                }
            })
            .then(json => {
                if (typeof json === 'object') {
                    res.json(200, json);
                }else {
                    res.json(400, {status : "error", message : json});
                }
            })
            .catch(err => {
                res.json(400, err);
            })
    },
    grabQlueReportProgress : (req,res) => {
        fetch('http://sync.qlue.id/crop_generate_data_progress.php')
            .then(res => {
                // if res.status equal 200 and then return it as json, else write the error into file
                if (res.status === 200) {
                    return res.json();
                }else {
                    res.json(400, {status : "error", message : res.json()});
                }
            })
            .then(json => {
                if (typeof json === 'object') {
                    res.json(200, json);
                }else {
                    res.json(400, {status : "error", message : json});
                }
            })
            .catch(err => {
                res.json(400, err);
            })
    },
    grabQlueReportUpdated : (req,res) => {
        fetch('http://sync.qlue.id/crop_generate_data_updated.php')
            .then(res => {
                // if res.status equal 200 and then return it as json, else write the error into file
                if (res.status === 200) {
                    return res.json();
                }else {
                    res.json(400, {status : "error", message : res.json()});
                }
            })
            .then(json => {
                if (typeof json === 'object') {
                    res.json(200, json);
                }else {
                    res.json(400, {status : "error", message : json});
                }
            })
            .catch(err => {
                res.json(400, err);
            })
    }
};

module.exports = qlueApi;