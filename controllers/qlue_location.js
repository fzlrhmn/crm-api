/**
 * Created by fzlrhmn on 8/27/16.
 */

let qlueLocationModels  = require('../models/qlue_location');
let cache               = require('../cache/cache');
var logger              = require('../config/log');
let slack               = require('../config/slack');

let location = {
    /**
     * Get All Report From Report Location table
     * @param {Object} req
     * @param {Object} res
     *
     * get all report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache. Response should be in json that contains location of report
     *
     */
    allReport : function (req,res) {
        let params = {};
        params.id_skpd     = req.decoded.id_skpd;
        params.id_wilayah  = req.decoded.id_wilayah;
        params.id_user     = req.decoded.id_user;
        params.user_level  = req.decoded.id_user_level;

        cache
            .getCache('qlue_report_location_' + params.id_user)
            .then(result => {
                res.json(200, result)
            })
            .catch(error => {
                qlueLocationModels
                    .getAllReportLocation(params)
                    .then(results => {
                        cache.setCache('qlue_report_location_' + params.id_user, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get complete report location maps : ' + error);
                        res.json(400, error);
                    })
            });
    },
    /**
     * Get Open Report From Report Location table
     * @param {Object} req
     * @param {Object} res
     *
     * get open report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache. Response should be in json that contains location of report
     *
     */
    openReport : function (req,res) {
        let params = {};
        params.id_skpd     = req.decoded.id_skpd;
        params.id_wilayah  = req.decoded.id_wilayah;
        params.id_user     = req.decoded.id_user;
        params.user_level  = req.decoded.id_user_level;

        cache
            .getCache('qlue_open_report_location_' + params.id_user)
            .then(result => {
                res.json(200, result)
            })
            .catch(error => {
                qlueLocationModels
                    .getOpenReportLocation(params)
                    .then(results => {
                        cache.setCache('qlue_open_report_location_' + params.id_user, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get open report location maps : ' + error);
                        res.json(400, error);
                    })
            });

    },
    /**
     * Get Process  Report From Report Location table
     * @param {Object} req
     * @param {Object} res
     *
     * get process report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache. Response should be in json that contains location of report
     *
     */
    processReport : function (req,res) {
        let params = {};
        params.id_skpd     = req.decoded.id_skpd;
        params.id_wilayah  = req.decoded.id_wilayah;
        params.id_user     = req.decoded.id_user;
        params.user_level  = req.decoded.id_user_level;

        cache
            .getCache('qlue_process_report_location_' + params.id_user)
            .then(result => {
                res.json(200, result)
            })
            .catch(error => {
                qlueLocationModels
                    .getProcessReportLocation(params)
                    .then(results => {
                        cache.setCache('qlue_process_report_location_' + params.id_user, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get process report location maps : ' + error);
                        res.json(400, error);
                    })
            });
    },
    /**
     * Get Complete  Report From Report Location table
     * @param {Object} req
     * @param {Object} res
     *
     * get complete report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache. Response should be in json that contains location of report
     *
     */
    completeReport : function (req,res) {
        let params = {};
        params.id_skpd     = req.decoded.id_skpd;
        params.id_wilayah  = req.decoded.id_wilayah;
        params.id_user     = req.decoded.id_user;
        params.user_level  = req.decoded.id_user_level;

        cache
            .getCache('qlue_complete_report_location_' + params.id_user)
            .then(result => {
                res.json(200, result)
            })
            .catch(error => {
                qlueLocationModels
                    .getCompleteReportLocation(params)
                    .then(results => {
                        cache.setCache('qlue_complete_report_location_' + params.id_user, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get complete report location maps : ' + error);
                        res.json(400, error);
                    })
            });
    },
    /**
     * Search Report From Report Location
     * @param {Object} req
     * @param {Object} res
     *
     * search report from qlue_report model and qlue cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache. Response should be in json that contains location of report
     *
     */
    searchReport: function (req,res) {
        let params = {};
        params.id_skpd      = req.decoded.id_skpd;
        params.id_wilayah   = req.decoded.id_wilayah;
        params.id_user      = req.decoded.id_user;
        params.user_level   = req.decoded.id_user_level;

        // search parameter
        params.keyword      = req.body.keyword || null;
        params.id_laporan   = req.body.id_laporan || null;
        params.date         = req.body.date || null;
        params.category     = req.body.category || null;
        params.dinas        = req.body.dinas || null;
        params.kota         = req.body.kota || null;
        params.kecamatan    = req.body.kecamatan || null;
        params.kelurahan    = req.body.kelurahan || null;
        params.status       = req.body.status || null;

        qlueLocationModels
            .searchReportMaps(params)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);

                logger.log('error', 'error get complete report location maps : ' + error);
                res.json(400, error);
            })
    }
};

module.exports = location;