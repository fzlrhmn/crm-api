/**
 * Created by fzlrhmn on 8/26/16.
 */
let appIdNotification   = require('../models/app_id_notification');
var logger              = require('../config/log');
let slack			    = require('../config/slack');

let appId = {
    /*
     * Set AppId for notification purpose
     * @param {Object} req
     * @param {Object} res
     *
     * Set AppId for notification from user_notification table via app_id_notification models
     */
    setAppId : function(req,res) {

        var id_user = req.decoded.id_user;
        var app_id  = req.body.key;
        var payload = {
            user_id : id_user,
            app_id  : app_id
        };

        appIdNotification
            .setAppId(payload)
            .then(result => {
                res.json(200,{message : 'Key Berhasil Update'});
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);

                logger.log('error', 'error set app_id' + error);
                res.json(400,error);
            })
    },
    /*
     * Remove AppId for notification purpose
     * @param {Object} req
     * @param {Object} res
     *
     * Remove device AppId for notification from user_notification table via app_id_notification models
     */
    removeAppId : function(req,res) {
        let params = {};
        params.key      = req.body.key;

        appIdNotification
            .removeAppId(params)
            .then(result => {
                res.json(200,{message : 'Key Berhasil Dihapus', result : result});
            })
            .catch(error => {
                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : req.decoded
                };
                slack(paramsError);

                logger.log('error', 'error delete app_id' + error);
                res.json(400,error);
            })
    }
};

module.exports = appId;