/**
 * Created by fzlrhmn on 8/27/16.
 */

let ropReport           = require('../models/rop_report');
let skpdModels          = require('../models/skpd');
let cache               = require('../cache/cache');
var logger              = require('../config/log');
let slack               = require('../config/slack');
let userNotification    = require('../models/user_notification');
let push                = require('../helpers/push');

let report = {
    /*
     * Get Open Report From Report Non Location / ROP
     * @param {Object} req
     * @param {Object} res
     *
     * get open report from rop_report model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    openReport : function (req,res) {
        const STATUS = 'open';
        let params = ropReport.paramsConstructor(req.decoded,req.params,STATUS);

        cache
            .getCache(params.hash)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                ropReport
                    .getOpenReport(params)
                    .then(results => {
                        cache.setCache(params.hash, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get open rop report ' + error);
                        res.json(400, error);
                    })
            })
    },
    /*
     * Get Process Report From Report Non Location / ROP
     * @param {Object} req
     * @param {Object} res
     *
     * get process report from rop_report model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    processReport : function (req,res) {
        const STATUS = 'process';
        let params = ropReport.paramsConstructor(req.decoded,req.params,STATUS);

        cache
            .getCache(params.hash)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                console.log(error);
                ropReport
                    .getProcessReport(params)
                    .then(results => {
                        cache.setCache(params.hash, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get process rop report ' + error);
                        res.json(400, error);
                    })
            })
    },
    /*
     * Get Complete Report From Report Non Location / ROP
     * @param {Object} req
     * @param {Object} res
     *
     * get complete report from rop_report model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    completeReport : function (req,res) {
        const STATUS = 'complete';
        let params = ropReport.paramsConstructor(req.decoded,req.params,STATUS);

        cache
            .getCache(params.hash)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                console.log(error);
                ropReport
                    .getCompleteReport(params)
                    .then(results => {
                        cache.setCache(params.hash, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get complete rop report ' + error);
                        res.json(400, error);
                    })
            })
    },
    /*
     * Get Detail Report From Report Non Location / ROP
     * @param {Object} req
     * @param {Object} res
     *
     * get detail report from rop_report model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    detailReport : function (req,res) {
        let params = {};
        params.id_report      = req.params.id_report;
        params.user_level   = req.decoded.id_user_level;

        cache
            .getCache('rop_report_' +params.id_report)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                console.log(error);
                ropReport
                    .getDetailReport(params)
                    .then(results => {
                        cache.setCache('rop_report_' +params.id_report, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get detail rop report ' + params.id_report + ' : ' + error);
                        res.json(400, error);
                    })
            })
    },
    /*
     * Get Log Detail Report From Report Non Location / ROP
     * @param {Object} req
     * @param {Object} res
     *
     * get log detail report from rop_report model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    detailLogReport : function (req,res) {
        let id_report      = req.params.id_report;

        cache
            .getCache('rop_report_log_' +id_report)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                console.log(error);
                ropReport
                    .getDetailLogReport(id_report)
                    .then(results => {
                        cache.setCache('rop_report_log_' + id_report, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get detail log rop report ' + params.id_report + ' : ' + error);
                        res.json(400, error);
                    })
            })
    },
    /*
     * Get List of SKPD Koordinasi based on id group
     * @param {Object} req
     * @param {Object} res
     *
     * get list of skpd koordinasi from skpd model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    getSkpdKoordinasi : function (req,res) {
        let id_user      = req.decoded.id_user;

        cache
            .getCache('skpd_koodinasi_' + id_user)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                skpdModels
                    .getSkpdKoordinasi(req.decoded.id_group)
                    .then(results => {
                        cache.setCacheLongDuration('skpd_koodinasi_' + id_user, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get koordinasi SKPD ' + id_user + ' : ' + error);
                        res.json(400, error);
                    })
            })
    },
    /*
     * Get List of SKPD Disposisi based on id group
     * @param {Object} req
     * @param {Object} res
     *
     * get list of skpd Disposisi from skpd model and cache. If it is not exist in cache, take from database
     * and set it to cache. otherwise send from cache
     *
     */
    getSkpdDisposisi : function (req,res) {
        let id_user      = req.decoded.id_user;

        cache
            .getCache('skpd_disposisi_' + id_user)
            .then(results => {
                res.json(200, results);
            })
            .catch(error => {
                skpdModels
                    .getSkpdDisposisi(req.decoded.id_group)
                    .then(results => {
                        cache.setCacheLongDuration('skpd_disposisi_' + id_user, results);
                        res.json(200, results);
                    })
                    .catch(error => {
                        let paramsError = {
                            title : __filename,
                            error : error,
                            payload : req.decoded
                        };
                        slack(paramsError);

                        logger.log('error', 'error get koordinasi SKPD ' + id_user + ' : ' + error);
                        res.json(400, error);
                    })
            })
    },
    /*
     * ROP report dispatch
     * @param {Object} req
     * @param {Object} res
     *
     * ROP report dispatch
     *
     */
    dispatchReport : function (req,res) {
        let params      = {};
        params.id_report_non_location   = req.body.id_report;
        params.id_skpd                  = req.body.skpd;
        params.comment                  = req.body.comment;
        params.id_master_status         = 4;
        params.assigner                 = req.decoded.id_user;
        let status                      = 'dispatch';

        ropReport
            .setProcessReport(params, status)
            .then(result => {

                cache.deleteCache('rop_report_'+params.id_report_non_location);
                cache.deleteCache('rop_report_log_'+params.id_report_non_location);
                cache.deleteCache('status_rop_report_' + req.decoded.id_user);

                res.json(200, {'message' : 'Disposisi Laporan Berhasil dimasukkan'});
            })
            .catch(error => {
                let reqPayload = {
                    decoded : req.decoded,
                    body    : req.body
                };

                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : reqPayload
                };
                slack(paramsError);

                logger.log('error', 'error set Disposisi ROP ' + params.id_report_non_location + ' : ' + error);
                res.json(400, error);
            })
    },
    /*
     * ROP report coordination
     * @param {Object} req
     * @param {Object} res
     *
     * ROP report coordination
     *
     */
    coordinationReport : function (req,res) {
        let params      = {};
        params.id_report_non_location   = req.body.id_report;
        params.id_skpd                  = req.body.skpd;
        params.comment                  = req.body.comment;
        params.id_master_status         = 5;
        params.assigner                 = req.decoded.id_user;
        let status                      = 'coordination';

        ropReport
            .setProcessReport(params,status)
            .then(result => {

                cache.deleteCache('rop_report_'+params.id_report_non_location);
                cache.deleteCache('rop_report_log_'+params.id_report_non_location);
                cache.deleteCache('status_rop_report_' + req.decoded.id_user);

                res.json(200, {'message' : 'Koordinasi Laporan Berhasil dimasukkan'});
            })
            .catch(error => {
                let reqPayload = {
                    decoded : req.decoded,
                    body    : req.body
                };

                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : reqPayload
                };
                slack(paramsError);

                logger.log('error', 'error set Koordinasi ROP ' + params.id_report_non_location + ' : ' + error);
                res.json(400, error);
            })
    },
    /*
     * Processing ROP report
     * @param {Object} req
     * @param {Object} res
     *
     * Processing ROP Report
     *
     */
    processingReport : function (req,res) {
        let params      = {};
        params.id_report_non_location   = req.body.id_report;
        params.id_skpd                  = req.decoded.id_skpd;
        params.comment                  = req.body.comment;
        params.id_master_status         = 3;
        params.id_user_detail           = req.decoded.id_user;
        let status                      = 'process';

        ropReport
            .setProcessReport(params,status)
            .then(result => {

                cache.deleteCache('rop_report_'+params.id_report_non_location);
                cache.deleteCache('rop_report_log_'+params.id_report_non_location);
                cache.deleteCache('status_rop_report_' + req.decoded.id_user);

                res.json(200,  {'message' : 'Silahkan Kerjakan Laporan Anda'});
            })
            .catch(error => {
                let reqPayload = {
                    decoded : req.decoded,
                    body    : req.body
                };

                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : reqPayload
                };
                slack(paramsError);

                logger.log('error', 'error set Processing ROP ' + params.id_report_non_location + ' : ' + error);
                res.json(400, error);
            })
    },
    /*
     * Completing ROP report
     * @param {Object} req
     * @param {Object} res
     *
     * Completing ROP report
     *
     */
    completingReport : function (req,res) {
        let params      = {};
        params.id_report_non_location   = req.body.id_report;
        params.id_skpd                  = req.decoded.id_skpd;
        params.comment                  = req.body.comment;
        params.id_master_status         = 6;
        params.id_user_detail           = req.decoded.id_user;
        params.file_name                = req.files.file.name;
        params.file_path                = req.files.file.path;
        let status                      = 'complete';

        ropReport
            .setComplete(params)
            .then(result => {

                cache.deleteCache('rop_report_'+params.id_report_non_location);
                cache.deleteCache('rop_report_log_'+params.id_report_non_location);
                cache.deleteCache('status_rop_report_' + req.decoded.id_user);

                res.json(200,  {'message' : 'Laporan Anda Telah Selesai Dikerjakan'});
            })
            .catch(error => {
                let reqPayload = {
                    decoded : req.decoded,
                    body    : req.body
                };

                let paramsError = {
                    title : __filename,
                    error : error,
                    payload : reqPayload
                };
                slack(paramsError);

                logger.log('error', 'error set Complete ROP ' + params.id_report_non_location + ' : ' + error);
                res.json(400, error);
            })
    }
};

module.exports = report;