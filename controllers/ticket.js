/**
 * Created by fzlrhmn on 12/27/16.
 */

let ticketModel         = require('../models/ticket');
let qlueReportModel     = require('../models/qlue_report');
let slack			    = require('../config/slack');
let Terraformer         = require('terraformer');
let cache               = require('../cache/cache');
let geocodeService      = require('../helpers/geocode');
let async               = require('async');

let ticket = {
    getKelurahan : (req,res) => {

        async
            .waterfall([
                (callback) => {
                    let pointPrimitive = new Terraformer.Primitive({
                        type : "Point",
                        coordinates : [req.body.longitude_proposal, req.body.latitude_proposal]
                    });

                    cache
                        .getCache('kelurahan')
                        .then(result => {
                            let data = result.filter(item => {
                                return pointPrimitive.within(item.geojson);
                            });
                            callback(null, {latitude_proposal : req.body.latitude_proposal, longitude_proposal : req.body.longitude_proposal, kelurahan_proposal : data[0]});
                        })
                        .catch(error => {
                            ticketModel
                                .getKelurahan()
                                .then(result => {
                                    cache.setCacheInfiniteDuration('kelurahan', result);

                                    let data = result.filter(item => {
                                        return pointPrimitive.within(item.geojson);

                                    });
                                    callback(null, {latitude_proposal : req.body.latitude_proposal, longitude_proposal : req.body.longitude_proposal, kelurahan_proposal : data[0]});
                                })
                                .catch(error => {
                                    callback(error, null);
                                })
                        })
                },
                (data, callback) => {
                    geocodeService(data.latitude_proposal, data.longitude_proposal)
                        .then(result => {
                            let response = {
                                latitude : result.latitude,
                                longitude : result.longitude,
                                address : result[0].formatted_address
                            };

                            let dataProposal = data;
                            dataProposal.address = response;
                            delete dataProposal.kelurahan_proposal.geojson;
                            callback(null,{data_proposal : dataProposal})
                        })
                        .catch(error => {
                            callback(error, null);
                        })
                },
                (dataProposal, callback) =>{
                    let params          = {};
                    params.id_report    = req.body.id_report;
                    params.user_level   = req.decoded.id_user_level;
                    params.id_skpd      = req.decoded.id_skpd;
                    params.id_wilayah   = req.decoded.id_wilayah;
                    params.id_user      = req.decoded.id_user;
                    qlueReportModel
                        .getDetailReport(params)
                        .then(result => {
                            let data = dataProposal;
                            data.report = result.data;
                            callback(null, data);
                        })
                        .catch(error => {
                            callback(error, null);
                        })
                },
                (data, callback) => {
                    if ( data.data_proposal.kelurahan_proposal.kode_kecamatan === data.report.id_kecamatan ) {
                        data.result = {
                            is_same_kecamatan : true
                        }
                    } else {
                        data.result = {
                            is_same_kecamatan : false
                        }
                    }
                    callback(null, data);
                }
            ], (error, data) => {
                if (error) res.json(400, {message : "error", data : error});
                res.json(200, data);

            })
    },
    getIssueCategories : (req,res) => {
        cache
            .getCache('issue_report')
            .then(result => {
                res.json(200, {status : "success", data : result});
            })
            .catch(error => {
                ticketModel
                    .getIssue()
                    .then(result => {
                        cache.setCacheInfiniteDuration('issue_report', result);
                        res.json(200, {status : "success", data : result});
                    })
                    .catch(error => {
                        res.json(400, {status : "error", message : error});
                    })
            })
    }
};

module.exports = ticket;