// DAO model
let locationController      = require('../controllers/location');
let statusController        = require('../controllers/status');
let skpdController          = require('../controllers/skpd');
let userController          = require('../controllers/user');
let geocode                 = require('../controllers/geocode');
let qlueController 	        = require('../controllers/qlue_report');
let ropController 	        = require('../controllers/rop_report');
let appIdController         = require('../controllers/app_id_notification');
let qlueLocationController  = require('../controllers/qlue_location');
let loginController         = require('../controllers/login');
let auth				    = require('../middleware/auth');
let notification            = require('../controllers/notification');
let callCenter              = require('../controllers/call_center');
let faqController           = require('../controllers/faq');
let qlueApiController       = require('../controllers/qlue_api');
let htTrunkingController    = require('../controllers/ht_trunking');
let ticketController        = require('../controllers/ticket');

module.exports = function (server) {

	// for login, doesn't use auth middleware
    server.post('/login' , loginController.checkLogin);
    server.get('/generate/token/:id_user' , loginController.generateToken);
    server.get('/check/ip' , loginController.checkIp);

    // Report ROP
	server.get('/report/rop/open', auth ,ropController.openReport);
    server.get('/report/rop/process', auth ,ropController.processReport);
    server.get('/report/rop/complete', auth ,ropController.completeReport);

    // POST Report ROP
    server.post('/report/rop/disposisi', auth ,ropController.dispatchReport);
    server.post('/report/rop/koordinasi', auth ,ropController.coordinationReport);
    server.post('/report/rop/process', auth ,ropController.processingReport);
    server.post('/report/rop/finish', auth ,ropController.completingReport);

    // Detail ROP report
    server.get('/report/rop/:id_report', auth ,ropController.detailReport);
    server.get('/report/rop/log/:id_report', auth ,ropController.detailLogReport);

    // GET Report Qlue
    server.get('/report/qlue/open', auth ,qlueController.openReport);
    server.get('/report/qlue/process', auth ,qlueController.processReport);
    server.get('/report/qlue/complete', auth ,qlueController.completeReport);
    server.get('/report/qlue/coordination/history', auth ,qlueController.coordinationHistoryReport);

    // POST Report QLUE
    server.post('/report/qlue/disposisi', auth ,qlueController.dispatchReport);
    server.post('/report/qlue/koordinasi', auth ,qlueController.coordinationReport);
    server.post('/report/qlue/progress', auth ,qlueController.processingReport);
    server.post('/report/qlue/finish', auth, qlueController.completingReport);

    // Detail Qlue Report
    server.get('/report/qlue/:id_report', auth ,qlueController.detailReport);
    server.get('/report/qlue/log/:id_report', auth ,qlueController.detailLogReport);
    server.get('/report/qlue/location/:id_report', auth ,qlueController.locationReport);

    // Status Resume
    server.get('/qlue/status', auth ,statusController.getQlueStatus);
    server.get('/rop/status', auth ,statusController.getRopStatus);

    // SKPD
    server.get('/skpd', auth ,skpdController.skpdList);
    server.get('/kelurahan', auth ,skpdController.kelurahanList);
    server.get('/skpd/workers', auth ,skpdController.skpdWorkers);
    server.get('/skpd/koordinasi', auth ,ropController.getSkpdKoordinasi);
    server.get('/skpd/disposisi', auth ,ropController.getSkpdDisposisi);

    // SET LOCATION
    server.post('/location', auth, locationController.setLocation);
    server.post('/key', auth, appIdController.setAppId);
    server.post('/removekey', auth, appIdController.removeAppId);

    // REPORT LOCATION GET
    server.get('/report/location', auth ,qlueLocationController.allReport);
    server.get('/report/location/open', auth ,qlueLocationController.openReport);
    server.get('/report/location/process', auth ,qlueLocationController.processReport);
    server.get('/report/location/complete', auth ,qlueLocationController.completeReport);
    server.post('/maps/search', auth ,qlueLocationController.searchReport);

    // USER SETTING
    server.get('/user', auth, userController.getUser);
    server.get('/user/detail', auth, userController.getUserDetail);
    server.post('/user', auth, userController.updateUser);
    server.post('/password', auth, userController.updatePassword);

    // GEOCODING
    server.get('/geocode', auth, geocode.getCoordinate);

    // NOTIFICATION
    server.get('/notification', auth, notification.getNotification);
    server.post('/notification/read', auth, notification.readNotification);

    // FAQ
    server.get('/faq', auth, faqController.getFaqData);

    // CALL CENTER 112
    server.post('/callcenter', callCenter.setCallCenter);
    server.post('/callcenter/update', callCenter.setCallCenterLog);
    server.get('/callcenter/skpd', callCenter.getCallCenterSkpd);
    server.get('/callcenter', auth, callCenter.getCallCenterReport);
    server.get('/callcenter/:ticket', auth, callCenter.getDetailCallCenterReport);
    server.get('/callcenter/log/:ticket', auth, callCenter.getCallCenterReportLog);

    server.get('/qlue/report', qlueApiController.grabQlueReport);
    server.get('/qlue/report/updated', qlueApiController.grabQlueReportUpdated);
    server.get('/qlue/report/log', qlueApiController.grabQlueReportProgress);

    server.get('/httrunking', htTrunkingController.getHtTrunkingData);

    // TICKET
    server.post('/coordinate/check', auth, ticketController.getKelurahan);
    server.get('/issue', auth, ticketController.getIssueCategories);
};