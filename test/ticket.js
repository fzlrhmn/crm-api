/**
 * Created by fzlrhmn on 12/27/16.
 */
let chai        = require('chai');
let assert      = chai.assert;
let expect      = chai.expect;
let should      = chai.should();
let supertest   = require('supertest');
let moment      = require('moment');
let R           = require('ramda');
let ticketModel = require('../models/ticket');
let Terraformer = require('Terraformer');
let server      = supertest.agent("http://localhost:3301");

let login = function() {
    return new Promise((resolve, reject) => {
        let loginData = {
            username : "faisalrahman",
            password : "12345",
            latitude : "-6.18156",
            longitude : "106.828655",
            device_model : "device_model",
            device_uuid : "device_uuid",
            device_platform : "device_platform",
            os_version : "os_version"
        };

        server
            .post('/login')
            .send(loginData)
            .end(function(error,result) {
                if (error) reject(error);
                resolve(result.body.token);
            })
    });
};

describe('ticket test module', function() {
    this.timeout(100000);

    it('should return array of kelurahan data', function(done) {
        ticketModel
            .getKelurahan()
            .then(result => {
                //console.log(result);
                result.forEach(item => {
                    expect(item.kode_kota).to.be.a('number');
                    expect(item.geojson).to.be.a('object');
                    let polygon = new Terraformer.Primitive(item.geojson);
                    expect(polygon instanceof Terraformer.Polygon).to.equal(true);

                });
                done()
            })
            .catch(error => {
                done(error)
            })
    });

    it('should check coordinate to get correct area', function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .post('/coordinate/check')
                    .set('x-access-token',result)
                    .send({
                        "latitude_proposal" : -6.144347,
                        "longitude_proposal" : 106.750710,
                        "id_report" : 699625
                    })
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) done(error);
                        expect(result.body.result.is_same_kecamatan).to.equal(false);
                        done();
                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it('should return master issue data', function(done) {
        ticketModel
            .getIssue()
            .then(result => {
                assert.isArray(result);
                done();
            })
            .catch(error => {
                done(error);
            })
    });

    it('should get master issue data with API', function (done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/issue')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) done(error);
                        assert.isObject(result.body);
                        done();
                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it('should return error', (done) => {
        ticketModel
            .setIssue({message : null})
            .then(result => {
                done(result);
            })
            .catch(error => {
                done();
            })
    });

    it('should set move report location data', (done) => {
        ticketModel
            .setIssue({
                id_report : 700246,
                id_issue : 2,
                issue_code : 'PL',
                id_user : 36,
                content : 'Laporan Salah',
                latitude_proposal : -6.144347,
                longitude_proposal : 106.75071,
                kode_kota : 3174,
                kode_kecamatan : 3174070,
                kode_kelurahan : 3174070003,
                address : "Jl. Zamrud Blok D No.104, Kedaung Kali Angke, Cengkareng, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11720, Indonesia",
                is_same_kecamatan : true
            }, {
                "code": "Success",
                "id_user": 36,
                "username": "adminjsc6",
                "fullname": "Lurah Kedoya Selatan",
                "id_user_level": 8,
                "id_group": 4,
                "id_skpd": 1628,
                "nama_skpd": "KELURAHAN KEDOYA SELATAN",
                "id_wilayah": "3174020006"
            })
            .then(result => {
                done();
            })
            .catch(error => {
                done(error);
            })
    });
});