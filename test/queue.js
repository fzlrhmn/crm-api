/**
 * Created by fzlrhmn on 9/14/16.
 */
let chai    = require('chai');
let assert  = chai.assert;
let expect  = chai.expect;
let queue   = require('../models/user_notification');

describe('User Queue rabbitmq', function() {

    it('should return user', function(done) {
        queue.getNotificationUser(54, 'coordination', function(err, data) {
            if (err) {
                done(err);
            }else{
                assert.isArray(data);
                done();
            }
        });
    });

    it('should insert user', function(done) {
        let data = [
            {
                id_report: 592626,
                user_id: '235586b2-8c88-4e1f-8efa-c127877da2c2',
                title: 'Notifikasi Laporan CRM',
                message: 'Anda Menerima Koordinasi Laporan #592626'
            },
            {
                id_report: 592626,
                user_id: 'd1acba27-cf46-4606-baf1-d5d128afd7cb',
                title: 'Notifikasi Laporan CRM',
                message: 'Anda Menerima Koordinasi Laporan #592626'
            },
            {   id_report: 592626,
                user_id: '5515ccc7-20d7-4a62-91f5-5203905b4de7',
                title: 'Notifikasi Laporan CRM',
                message: 'Anda Menerima Koordinasi Laporan #592626'
            },
            {   id_report: 592626,
                user_id: 'c879bdf9-1e55-49f0-95a3-45f553843ddf',
                title: 'Notifikasi Laporan CRM',
                message: 'Anda Menerima Koordinasi Laporan #592626'
            }
        ];

        queue
            .setNotification(data)
            .then(res => {
                done();
            })
            .catch(error => {
                done(error);
            });
    });
});