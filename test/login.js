/**
 * Created by fzlrhmn on 8/27/16.
 */

let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
let login = require('../models/login');

describe('login module', function () {
    it('should have response in array and have 1 index', function(done) {
        let params = {};
        params.username = 'adminjsc1';
        login
            .checkLogin(params)
            .then(result => {
                assert.isArray(result);

                done();
            })
            .catch(error => {
                done(error);
            })
    });

    //it('should in form of object', function(done) {
    //    login
    //        .createObject()
    //        .then(result => {
    //            assert.isObject(result);
    //            expect(result).to.have.property('id_user');
    //            done();
    //        })
    //        .catch(error => {
    //            done(error);
    //        })
    //});
});