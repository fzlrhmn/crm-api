/**
 * Created by fzlrhmn on 9/6/16.
 */

let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
let skpdModels   = require('../models/skpd');

describe('SKPD models' , function() {

    it('should get SKPD Koordinasi response from database and in JSON response' , function(done) {
        skpdModels.getSkpdKoordinasi(2)
            .then(result => {
                assert.isObject(result);
                assert.isArray(result.data);
                done();
            })
            .catch(error => {
                done(error);
            })
    });

    it('should get SKPD Disposisi response from database and in JSON response' , function(done) {
        skpdModels.getSkpdDisposisi(2)
            .then(result => {
                assert.isObject(result);
                assert.isArray(result.data);
                done();
            })
            .catch(error => {
                done(error);
            })
    })
});