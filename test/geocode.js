/**
 * Created by fzlrhmn on 8/24/16.
 */
let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
let qlueReport = require('../models/qlue_report');
let geocode = require('../helpers/geocode');

describe('Geocode Module', function() {

    it('should be done with object and have coordinate', function(done) {
        qlueReport.getCoordinate(592629)
            .then((result) => {
                assert.isObject(result);
                expect(result).to.have.property('latitude',106.698647);
                expect(result).to.have.property('longitude',-6.151095);
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be get geocode', function(done) {
        geocode(-6.151095,106.698647)
            .then((result) => {
                done()
            })
            .catch((err) => {
                done(err);
            })
    })
});