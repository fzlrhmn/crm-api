/**
 * Created by fzlrhmn on 9/27/16.
 */

let chai        = require('chai');
let assert      = chai.assert;
let expect      = chai.expect;
let should      = chai.should();
let supertest   = require('supertest');
let moment      = require('moment');
let R           = require('ramda');

moment().format();
moment.locale('id');

let server = supertest.agent("http://localhost:3301");

let login = function() {
    return new Promise((resolve, reject) => {
        let loginData = {
            username : "faisalrahman",
            password : "12345",
            latitude : "-6.18156",
            longitude : "106.828655",
            device_model : "device_model",
            device_uuid : "device_uuid",
            device_platform : "device_platform",
            os_version : "os_version"
        };

        server
            .post('/login')
            .send(loginData)
            .end(function(error,result) {
                if (error) reject(error);
                resolve(result.body.token);
            })
    });
};

describe('Qlue Report Module', function() {
    this.timeout(100000);

    it("should login", function(done) {
        let loginData = {
            username : "faisalrahman",
            password : "12345",
            latitude : "-6.18156",
            longitude : "106.828655",
            device_model : "device_model",
            device_uuid : "device_uuid",
            device_platform : "device_platform",
            os_version : "os_version"
        };

        server
            .post('/login')
            .send(loginData)
            .end(function(error,result) {
                if (error) done(error);
                assert.isString(result.body.token);
                done();
            })
    });

    it("should get open qlue report data sort by default updated date descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/open')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.updated_at);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].updated_at);

                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it("should get open qlue report data sort by updated date parameter descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/open?sort_by=updated')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.updated_at);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].updated_at);

                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it("should get open qlue report data sort by created date parameter descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/open?sort_by=created')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.created_date);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].created_date);
                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it("should get process qlue report data sort by default updated date descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/process')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.updated_at);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].updated_at);

                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it("should get process qlue report data sort by updated date parameter descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/process?sort_by=updated')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.updated_at);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].updated_at);

                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it("should get process qlue report data sort by created date parameter descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/process?sort_by=created')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.created_date);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].created_date);
                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it("should get complete qlue report data sort by default updated date descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/complete')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.updated_at);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].updated_at);

                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it("should get complete qlue report data sort by updated date parameter descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/complete?sort_by=updated')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.updated_at);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].updated_at);

                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });

    it("should get complete qlue report data sort by created date parameter descending", function(done) {
        login()
            .then(result => {
                assert.isString(result);

                server
                    .get('/report/qlue/complete?sort_by=created')
                    .set('x-access-token',result)
                    .expect(200)
                    .expect('Content-Type', /json/)
                    .end((error, result) => {
                        if (error) {
                            done(error);
                        } else {
                            expect(result.body).to.have.property('data');
                            expect(result.body).to.have.property('count_data');

                            let reportData = result.body.data;
                            let reportDataCount = reportData.length;

                            let sortDateArrayBool = [];

                            reportData.forEach((item, index) => {
                                let firstDate = moment(item.created_date);

                                if (index < reportDataCount-1) {
                                    let nextDate = moment(reportData[index+1].created_date);
                                    if (firstDate.isAfter(nextDate) || firstDate.isSame(nextDate)) {
                                        sortDateArrayBool.push(true);
                                    } else {
                                        sortDateArrayBool.push(false);
                                    }
                                }
                            });

                            if (R.contains(false, sortDateArrayBool) === true ) {
                                done(new Error('Bad Sort'));
                            } else{
                                done();
                            }
                        }

                    });


            })
            .catch(error => {
                done(error)
            })
    });
});