/**
 * Created by fzlrhmn on 11/22/16.
 */

let chai        = require('chai');
let assert      = chai.assert;
let expect      = chai.expect;
let should      = chai.should();
let qlueReports = require('../models/qlue_report');

describe('Coordination History module', function() {
    it('should return as array of object and has more than 0 array element', function(done) {
        let params = {};
        params.id_user = 26599;
        params.page = 1;
        qlueReports.getCoordinationHistoryReport(params)
            .then((result) => {
                assert.isArray(result.data);
                expect(result.data.length).to.be.above(0);
                done();
            })
            .catch((error) => {
                done(error)
            })
    });
});