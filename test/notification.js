/**
 * Created by fzlrhmn on 9/23/16.
 */
let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
let should = chai.should();
let UserNotification = require('../models/user_notification');

describe('Notification Module', function() {

    it('should be done with object and have coordinate', function(done) {
        let data = {
            user_id : 36
        };
        UserNotification.getNotification(data)
            .then((result) => {
                assert.isArray(result);
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be set read_at column', function(done) {
        let data = {
            id : '123a'
        };

        UserNotification
            .readNotification(data)
            .then(result => {
                expect(result).to.equal(1);
                done();
            })
            .catch(error => {
                done(error);
            })
    })
});