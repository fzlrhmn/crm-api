/**
 * Created by fzlrhmn on 9/29/16.
 */
let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
let should = chai.should();
let callCenterModels = require('../models/call_center');

describe('Call Center Module', function() {

    it('should be set report', function(done) {
        let data = {
            "ticket" : "QGY201609231s34370a3",
            "latitude" : -6.1769186,
            "longitude" : 106.8273546,
            "skpd" : [3710,58],
            "status" : "open",
            "category" : "Non Emergency",
            "subcategory" : "Pengamen",
            "location" : "jalan medan merdeka Selatan",
            "location_typo" : "Monas",
            "city" : 3173,
            "kecamatan" : 3173080,
            "created_date" : "2016-10-20 16:17:00"
        };
        callCenterModels.setCallCenter(data)
            .then((result) => {
                assert.isArray(result);
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be done with object form', function(done) {
        let data = {
            ticket : 'QGY201609231s34370a3'
        };
        callCenterModels.getDetailCallCenterReport(data.ticket)
            .then((result) => {
                assert.isObject(result);
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be done with array of report data', function(done) {
        let data = {
            skpd : 58
        };
        callCenterModels.getCallCenterReport(data.skpd)
            .then((result) => {
                assert.isObject(result);
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be set report log', function(done) {
        let data = {
            ticket : 'QGY201609231s34370a3',
            status : 'closed',
            id_skpd : 3710,
            id_user : 3248605,
            latitude : -6.28473,
            longitude : 106.37854,
            message : 'Sedang di perjalanan',
            update_source : 'HT Trunking'
        };
        callCenterModels.setCallCenterLog(data)
            .then((result) => {
                assert.isArray(result);
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be fail validate data payload with null ticket', function(done) {
        let data = {
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : 3173080,
            "created_date" : "2016-09-29 12:00:00"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //assert.isArray(result);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data payload with empty ticket', function(done) {
        let data = {
            "ticket" : "",
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : 3173080,
            "created_date" : "2016-09-29 12:00:00"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //assert.isArray(result);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data payload with wrong timestamp', function(done) {
        let data = {
            "ticket" : "QGY2016092213261",
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : 3173080,
            "created_date" : "2016-09-29 12:00:00.979890"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //assert.isArray(result);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data payload with wrong ticket and timestamp', function(done) {
        let data = {
            "ticket" : null,
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : 3173080,
            "created_date" : "2016-09-29 12:00:00.0978"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //assert.isArray(result);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data payload with empty skpd', function(done) {
        let data = {
            "ticket" : "QGY2016092213261",
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : 3173080,
            "created_date" : "2016-09-29 12:00:00"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //assert.isArray(result);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data payload with wrong city code', function(done) {
        let data = {
            "ticket" : "QGY2016092213261",
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3178,
            "kecamatan" : 3173080,
            "created_date" : "2016-09-29 12:00:00"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //assert.isArray(result);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail data payload with null kecamatan', function(done) {
        let data = {
            "ticket" : "QGY2016092213261",
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : null,
            "created_date" : "2016-09-29 12:00:00"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //assert.isArray(result);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail data payload with wrong kecamatan', function(done) {
        let data = {
            "ticket" : "QGY2016092213261",
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : 31730807887,
            "created_date" : "2016-09-29 12:00:00"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //assert.isArray(result);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data payload with null status', function(done) {
        let data = {
            "ticket" : "QGY2016092213261",
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : null,
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : 3173080,
            "created_date" : "2016-09-29 12:00:00"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                //result.should.have.length(0);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be success validate data payload with empty result', function(done) {
        let data = {
            "ticket" : "QGY2016092213261",
            "latitude" : -6.144347,
            "longitude" : 106.750710,
            "skpd" : [58,3710,58],
            "status" : "open",
            "category" : "emergency",
            "subcategory" : "Kebakaran",
            "location" : "jalan asdf",
            "location_typo" : "dlvfysgfu",
            "city" : 3173,
            "kecamatan" : 3173080,
            "created_date" : "2016-09-29 12:00:00"
        };
        callCenterModels.validateDataReport(data)
            .then((result) => {
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be fail validate data progress payload with wrong ticket', function(done) {
        let data = {
            "ticket" : "QGY2016092213259123",
            "latitude" : -6.144347,
            "longitude" : 106.1234567,
            "id_skpd" : 3710,
            "id_user" : 2343497,
            "status" : "ON PROGRESS",
            "substatus" : "on process",
            "message" : "Sedang perjalanan",
            "update_source" : "HT Trunking"
        };
        callCenterModels.validateDataReportLog(data)
            .then((result) => {
                //result.should.have.length(0);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data progress payload with null status', function(done) {
        let data = {
            "ticket" : "QGY2016092213259",
            "latitude" : -6.144347,
            "longitude" : 106.1234567,
            "id_skpd" : 3710,
            "id_user" : 2343497,
            "status" : null,
            "substatus" : "on process",
            "message" : "Sedang perjalanan",
            "update_source" : "HT Trunking"
        };
        callCenterModels.validateDataReportLog(data)
            .then((result) => {
                //result.should.have.length(0);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data progress payload with null skpd', function(done) {
        let data = {
            "ticket" : "QGY2016092213259",
            "latitude" : -6.144347,
            "longitude" : 106.1234567,
            "id_skpd" : null,
            "id_user" : 2343497,
            "status" : "ON PROGRESS",
            "substatus" : "on process",
            "message" : "Sedang perjalanan",
            "update_source" : "HT Trunking"
        };
        callCenterModels.validateDataReportLog(data)
            .then((result) => {
                //result.should.have.length(0);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data progress payload with wrong skpd', function(done) {
        let data = {
            "ticket" : "QGY2016092213259",
            "latitude" : -6.144347,
            "longitude" : 106.1234567,
            "id_skpd" : "123rd",
            "id_user" : 2343497,
            "status" : "ON PROGRESS",
            "substatus" : "on process",
            "message" : "Sedang perjalanan",
            "update_source" : "HT Trunking"
        };
        callCenterModels.validateDataReportLog(data)
            .then((result) => {
                //result.should.have.length(0);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be fail validate data progress payload with wrong latitude', function(done) {
        let data = {
            "ticket" : "QGY2016092213259",
            "latitude" : -6,
            "longitude" : 106.1234567,
            "id_skpd" : 3710,
            "id_user" : 2343497,
            "status" : "ON PROGRESS",
            "substatus" : "on process",
            "message" : "Sedang perjalanan",
            "update_source" : "HT Trunking"
        };
        callCenterModels.validateDataReportLog(data)
            .then((result) => {
                //result.should.have.length(0);
                //done();
            })
            .catch((err) => {
                done();
            })
    });

    it('should be success validate log data payload with empty result', function(done) {
        let data = {
            "ticket" : "QGY201609231s34370a3",
            "latitude" : -6.123,
            "longitude" : 106.1234567,
            "id_skpd" : 3710,
            "id_user" : 2343497,
            "status" : "ON PROGRESS",
            "substatus" : "on process",
            "message" : "Sedang perjalanan",
            "update_source" : "HT Trunking"
        };
        callCenterModels.validateDataReportLog(data)
            .then((result) => {
                expect(result).have.length.above(1);
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be success validate log data payload without point location', function(done) {
        let data = {
            "ticket" : "QGY201609231s34370a3",
            "id_skpd" : 3710,
            "id_user" : 2343497,
            "status" : "ON PROGRESS",
            "substatus" : "on process",
            "message" : "Sedang perjalanan",
            "update_source" : "HT Trunking"
        };
        callCenterModels.validateDataReportLog(data)
            .then((result) => {
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be done with array of report data logs', function(done) {
        let data = {
            ticket : 'QGY2016092213258'
        };
        callCenterModels.getCallCenterReportLog(data.ticket)
            .then((result) => {
                assert.isObject(result);
                result.should.be.a('object');
                result.should.have.any.keys('data','count');
                result.data.should.be.a('array');
                done();
            })
            .catch((err) => {
                done(err);
            })
    });

    it('should be done with array of SKPD data', function(done) {

        callCenterModels.getCallCenterSkpd()
            .then((result) => {
                result.should.be.a('array');
                result.forEach(e => {
                    expect(e.is_active).to.equal(true);
                });
                done();
            })
            .catch((err) => {
                done(err);
            })
    });
});