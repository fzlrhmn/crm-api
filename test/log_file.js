/**
 * Created by fzlrhmn on 1/3/17.
 */
let chai        = require('chai');
let assert      = chai.assert;
let expect      = chai.expect;
let should      = chai.should();
let fs          = require('fs');
let async       = require('async');
let logFiles    = require('../models/log_file');

let readAsync = (fileDir, callback) => {
    fs.readFile(fileDir, 'utf8', callback);
}

describe('log file module', function() {
    this.timeout(20000);
    it('should return array of files', function(done) {
        logFiles
            .getData('2016-12-22')
            .then(result => {
                assert.isArray(result);
                done();
            })
            .catch(error => {
                done(error);
            })
    });

    it('should read data', function(done) {
        logFiles
            .getData('2016-12-22')
            .then(result => {
                assert.isArray(result);

                let reportData = [];
                result.forEach(item => {
                    let reports = fs.readFileSync(item, 'utf8');
                    reportData.push(JSON.parse(reports));
                });
                done();
            })
            .catch(error => {
                done(error);
            })
    })
});