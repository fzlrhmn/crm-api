/**
 * Created by fzlrhmn on 9/5/16.
 */
let chai    = require('chai');
let assert  = chai.assert;
let expect  = chai.expect;
let queue   = require('../config/queue');
let qlue    = require('../models/qlue_report');
let push    = require('../models/push_request');

describe('Send Status Qlue', function() {
    this.timeout(100000);

    it('should response in object from qlue', function(done) {
        let payload = {
            url : 'http://services.qluein.org/qluein_changePostStatus.php?id=595387&status=wait'
        };
        qlue.sendStatus(payload.url)
            .then(result => {
                assert.isObject(result);
                expect(result.success).to.equal('true');
                done();
            })
            .catch(error => {
                done(error);
            })
    });

    it('should send notification', function(done) {

    });
});