/**
 * Created by fzlrhmn on 8/23/16.
 */
let chai = require('chai');
let assert = chai.assert;
let expect = chai.expect;
let userModel   = require('../models/user');
let userNotification   = require('../models/user_notification');
let supertest = require('supertest');

let local = supertest.agent('http://localhost:3301');

describe('user model', function() {
    this.timeout(100000);

    it('should response in array (get notification user)', function(done) {
        userNotification.getNotificationUser(3171060006, 'wait', function(err, data) {
            if (err) {
                done(err);
            }else{
                assert.isObject(data);
                done();
            }
        });
    });

    it('should response in array, contain 1 index', function(done) {
        userModel.getUser(1)
            .then((result) => {
                expect(result).to.have.length(1);
                done();
            })
            .catch((error) => {
                done(error);
            });
    });

    it('should response in object, got 200', function(done) {
        local
            .get('/user')
            .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb2RlIjoiU3VjY2VzIiwiaWRfdXNlciI6MSwidXNlcm5hbWUiOiJha2JhciIsImZ1bGxuYW1lIjoiYWtiYXIiLCJpZF91c2VyX2xldmVsIjo0LCJpZF9ncm91cCI6bnVsbCwiaWRfc2twZCI6MjUxLCJuYW1hX3NrcGQiOiJLQU5UT1IgS0VMVVJBSEFOIENJREVORyBLRUNBTUFUQU4gR0FNQklSIEpBS1BVUyIsImlkX3dpbGF5YWgiOiIzMTczMDgwMDAxIiwiaWF0IjoxNDcxOTQzMzUxLCJleHAiOjE1MDM0NzkzNTF9.SBzrq8mIGXLKbY2UpaIW7QDpBvwPUPSxdV3-WgPPDq8')
            .expect("Content-type", /json/)
            .expect(200)
            .end((err,res) => {
                if (err) { return done(err) }
                // HTTP status should be 200
                expect(res.statusCode).equal(200);
                assert.isObject(res);
                assert.equal(res.body.id, 1);
                expect(res.body.id).equal(1);
                done();
            });
    });

    it('should update user data', function(done) {
        let userPayload = {
            id : 1,
            full_name : "akbarjsc",
            phone : 123,
            email : "akbarkusumanegaralth@gmail.com",
            alamat : "jalan pisang nomor 10",
            nir_nik : "nik_akbar"
        };
        userModel.updateUser(userPayload)
            .then((result) => {
                expect(result).equal(1);
                done();
            })
            .catch((error) => {
                done(error);
            });
    });

    it('should response in object, got 200', function(done) {
        let userPayload = {
            full_name : "akbarjsc123",
            phone : 123,
            email : "akbarkusumanegaralth@gmail.com",
            alamat : "jalan pisang nomor 10",
            nir_nik : "nik_akbar"
        };
        local
            .post('/user')
            .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb2RlIjoiU3VjY2VzIiwiaWRfdXNlciI6MSwidXNlcm5hbWUiOiJha2JhciIsImZ1bGxuYW1lIjoiYWtiYXIiLCJpZF91c2VyX2xldmVsIjo0LCJpZF9ncm91cCI6bnVsbCwiaWRfc2twZCI6MjUxLCJuYW1hX3NrcGQiOiJLQU5UT1IgS0VMVVJBSEFOIENJREVORyBLRUNBTUFUQU4gR0FNQklSIEpBS1BVUyIsImlkX3dpbGF5YWgiOiIzMTczMDgwMDAxIiwiaWF0IjoxNDcxOTQzMzUxLCJleHAiOjE1MDM0NzkzNTF9.SBzrq8mIGXLKbY2UpaIW7QDpBvwPUPSxdV3-WgPPDq8')
            .send(userPayload)
            .expect("Content-type", /json/)
            .expect(200)
            .end((err,res) => {
                if (err) { return done(err) }
                // HTTP status should be 200
                expect(res.statusCode).equal(200);
                assert.isObject(res);
                assert.equal(res.body.message, "Data berhasil dirubah");
                done();
            });
    });

    it('should response in password, contain 1 index', function(done) {
        userModel.getPassword(1)
            .then((result) => {
                assert.isObject(result);
                done();
            })
            .catch((error) => {
                done(error);
            });
    });

    it('should response in 0 that user has been deleted', function(done) {
        userModel.checkDeletedUser(7)
            .then((result) => {
                expect(result).equal(1);
                done();
            })
            .catch((error) => {
                done(error);
            });
    });

    it('should response in 1 that user exist', function(done) {
        userModel.checkDeletedUser(1)
            .then((result) => {
                expect(result).equal(0);
                done();
            })
            .catch((error) => {
                done(error);
            });
    });
});