/**
 * Created by fzlrhmn on 10/10/16.
 */

let chai        = require('chai');
let assert      = chai.assert;
let expect      = chai.expect;
let should      = chai.should();
let faq         = require('../models/faq');

describe('FAQ module', function() {
    it('should be address', function(done) {
        faq.getFaq()
            .then(result => {
                result.should.be.a('array');
                done();
            })
            .catch(error => {
                done(error);
            })
    });
});