var restify 	= require('restify'); // Restify for REST API
var server 		= restify.createServer(); // create restify server
var routes		= require('./routes/routes');
require('@risingstack/trace');
require('dotenv').config();

restify.CORS.ALLOW_HEADERS.push('Accept-Encoding');
restify.CORS.ALLOW_HEADERS.push('Accept-Language');
restify.CORS.ALLOW_HEADERS.push('x-access-token');

server.use(restify.CORS());
server.use(restify.queryParser({ mapParams: true }));
server.use(restify.bodyParser());
server.use(
	function crossOrigin(req,res,next){
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization, x-access-token');
		res.setHeader('Access-Control-Allow-Methods', '*');
		res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
		res.setHeader('Access-Control-Max-Age', '1000');

		return next();
	}
);

routes(server);

server.listen(process.env.PORT, function() {
	console.log('%s listening at %s', server.name, server.url);
});