/**
 * Created by fzlrhmn on 9/16/16.
 */

let amqp = require('amqplib/callback_api');
require('dotenv').config();

module.exports = function(payload, callback) {
    amqp.connect('amqp://localhost', function (err, conn) {
        if (err) {
            console.log(err);
            callback(err, null);
        }
        conn.createChannel(function (err, ch) {
            if (err) {
                console.log(err);
                callback(err, null);
            }
            let q = process.env.QUEUE_PUSH_NOTIFICATION;
            ch.assertQueue(q, {durable: true});

            ch.sendToQueue(q, Buffer.from(JSON.stringify(payload)));
        });

        setTimeout(function() { conn.close(); callback(null, payload); }, 500);
    })
};