/**
 * Created by fzlrhmn on 9/14/16.
 *
 * Worker
 */

let amqp        = require('amqplib/callback_api');
let fs          = require('fs');
let fetch       = require('node-fetch');
let moment 	    = require('moment');
let qlueModel   = require('../models/qlue_report');

require('dotenv').config();
moment().format();
moment.locale('id');

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        let q = process.env.QUEUE_POST_QLUE;

        ch.assertQueue(q, {durable: true});

        ch.consume(q, function(msg) {
            let payload = JSON.parse(msg.content.toString());
            let options = {
                method : 'POST',
                body : JSON.stringify(payload)
            };

            fetch(process.env.QLUE_URL, options)
                .then(res => {
                    if (res.status === 200) {
                        ch.ack(msg);
                        return res.json();
                    }else{
                        ch.nack(msg, false, true);
                    }
                })
                .then(json => {
                    let payloadDbReport = {};
                    if (payload.status === 'process') {
                        payloadDbReport.done_post_process_qlue = true;
                        payloadDbReport.post_process_qlue_response = JSON.stringify(json);
                    }
                    if(payload.status === 'complete'){
                        payloadDbReport.done_post_complete_qlue = true;
                        payloadDbReport.post_complete_qlue_response = JSON.stringify(json);
                    }
                    qlueModel.setProcessQlue(payloadDbReport,payload.post_id);
                    fs.writeFileSync('./log/success_log_queue_post_data_'+payload.post_id+' '+moment().format("YYYY-MM-DD HH:mm:ss")+'.json',JSON.stringify(json),'utf8');
                })
                .catch(err => {
                    fs.writeFileSync('./log/error_log_queue_post_data_'+payload.post_id+' '+moment().format("YYYY-MM-DD HH:mm:ss")+'.json',JSON.stringify(err),'utf8');
                });
        }, {noAck: false});
    });
});
