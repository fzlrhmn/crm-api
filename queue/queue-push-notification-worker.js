/**
 * Created by fzlrhmn on 9/16/16.
 */

let amqp                = require('amqplib/callback_api');
let fs                  = require('fs');
let fetch               = require('node-fetch');
let moment 	            = require('moment');
let UserNotification    = require('../models/user_notification');
let async               = require('async');

require('dotenv').config();
moment().format();
moment.locale('id');

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        let q = process.env.QUEUE_PUSH_NOTIFICATION;

        ch.assertQueue(q, {durable: true});

        ch.consume(q, function(msg) {
            let report = JSON.parse(msg.content.toString());
            UserNotification.getNotificationUser(report.id_skpd, report.status, function(error,data) {
                if (error) {
                    console.log('Error get user notification data : ' + error);
                }
                if (data.appIdArray.length > 0) {

                    var message = {
                        app_id: "863335bd-3f12-4af7-8cb6-7b1273224008",
                        contents: {"en": report.message },
                        headings:{"en" : report.heading},
                        include_player_ids: data.appIdArray,
                        data:{"id_report" : report.id_report,channel : report.channel}
                    };

                    if (report.channel == 'call_center_112') {
                        message.android_sound = 'siren';
                    }

                    var headers = {
                        "Content-Type": "application/json",
                        "Authorization": "Basic OGUwNmM5YTQtZTBkMC00NjlmLTg0ZTEtNzJkYmM4OTU3ZDNh"
                    };

                    var options = {
                        method: "POST",
                        headers: headers,
                        body : JSON.stringify(message)
                    };

                    fetch('https://onesignal.com/api/v1/notifications', options)
                        .then(res => {
                            if (res.status === 200) {

                                async.waterfall([
                                    function(callback) {

                                        let notificationData = [];

                                        let userData = data.userId.filter(function(value,index,array) {
                                            return array.indexOf(value) == index;
                                        });

                                        userData.forEach(item => {
                                            let rowData = {
                                                id_report : report.id_report,
                                                user_id : item,
                                                title : report.heading,
                                                message : report.message
                                            };

                                            notificationData.push(rowData);
                                        });

                                        callback(null, notificationData);
                                    },
                                    function(notification,callback) {
                                        UserNotification
                                            .setNotification(notification)
                                            .then(res => {
                                                callback(null, res);
                                            })
                                            .catch(error => {
                                                callback(error, null);
                                            });
                                    }
                                ], function(err, result) {
                                    if (err) {
                                        console.log(err);
                                    }
                                    ch.ack(msg);
                                    return res.json();
                                });
                            }else{
                                fs.writeFileSync('./log/error_log_queue_push_notification_'+report.id_report+' '+moment().format("YYYY-MM-DD HH:mm:ss")+'.json',res.text(),'utf8');
                                ch.nack(msg);
                            }
                        })
                        .then(response => {
                            //fs.writeFileSync('./log/success_log_queue_push_notification_'+report.id_report+' '+moment().format("YYYY-MM-DD HH:mm:ss")+'.json',JSON.stringify(response),'utf8');
                        })
                        .catch(err => {
                            // write error file and unacknowledge message
                            fs.writeFileSync('./log/error_queue_push_notification_'+report.id_report+' '+moment().format("YYYY-MM-DD HH:mm:ss")+'.json',err,'utf8');
                            ch.nack(msg);
                        });
                } else {
                    ch.ack(msg);
                }

            });
        }, {noAck: false});
    });
});
